#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

PROFILE_INFERENCE_URL="https://github.com/wassermanlab/JASPAR-profile-inference.git"
JPI="$BASEDIR/profile-inference/common/jpi"
PI="$BASEDIR/profile-inference"

# Download the package and place it in the base directory.
# The dependencies are incorporated into the JASPAR dependencies found in the
# requirements file, additional ones being pandas, scipy and tqdm.

cd "$BASEDIR"

for VERSION in common 2020 2022 ; do
    if [ ! -e "$PI/$VERSION" ] ; then
        mkdir -p "$PI/$VERSION"
    fi
done

if [ ! -e "$JPI" ] ; then
    git clone "$PROFILE_INFERENCE_URL" "$JPI"
fi

# Make sure the repository is up to date.

cd "$JPI"
git fetch

# Make version-specific clones.

if [ ! -e "$PI/2022/jpi" ] ; then
    git clone "$JPI" "$PI/2022/jpi"
    cd "$PI/2022/jpi"
    git checkout "5fa64a79c79763384b484d9345bf112bd7dcbf11"
fi

if [ ! -e "$PI/2020/jpi" ] ; then
    git clone "$JPI" "$PI/2020/jpi"
    cd "$PI/2020/jpi"
    git checkout "faa660497f6a222c22f3aa42db992a5f70f84b4b"
fi

# vim: tabstop=4 expandtab shiftwidth=4
