#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
BRANDING=`realpath "$BASEDIR/../jaspar-branding"`

# Obtain any logos that can be deployed in place of various placeholder logos.

if [ -e "$BRANDING" ] ; then
    cp "$BRANDING/logos/"* "$BASEDIR/portal/static/img/logos/"
    cp "$BRANDING/templates/"* "$BASEDIR/portal/templates/portal/"
fi

# vim: tabstop=4 expandtab shiftwidth=4
