#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

APACHE=`apache_user_name`

# Set permissions on the application directory for SQLite write access.
# See: https://stackoverflow.com/questions/3319112/sqlite-error-attempt-to-write-a-readonly-database-during-insert

setfacl -m "u:$APACHE:rwx" "$BASEDIR"

for DATABASE in "$BASEDIR/JASPAR"*".sqlite3" ; do

    # Handle the wildcard case.

    if [ ! -e "$DATABASE" ] ; then
        echo "No databases found!" 1>&2
        exit 1
    fi

    setfacl -m "u:$APACHE:rw" "$DATABASE"
done

# vim: tabstop=4 expandtab shiftwidth=4
