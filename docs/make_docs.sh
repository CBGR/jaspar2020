#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Produce a rewritten path, replacing a base directory with an output directory.
# rewrite_path <filename> <base directory> <output directory>

rewrite_path()
{
    FILEPATH=`realpath "$1"`
    RELPATH=${FILEPATH##$2/}
    NEWPATH="$3/$RELPATH"

    echo "$NEWPATH"
}

# Produce a filename with a rewritten suffix/extension.
# rewrite_suffix <filename> <input suffix> <output suffix>

rewrite_suffix()
{
    # Combine the directory with basename and new extension.

    echo `dirname "$1"`/`basename "$1" "$2"`"$3"
}

# Ensure that the directory for a filename exists.
# ensure_path <filename>

ensure_path()
{
    NEWDIR=`dirname "$1"`

    if [ ! -e "$NEWDIR" ] ; then
        mkdir -p "$NEWDIR"
    fi
}

# Produce the rewritten output filename.
# output_name <filename> <base directory> <output directory> <input suffix> <output suffix>

output_name()
{
    OUTPATH=`rewrite_path "$1" "$2" "$3"`
    OUTFILE=`rewrite_suffix "$OUTPATH" "$4" "$5"`

    echo "$OUTFILE"
}



# Test for options.

if [ "$1" = '--links' ] ; then
    MAKE_LINKS=$1
    shift 1
else
    MAKE_LINKS=
fi

if [ "$1" = '--diagrams-only' ] ; then
    DIAGRAMS_ONLY=$1
    shift 1
else
    DIAGRAMS_ONLY=
fi

# Write relative to a chosen output directory or the base directory.

if [ "$1" ] ; then
    OUTDIR=$1
    shift 1
else
    OUTDIR=$BASEDIR
    if [ "$MAKE_LINKS" ] ; then
        echo "Links may not be created. Use an explicit output directory instead." 1>&2
    fi
fi

# Process Graphviz diagrams.

for FILENAME in "$THISDIR/"*.dot ; do
    if [ ! -e "$FILENAME" ] ; then
        break
    fi
    OUTFILE=`output_name "$FILENAME" "$BASEDIR" "$OUTDIR" .dot .svg`
    ensure_path "$OUTFILE"
    dot -Tsvg -o "$OUTFILE" "$FILENAME"
done

# Stop processing if only the diagrams are to be converted.

if [ "$DIAGRAMS_ONLY" ] ; then
    exit 0
fi

# Process reStructured Text documents.

for FILENAME in "$BASEDIR/"*.rst "$THISDIR/"*.rst ; do
    if [ ! -e "$FILENAME" ] ; then
        break
    fi

    OUTLINK=`rewrite_path "$FILENAME" "$BASEDIR" "$OUTDIR"`
    OUTFILE=`rewrite_suffix "$OUTLINK" .rst .html`
    ensure_path "$OUTFILE"
    rst2html "$FILENAME" "$OUTFILE"

    # Link to the output files from input file aliases.
    # This allows convenient browsing of static output with links leading to
    # output rather than input, but it only works with a separate output
    # directory.

    if [ "$MAKE_LINKS" ] && [ ! -f "$OUTLINK" ] ; then

        # Remove any existing links.

        if [ -h "$OUTLINK" ] ; then
            rm "$OUTLINK"
        fi

        ln -s `basename "$OUTFILE"` "$OUTLINK"
    fi
done

# vim: tabstop=4 expandtab shiftwidth=4
