#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest import main, TestCase
import re, requests, sys

# API access definitions.

base_url = "http://localhost:8000/api/v1"
json_format = "application/json"

# Annotation tags with optional/unpopulated tags commented out.

tags = {
    "alias",
    "centrality_logp",
    "class",
    "comment",
    #"consensus",
    "description",
    #"end_relative_to_tss",
    "family",
    #"included_models",
    #"jaspar",
    #"mcs",
    "pubmed_ids",
    "remap_tf_name",
    "source",
    #"start_relative_to_tss",
    "symbol",
    "tax_group",
    "tfbs_shape_id",
    #"transfac",
    "type",
    #"unibind",
    }

# Utility functions.

def GET(url, params=None, format=json_format):
    r = requests.get(url, params=params, headers={"Accept" : format})
    if not r.ok:
        return None

    if format == json_format:
        return r.json()
    else:
        return r.text

def get_matrix(base_id, version, format=json_format):
    return GET("%s/matrix/%s.%s/" % (base_url, base_id, version), format=format)

# Test cases.

class ResourceCollectionTest(TestCase):
    '''
    Test collections of resources.
    '''

    def _test_collections(self, resource_collection, attr):
        '''
        Test for the presence of collections.
        '''

        info = GET("%s/%s" % (base_url, resource_collection))
        self.assertTrue(info["count"], "Count %s for available collections in %s" % (info["count"], resource_collection))

        results = info["results"]
        self.assertTrue(results, "Number of results %s for available collections in %s" % (results, resource_collection))

        result = results[0]
        url = result["url"]
        collection = GET(url)

        # Handle missing resources and unexpected results.

        self.assertTrue(collection, "Unavailable first collection in %s" % resource_collection)
        self.assertTrue(collection[attr], "Attribute %s is %s for first available collection in %s" % (attr, collection[attr], resource_collection))

    def _test_collection(self, resource_collection, collection, attr, fn):
        '''
        Get the given collection and test the result with the given
        function.
        '''

        info = GET("%s/%s/%s/" % (base_url, resource_collection, collection))

        # Handle missing resources and unexpected results.

        fn(info and info[attr], "Attribute %s is %s for collection %s in %s" % (attr, info and info[attr], collection, resource_collection))

class ReleaseCollectionTest(ResourceCollectionTest):
    '''
    Test release details.
    '''

    def test_releases(self):
        '''
        Test the presence of release data.
        '''

        self._test_collections("releases", "year")

    def test_example_release(self):
        '''
        Test for the presence of the 2022 (9th) release.
        '''

        self._test_collection("releases", "9", "year", self.assertTrue)

    def test_unrecognised_release(self):
        '''
        Test for the absence of an unrecognised release.
        '''

        self._test_collection("releases", "XXXXXXXX", "year", self.assertFalse)

class MatrixCollectionTest(ResourceCollectionTest):
    '''
    Test collections of matrices.
    '''

    def test_collections(self):
        '''
        Test the presence of matrix collections.
        '''

        self._test_collections("collections", "count")

    def test_core_collection(self):
        '''
        Test for the presence of the CORE collection.
        '''

        self._test_collection("collections", "CORE", "count", self.assertTrue)

    def test_unrecognised_collection(self):
        '''
        Test for the absence of an unrecognised collection.
        '''

        self._test_collection("collections", "XXXXXXXX", "count", self.assertFalse)

class MatrixDetailTest(TestCase):
    '''
    Test the matrix detail information.
    '''

    def test_basic_detail(self):
        '''
        Test the detail information for a single matrix.
        '''

        base_id = "MA0597"
        version = 1

        detail = get_matrix(base_id, version)
        self._test_identifiers(detail, base_id, version)

        for tag in tags:
            self.assertTrue(detail.get(tag), "Tag %s in detail for %s.%s" % (tag, base_id, version))

        species = detail.get("species")

        self.assertTrue(species, "Species for %s.%s" % (base_id, version))
        self.assertEqual(species[0]["tax_id"], 9606)

    def test_versions_navigation(self):
        '''
        Test version information and navigation.
        '''

        base_id = "MA0007"
        version = 2

        # Obtain matrix details.

        detail = get_matrix(base_id, version)
        self._test_identifiers(detail, base_id, version)

        # Obtain matrix versions from the details.

        versions = detail["versions_url"]
        info = GET(versions)
        self.assertGreaterEqual(info["count"], version, "Versions %s, given identified version %s" % (info["count"], version))

        # Obtain the first version from the versions.

        first = info["results"][0]
        detail = GET(first["url"])
        self._test_identifiers(detail, base_id, 1)

    def _test_identifiers(self, detail, base_id, version):
        '''
        Test the reported identifiers in the result details.
        '''
        self.assertEqual(detail["base_id"], base_id, "Base identifier %s, specified %s" % (detail["base_id"], base_id))
        self.assertEqual(detail["version"], version, "Version %s, specified %s" % (detail["version"], version))

class MatrixFormatTest(TestCase):
    '''
    Test different matrix data output formats.
    '''

    def test_meme_format(self):
        '''
        Test MEME format output.
        '''

        base_id = "MA0597"
        version = 1

        text = get_matrix(base_id, version, format="text/meme")
        header = re.compile("^MEME version 4$", re.MULTILINE)
        self.assertRegex(text, header, "Header in received text for %s.%s" % (base_id, version))

class MatrixInferTest(TestCase):
    '''
    Test inference requests.
    '''

    def test_infer(self):
        '''
        Test motif inference.
        '''

        # Attempt to perform profile inference.

        seq = """\
MYSMLDTDMKSPVQQSNALSGGPGTPGGKGNTSTPDQDRVKRPMNAFMVWSRGQRRKMAQ
ENPKMHNSEISKRLGADWKLLSDSEKRPFIDEAKRLRAVHMKDYPDYKYRPRRKTKTLLK
KDKYSLPGNLLAPGINPVSGGVGQRIDTYPHMNGWTNGAYSLMQEQLGYGQHPAMNSSQM
QQIQHRYDMGGLQYSPMMSSAQTYMNAAASTYSMSPAYNQQSSTVMSLASMGSVVKSEPS
SPPPAITSHTQRACLGDLRDMISMYLPPGGDAGDHSSLQNSRLHSVHQHYQSAGGPGVNG
TVPLTHI"""

        seq = seq.replace("\n", "")

        info = GET("%s/infer/%s/" % (base_url, seq))

        # Handle missing inference feature by accepting results as None.

        self.assertTrue(info["results"] is None or info["count"], "Count %s for results of motif inference")

class MatrixSearchTest(TestCase):
    '''
    Test searching for matrix profiles.
    '''

    def test_name_search(self):
        '''
        Test a search for profiles using a name.
        '''

        self._test_search("name", "LFY", self.assertTrue)
        self._test_search("name", "leafy", self.assertFalse)

    def test_name_search_versioned(self):
        '''
        Test a name-based search selecting the latest versions of profiles.
        '''

        unversioned = self._test_search("name", "Ar", self.assertTrue)
        versioned = self._test_search("name", "Ar", self.assertTrue, {"version" : "latest"})

        self.assertGreater(unversioned["count"], versioned["count"],
            "Unversioned result count %s, versioned result count %s" % (
                unversioned["count"], versioned["count"]))

    def test_general_search(self):
        '''
        Test a search for profiles using a name, matching generally, including
        synonyms.
        '''

        self._test_search("search", "LFY", self.assertTrue)
        self._test_search("search", "leafy", self.assertTrue)

    def _test_search(self, param, query, fn, params=None):
        '''
        Perform a search, testing the result with self.assertFalse or
        self.assertTrue.
        '''

        _params = {param : query}
        _params.update(params or {})

        info = GET("%s/matrix/" % base_url, params=_params)
        fn(info["count"], "Count %s for search involving %s" % (info["count"], query))
        return info



# Some additional help.

help_text = """\
To test a remote site, specify the --url option as follows:

%s --url http://test-jaspar.uio.no/api/v1

In order to function correctly, the configuration must allow cookies to be
transmitted over the protocol in use. The following settings must be verified:

 * CSRF_COOKIE_SECURE
 * SESSION_COOKIE_SECURE
""" % (sys.argv[0])



# Main program, calling the unit test runner.

if __name__ == '__main__':
    args = sys.argv

    # Handle command arguments before the test runner sees them.

    if len(args) > 1:

        # Show some additional help.

        if args[1] == "--test-help":
            print(help_text, file=sys.stderr)
            sys.exit(1)

        # Obtain any indicated URL for the Web site.

        elif len(args) > 2 and args[1] == "--url":
            base_url = args[2]
            del args[1:3]

    main()

# vim: tabstop=4 expandtab shiftwidth=4
