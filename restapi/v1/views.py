# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from os.path import exists
from utils import utils

import coreapi
import coreschema

from django.db.models import Q, QuerySet, Max
from django.http import Http404
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import renderers
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.throttling import UserRateThrottle
from rest_framework.views import APIView

from rest_framework_jsonp.renderers import JSONPRenderer
from rest_framework_yaml.renderers import YAMLRenderer
from rest_framework_yaml.parsers import YAMLParser

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
    BaseFilterBackend,
    )
from rest_framework.generics import (
    ListAPIView,
    )
from rest_framework.pagination import (
    PageNumberPagination,
    )

# Live API imports.

from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework.decorators import api_view, renderer_classes
from rest_framework import response, schemas

# Portal imports.

from portal.models import (
    Gene, Matrix, MatrixAnnotation, MatrixProtein, MatrixSpecies, Synonym, Tffm,
    Tax, Release
    )

# REST API imports.

from .linking import *
from .serializers import MatrixSerializer, TffmSerializer, TaxSerializer, ReleaseSerializer

# YAML rendering fixes.

import yaml.representer

YAMLRenderer.encoder.add_representer(QuerySet, yaml.representer.SafeRepresenter.represent_list)
YAMLRenderer.encoder.add_representer(map, yaml.representer.SafeRepresenter.represent_list)

# Renderer customisations.
# Since dedicated templates are used, the renderers be overridden to reference
# them.

class BrowsableAPIRenderer(renderers.BrowsableAPIRenderer):
    template = 'restapi/api.html'

class LiveAPIRenderer(SwaggerUIRenderer):
    template = 'restapi/api_live.html'



# Pagination.

class MatrixResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000

# Database selection, employing the application settings.

def _get_jaspar_release(release='default'):
    '''
    This function defines which release of JASPAR database to use. If none is
    indicated, it uses the default (latest) release, configured in the settings.
    '''

    if release in ['2014', 'jaspar2014', 'JASPAR2014', '5', 'five', 'fifth']:
        return 'JASPAR2014'
    elif release in ['2016', 'jaspar2016','JASPAR2016', '6', 'six', 'sixth']:
        return 'JASPAR2016'
    elif release in ['2018', 'jaspar2018', 'JASPAR2018', '7', 'seven', 'seventh']:
        return 'JASPAR2018'
    elif release in ['2020', 'jaspar2020', 'JASPAR2020', '8', 'eight', 'eighth']:
        return 'JASPAR2020'
    elif release in ['2022', 'jaspar2022', 'JASPAR2022', '9', 'nine', 'ninth']:
        return 'JASPAR2022'
    else:
        return 'default'

# Utility functions.

def _matrix_ids_for_tax_group(tax_group):
    '''
    Obtain matrix references for annotations having the group.
    '''
    annotations = MatrixAnnotation.objects.filter(tag='tax_group', val=tax_group.lower())
    return annotations.values_list('matrix_id')

def _make_results_dict(results):
    '''
    Return a dictionary containing results and a count indicator.
    '''
    return {'count': len(results), 'results': results}

def _species_summary(tax):
    '''
    Return a summary of a species for a matrix.
    '''
    return {'tax_id': tax.tax_id, 'name': tax.species}

# Result rendering.

class CommonRenderer(renderers.BaseRenderer):
    '''
    Common support for rendering PFMs.
    '''

    def _get_output(self, data):
        '''
        Return any output representation of the PFM provided by the data.
        '''

        # Obtain a collection of results, even if only one was provided.

        results = data.get('results')
        if results is None:
            results = 'pfm' in data and [data] or []

        # Process each result, producing output.

        output = []
        for d in results:
            pfm = utils.print_matrix_data(utils.get_matrix_data(utils.get_internal_id(d['matrix_id'])),
                matrix_id=d['matrix_id'],
                matrix_name=d['name'],
                format=self.format
                )
            output.append(pfm)

        # Return a string representation of the output.

        return "".join(output)

    def _render_output(self, output):
        '''
        Return appropriate final output.
        '''
        return output or '# No PFM data available'

    def render(self, data, media_type=None, renderer_context=None):
        return self._render_output(self._get_output(data))

class JASPARRenderer(CommonRenderer):
    '''
    Render each PFM in JASPAR format.
    '''
    media_type = 'text/jaspar'
    format = 'jaspar'

class MEMERenderer(CommonRenderer):
    '''
    Render each PFM in MEME format.
    '''
    media_type = 'text/meme'
    format = 'meme'

    def _render_output(self, output):
        output = output and (utils.write_meme_header() + output)
        return super()._render_output(output)

class PFMRenderer(CommonRenderer):
    '''
    Render each PFM in Raw PFM format.
    '''
    media_type = 'text/pfm'
    format = 'pfm'

class TRANSFACRenderer(CommonRenderer):
    '''
    Render each PFM in TRANSFAC format.
    '''
    media_type = 'text/transfac'
    format = 'transfac'

class BEDListRenderer(renderers.BaseRenderer):
    '''
    Render a list of sites in BED format.
    '''
    media_type = 'text/bed'
    format = 'bed'

    def render(self, data, media_type=None, renderer_context=None):

        bed = []
        import json
        for site in data['sites']:
            region = '\t'.join([site.get('chrom'), str(site.get('start')), site.get('end'), site.get('name'), '.', site.get('strand')+"\n"])
            bed.append(region)

        if not bed:
            bed = 'No BED available'
            return bed
        else:
            return bed

# Filter classes.

class JASPARFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [coreapi.Field(
            name='collection',
            location='query',
            schema=coreschema.String(
                description='JASPAR Collection name. For example: CORE or CNE.',
                title='Collection',
                ),
            type='string',
            required=False,
            ),
        coreapi.Field(
            name='name',
            location='query',
            schema=coreschema.String(
                description='Search by TF name (case-sensitive). For example: SMAD3',
                title='TF Name',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='tax_group',
            location='query',
            schema=coreschema.String(
                description='Taxonomic group. For example: Vertebrates',
                title='Tax Group',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='tax_id',
            location='query',
            schema=coreschema.String(
                description='Taxa ID. For example: 9606 for Human & 10090 for Mus musculus. Multiple IDs can be added separated by commas (e.g. tax_id=9606,10090).',
                title='Tax ID',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='tf_class',
            location='query',
            schema=coreschema.String(
                description='Transcription factor class. For example: Zipper-Type',
                title='TF Class',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='tf_family',
            location='query',
            schema=coreschema.String(
                description='Transcription factor family. For example: SMAD factors',
                title='TF Family',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='data_type',
            location='query',
            schema=coreschema.String(
                description='Type of data/experiment. For example: ChIP-seq, PBM, SELEX etc.',
                title='Experiment/Data Type',
                ),
            required=False,
            type='string')
        ]

    def filter_queryset(self, request, queryset, view):

        get = request.GET.get
        query_string = get('search', None)
        collection = get('collection', None)
        tax_group = get('tax_group', None)
        tax_id = get('tax_id', None)
        tf_class = get('tf_class', None)
        tf_family = get('tf_family', None)
        data_type = get('data_type', None)
        name = get('name', None)

        if collection:
            queryset = queryset.filter(collection=collection.upper())

        if name:
            queryset = queryset.filter(name__iexact=name)

        annotations = MatrixAnnotation.objects.values_list('matrix_id')
        species = MatrixSpecies.objects.values_list('matrix_id')

        if tax_group:
            matrix_ids = annotations.filter(tag='tax_group', val=tax_group.lower())
            queryset = queryset.filter(id__in=matrix_ids)

        if tax_id:
            tax_ids = tax_id.split(',')
            matrix_ids = species.filter(tax_id__in=tax_ids)
            queryset = queryset.filter(id__in=matrix_ids)

        if tf_class:
            matrix_ids = annotations.filter(tag='class', val__icontains=tf_class)
            queryset = queryset.filter(id__in=matrix_ids)

        if tf_family:
            matrix_ids = annotations.filter(tag='family', val__icontains=tf_family)
            queryset = queryset.filter(id__in=matrix_ids)

        if data_type:
            matrix_ids = annotations.filter(tag='type', val__icontains=data_type)
            queryset = queryset.filter(id__in=matrix_ids)

        if query_string:
            #filter based of MatrixAnnotation model
            #matrix_ids = annotations.filter(val__icontains=query_string).distinct()
            #queryset = queryset.filter(id__in=matrix_ids)

            queryset = queryset.filter(
                Q(name__icontains=query_string) |
                Q(base_id__icontains=query_string) |
                Q(collection__icontains=query_string) |

                # Use synonyms to identify genes and - via proteins - profiles,
                # incorporating these identified profiles into the results.

                Q(genes__in=Gene.objects.filter(
                    gene_id__in=Synonym.objects.filter(
                        synonym__icontains=query_string).values_list("gene_id")))
                )

            # Eliminate duplicates.

            queryset = queryset.distinct()

        return queryset.order_by('name')

class VersionFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
        coreapi.Field(
            name='version',
            location='query',
            schema=coreschema.String(
                description ='If set to latest, return latest version',
                ),
            required=False,
            type='string')
        ]

    def filter_queryset(self, request, queryset, view):
        version = request.GET.get('version', None)

        if version in ['latest', 'current']:
            queryset = utils.get_latest_versions(queryset)

        return queryset.order_by('name')

class ReleaseFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
        coreapi.Field(
            name='release',
            location='query',
            schema=coreschema.String(
                description='Access a specific release of JASPAR. Available releases are: ' \
                            '2014, 2016, 2018, 2020 and 2022. If blank, the query will ' \
                            'provide data from the latest release.',
                title='JASPAR Release'
                ),
            required=False,
            type='string')
        ]

    def filter_queryset(self, request, queryset, view):
        release = request.GET.get('release', 'default')
        return queryset.using(_get_jaspar_release(release))

# View classes.

class MatrixDetailsViewSet(APIView):
    """
    API endpoint that returns the matrix profile detail information.
    """

    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get_matrix(self, base_id, version=None):
        """
        Return a matrix given the base identifier and optional version.
        Without a version indicated, return the latest matrix version.
        """

        try:
            if not version:
                return utils.get_latest_versions(Matrix.objects.filter(base_id=base_id)).first()
            else:
                return Matrix.objects.get(base_id=base_id, version=version)
        except Matrix.DoesNotExist:
            raise Http404

    def get(self, request, matrix_id, format=None):
        """
        Gets profile detail information
        """

        setattr(request, 'view', 'api-browsable')

        (base_id, version) = utils.split_id(matrix_id)
        matrix = self.get_matrix(base_id, version)

        data_dict = {
            'matrix_id': '%s.%s' % (matrix.base_id, matrix.version),
            'name': matrix.name,
            'base_id': matrix.base_id,
            'version': matrix.version,
            'collection': matrix.collection,
            'sequence_logo': get_sequence_logo(request, base_id, version),
            'versions_url': get_versions_url(request, base_id),
            'sites_url': get_sites_url(request, base_id, version),
            'pfm' : utils.get_matrix_data(matrix.id)
            }

        # Get more details about the matrix from annotations.

        annotations = MatrixAnnotation.objects.filter(matrix_id=matrix.id)
        data_dict.update(utils.map_annotations(annotations))

        # Get uniprot accessions.

        proteins = MatrixProtein.objects.filter(matrix_id=matrix.id)

        protein_list = []
        for protein in proteins:
            protein_list.append(protein.acc)

        data_dict.update({'uniprot_ids': protein_list})

        # Get species.

        matrix_species = MatrixSpecies.objects.filter(matrix_id=matrix.id).values_list("tax_id")
        all_species = Tax.objects.filter(tax_id__in=matrix_species)
        data_dict.update({'species': map(_species_summary, all_species)})

        #get tffm info
        try:
            tffm = Tffm.objects.get(matrix_base_id=base_id, matrix_version=version)
            tffm_dic = {
                'tffm': {
                    'tffm_id': '%s.%s' % (tffm.base_id, tffm.version),
                    'base_id': tffm.base_id,
                    'version': tffm.version,
                    'log_p_1st_order': tffm.log_p_1st_order,
                    'log_p_detailed': tffm.log_p_detailed,
                    'experiment_name': tffm.experiment_name,
                    'tffm_url': get_matrix_tffm_url(request, tffm.base_id, tffm.version)
                    }
                }
        except Tffm.DoesNotExist:
            tffm_dic = {
                'tffm': None,
                }

        data_dict.update(tffm_dic)

        return Response(data_dict)

class MatrixListViewSet(ListAPIView):
    """
    REST API endpoint that returns a list of all matrix profiles.
    """

    serializer_class = MatrixSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, JASPARFilterBackend, VersionFilterBackend, ReleaseFilterBackend, ]
    parser_classes = (YAMLParser,)
    renderer_classes = [ renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List all matrix profiles
        """

        setattr(self.request, 'view', 'api-browsable')

        return Matrix.objects.all().order_by('base_id')

class MatrixVersionsViewSet(APIView):
    """
    API endpoint that returns matrix model versions based on base_id.
    """

    serializer_class = MatrixSerializer
    throttle_classes = (UserRateThrottle,)
    filter_backends = [OrderingFilter]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get(self, request, base_id, format=None):
        """
        List matrix profile versions based on base_id
        """
        setattr(self.request, 'view', 'api-browsable')

        queryset = Matrix.objects.filter(base_id=base_id).order_by('version')

        results = []
        for matrix in queryset:
            results.append({
                'matrix_id': '%s.%s' % (matrix.base_id, matrix.version),
                'name': matrix.name,
                'base_id': matrix.base_id,
                'version': matrix.version,
                'collection': matrix.collection,
                'sequence_logo': get_sequence_logo(request, matrix.base_id, matrix.version),
                'url': get_matrix_url(request, matrix.base_id, matrix.version)
                })

        return Response(_make_results_dict(results))

class CollectionListViewSet(APIView):
    """
    API endpoint that returns a list of all collection names.
    """

    filter_backends = [SearchFilter, OrderingFilter,ReleaseFilterBackend]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request, format=None):
        """
        List all the collections available in JASPAR.
        """
        setattr(self.request, 'view', 'api-browsable')

        collections = utils.get_collection_names()

        results = []
        for collection in collections:
            results.append({
                'name': collection,
                'url': get_collection_url(request, collection)
                })

        return Response(_make_results_dict(results))

class CollectionMatrixListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all matrix profiles based on collection name.
    """

    serializer_class = MatrixSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, VersionFilterBackend,ReleaseFilterBackend]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List matrix profiles based on the collection name from the URL.
        """

        setattr(self.request, 'view', 'api-browsable')

        collection = self.kwargs['collection']
        return Matrix.objects.filter(collection=collection.upper()).order_by('name')

class TaxonListViewSet(APIView):
    """
    API endpoint that returns a list of all taxonomic groups.
    """

    filter_backends = [ReleaseFilterBackend,]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request, format=None):
        """
        List all the taxonomic groups that are available in JASPAR.
        """

        setattr(self.request, 'view', 'api-browsable')

        taxons = MatrixAnnotation.objects.values_list('val', flat=True).filter(tag='tax_group').distinct()

        results = []
        for taxon in taxons:
            results.append({
                'name': taxon,
                'url': get_taxon_url(request, taxon)
                })

        return Response(_make_results_dict(results))

class TaxonMatrixListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all matrix profiles based on taxonomic group.

    Available taxonomic groups are: vertebrates, plants, insects, nematodes, fungi, urochordates
    """

    serializer_class = MatrixSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, VersionFilterBackend, ReleaseFilterBackend]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List matrix profiles based on the taxonomic group from the URL.
        """

        setattr(self.request, 'view', 'api-browsable')

        tax_group = self.kwargs['tax_group']
        matrix_ids = _matrix_ids_for_tax_group(tax_group)
        return Matrix.objects.filter(id__in=matrix_ids).order_by('name')

class SpeciesListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all species.
    """

    serializer_class = TaxSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, ReleaseFilterBackend, ]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List all the species are available in JASPAR.
        """

        setattr(self.request, 'view', 'api-browsable')

        # Get species references for species associated with matrices.

        species = MatrixSpecies.objects.values_list('tax_id').distinct()
        return Tax.objects.filter(tax_id__in=species).order_by('species')

class SpeciesMatrixListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all matrix profiles based on species.
    """

    serializer_class = MatrixSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, VersionFilterBackend, ReleaseFilterBackend, ]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List matrix profiles based on the taxonomy identifier from the URL.
        """

        setattr(self.request, 'view', 'api-browsable')

        tax_id = self.kwargs['tax_id']

        # Get matrix references for specific species.

        species = MatrixSpecies.objects.filter(tax_id=tax_id)
        matrix_ids = species.values_list('matrix_id')
        return Matrix.objects.filter(id__in=matrix_ids).order_by('name')

class TFFMFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
        coreapi.Field(
            name='tax_group',
            location='query',
            schema=coreschema.String(
                description='Taxonomic group. For example: Vertebrates or Plants',
                title='Taxonomic Group',
                ),
            required=False,
            type='string')
        ]

    def filter_queryset(self, request, queryset, view):

        query_string = request.GET.get('search', None)
        tax_group = request.GET.get('tax_group', None)

        if tax_group:
            matrix_ids = _matrix_ids_for_tax_group(tax_group)
            matrix_base_ids = utils.get_matrix_instances(matrix_ids).values_list('base_id').distinct()
            queryset = queryset.filter(matrix_base_id__in=matrix_base_ids)

        if query_string:
            queryset = queryset.filter(
                Q(name__icontains=query_string) |
                Q(base_id__icontains=query_string) |
                Q(experiment_name__icontains=query_string) |
                Q(matrix_base_id__icontains=query_string)
                ).distinct()

        return queryset

class TFFMListViewSet(ListAPIView):
    """
    REST API endpoint that returns a list of all TFFM profiles.
    """

    serializer_class = TffmSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, TFFMFilterBackend, ReleaseFilterBackend]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List all TFFM profiles
        """

        setattr(self.request, 'view', 'api-browsable')

        release = self.request.GET.get('release', 'default')

        #TFFM was introduced in 2016 release of jaspar. A dirty way to return no tffm data for older releases of jaspar
        if release in ['2004','2006','2008','2010','2014','1','2','3','4','5']:
            queryset = Tffm.objects.filter(base_id='Some thing does not exist') #This will retun Tffm object with zero records
        else:
            queryset = Tffm.objects.using(_get_jaspar_release(release)).all().order_by('base_id')

        return queryset

class TFFMDetailsViewSet(APIView):
    """
    API endpoint that returns the TFFM detail information.
    """

    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request, tffm_id, format=None):
        """
        Gets TFFM detail information
        """

        setattr(request, 'view', 'api-browsable')

        (base_id, version) = utils.split_id(tffm_id)

        #get tffm info
        try:
            tffm = Tffm.objects.get(base_id=base_id, version=version)
            tffm_dic = {

                'tffm_id': '%s.%s' % (tffm.base_id, tffm.version),
                'base_id': tffm.base_id,
                'version': tffm.version,
                'matrix_base_id': tffm.matrix_base_id,
                'matrix_id': '%s.%s' % (tffm.matrix_base_id, tffm.matrix_version),
                'matrix_url': get_matrix_url(request, tffm.matrix_base_id, tffm.matrix_version),
                'matrix_version': tffm.matrix_version,
                'experiment_name': tffm.experiment_name,

                'first_order': {
                    'log_p': tffm.log_p_1st_order,
                    'dense_logo': get_tffm_url(request, tffm.base_id, tffm.version, 'first_order')+'_dense_logo.svg',
                    'summary_logo': get_tffm_url(request, tffm.base_id, tffm.version, 'first_order')+'_summary_logo.svg',
                    'hits': get_tffm_url(request, tffm.base_id, tffm.version, 'first_order')+'.hits.svg',
                    'xml': get_tffm_url(request, tffm.base_id, tffm.version, 'first_order')+'.xml',
                    },

                'detailed': {
                    'log_p': tffm.log_p_detailed,
                    'dense_logo': get_tffm_url(request, tffm.base_id, tffm.version, 'detailed')+'_dense_logo.svg',
                    'summary_logo': get_tffm_url(request, tffm.base_id, tffm.version, 'detailed')+'_summary_logo.svg',
                    'hits': get_tffm_url(request, tffm.base_id, tffm.version, 'detailed')+'.hits.svg',
                    'xml': get_tffm_url(request, tffm.base_id, tffm.version, 'detailed')+'.xml',
                    },

                }
        except Tffm.DoesNotExist:
            tffm_dic = {}

        return Response(tffm_dic)

class MatrixSitesListViewSet(ListAPIView):
    """
    API endpoint returns a list of all matrix profiles which have binding sites.
    """

    serializer_class = MatrixSerializer
    pagination_class = MatrixResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, ReleaseFilterBackend]
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        API endpoint returns a list of all matrix profiles which have binding sites
        """

        setattr(self.request, 'view', 'api-browsable')

        return Matrix.objects.all().order_by('base_id')

class MatrixSitesViewSet(APIView):
    """
    API endpoint that returns binding sites of profile based on matrix_id.
    """

    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, BEDListRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request, matrix_id, format=None):
        """
        List matrix profile sites based on matrix_id
        """
        setattr(self.request, 'view', 'api-browsable')

        #split base_id and version
        (base_id, version) = utils.split_id(matrix_id)

        try:
            matrix = Matrix.objects.get(base_id=base_id, version=version)
            sites = []
            bed_file = get_bed_filename(matrix_id)
            strand = ''
            if exists(bed_file):
                with open(bed_file, "r") as data:
                    for line in data:
                        region = line.split('\t')
                        name = region[3].rstrip("\n\r")
                        if len(region) > 5:
                            strand = region[5].rstrip("\n\r")
                        else:
                            strand = name[-2]
                        assembly = region[3].split('_')[0]
                        sites.append({
                            'chrom': region[0],
                            'start': region[1],
                            'end': region[2],
                            'strand': strand,
                            'assembly': assembly,
                            'name': region[3].rstrip("\n\r"),
                            })
                nsites = len(sites)
            else:
                sites = None
                nsites = None

            data_dict = {
                'matrix_id': '%s.%s' % (matrix.base_id, matrix.version),
                'name': matrix.name,
                'base_id': matrix.base_id,
                'version': matrix.version,
                'collection': matrix.collection,
                'url_matrix': get_matrix_url(request, matrix.base_id, matrix.version),
                'url_fasta': get_sites_fasta_url(request, matrix.base_id, matrix.version),
                'url_bed': get_sites_bed_url(request, matrix.base_id, matrix.version),
                'nsites': nsites,
                'sites': sites
                }

        except ObjectDoesNotExist:
            data_dict = {"Matrix ID does not exist."}

        return Response(data_dict)

class MatrixInferenceViewSet(APIView):
    """
    API endpoint that infer matrix models based on protien sequence.
    """

    throttle_classes = (UserRateThrottle,)
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, JASPARRenderer, TRANSFACRenderer, PFMRenderer, MEMERenderer, BrowsableAPIRenderer]

    def get(self, request, sequence, format=None):
        """
        Infer matrix profiles, given a protein sequence.
        """
        setattr(self.request, 'view', 'api-browsable')

        # Handle missing inference functionality.

        if not utils.jpi:
            return Response({'count' : 0, 'results' : None})

        # Get inferred motifs for the sequence.

        inferences = utils.motif_infer(sequence)
        results = []

        for values in inferences:
            (seq_record_id, name, matrix_id, evalue, query_start_end,
                target_start_end, dbd) = values[:7]

            data = {
                'matrix_id': matrix_id,
                'name': name,
                'evalue': evalue,
                'dbd': dbd,
                'url': get_matrix_url(request, matrix_id),
                'sequence_logo': get_sequence_logo(request, matrix_id),
                }

            results.append(data)

        return Response(_make_results_dict(results))

class ReleaseListViewSet(ListAPIView):
    """
    REST API endpoint that returns all releases of JASPAR database.
    """

    serializer_class = ReleaseSerializer
    throttle_classes = (UserRateThrottle,)
    filter_backends = [OrderingFilter,]
    pagination_class = MatrixResultsSetPagination
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        """
        List all JASPAR releases
        """

        setattr(self.request, 'view', 'api-browsable')

        return Release.objects.all().order_by('-year')

class ReleaseDetailsViewSet(APIView):
    """
    API endpoint that returns the JASPAR release detailed information.
    """

    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request, release_number, format=None):
        """
        Gets JASPAR release information based on release number
        """

        setattr(request, 'view', 'api-browsable')

        # Handle ill-formed parameters.

        try:
            release_number = int(release_number)
        except ValueError:
            raise Http404

        release = Release.objects.get(release_number=release_number)

        data_dict = {
            'year': release.year,
            'release_number': release.release_number,
            'doi': release.doi,
            'citation': release.citation,
            'release_date': release.release_date,
            'pubmed_id': release.pubmed_id,
            'abstract': release.abstract,
            'website': release.website,
            'active': release.active,
            }

        return Response(data_dict)

class APIRoot(APIView):
    """
    This is the root of the JASPAR RESTful API v1. Please read the documentation for more details.
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=format):
        setattr(request, 'view', 'api-browsable')
        return Response({
            'matrix': reverse('v1:matrix-list', request=request),
            'tffm': reverse('v1:tffm-list', request=request),
            'sites': reverse('v1:matrix-sites', args=['MA0550.1'], request=request),
            'collections': reverse('v1:list-collections', request=request),
            'taxons': reverse('v1:list-taxon', request=request),
            'species': reverse('v1:list-species', request=request),
            #'infer': reverse('v1:matrix-infer', request=request),
        })

def api_homepage(request):

    setattr(request, 'view', 'api-home')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api.html')

def api_docs(request):

    setattr(request, 'view', 'apidocs')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_docs.html')

def api_contactus(request):

    setattr(request, 'view', 'contactus')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_contact.html')

def api_overview(request):

    setattr(request, 'view', 'overview')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_overview.html')

def api_clients(request):

    setattr(request, 'view', 'clients')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_clients.html')

# Live API customisations.
# This is needed to permit overriding of both the renderer classes and the URL
# patterns, which the get_swagger_view function does not permit.

def api_live(patterns):
    class LiveAPIView(APIView):
        permission_classes = [AllowAny]
        renderer_classes = [OpenAPIRenderer, LiveAPIRenderer]

        def get(self, request):
            setattr(request, 'view', 'apilive')
            setattr(request, 'get_api_host', get_api_root_url(request))
            setattr(request, 'get_host', get_host_name(request))

            generator = schemas.SchemaGenerator(title='JASPAR Live API',
                                                patterns=patterns)
            return response.Response(generator.get_schema(request=request))

    return LiveAPIView.as_view()
