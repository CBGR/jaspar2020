Configuration
=============

Some additional configuration files may need to be defined in the deployment_
environment for certain functionality.

.. _deployment: Deployment.rst

Mail Sending
------------

The software may be configured to send e-mail messages when problems occur. To
do so, it will attempt to read from a file situated in the base directory of
this software distribution called ``smtp_settings.txt``. This file should
contain definitions in JSON format resembling the following:

.. code:: json

   {"host" : "smtp.uio.no",
    "user" : "jasparmail",
    "password" : "<password>",
    "port" : 465,
    "ssl" : true}

Here, the ``password`` would need to be set appropriately for the indicated
user recognised by the given SMTP host or server.

Settings Usage
--------------

Upon deploying data, the WSGI handler files may need to be adjusted to employ
settings_ corresponding to the "tags" employed by the available data.
Moreover, the generic ``jaspar/settings.py`` file may need to have the
default database name updated to reflect the most recent tag.

.. _settings: Settings.rst

Application Settings
++++++++++++++++++++

The ``FAMILIAL_BINDING_SITES_PREFIX`` setting refers to the location of
familial binding sites data. The presence and structure of such data should be
verified

The ``ADMINS`` setting should be changed to provide the contact details of
those responsible for the site being deployed. The setting is a list of
tuples, each tuple having the form...

::

  (name, e-mail address)

For the sending of mail messages to function, the settings file needs to be
defined and deployed, as described above.
