#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
WORKDIR="$THISDIR/work"

. "$THISDIR/tools/system.sh"

BLASTURL="ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/"

# Find and obtain the archive.

find_blast()
{
    ARCHIVE=`wget -O - -P "$WORKDIR" "$BLASTURL" | grep ">$ARCHIVE_PREFIX"'*'"-src.tar.gz<" | sed 's/^[^>]*>//;s/<.*$//'`
    if [ "$ARCHIVE" ] ; then
        wget -P "$WORKDIR" "$BLASTURL/$ARCHIVE"
    else
        return 1
    fi
}

# Test for the required program and exit if it is found.

if have blastp ; then
    exit 0
fi

if [ -e "$BASEDIR/lib/usr/local/bin/blastp" ] ; then
    exit 0
fi

# Find any existing blast archive.

ARCHIVE_PREFIX="ncbi-blast-2."

if [ ! -e "$WORKDIR" ] ; then
    mkdir "$WORKDIR"
fi

for FILENAME in "$WORKDIR/$ARCHIVE_PREFIX"*-src.tar.gz ; do

    # With no available archive, attempt to obtain one.

    if [ ! -e "$FILENAME" ] ; then
        if ! find_blast ; then
            echo "Failed to obtain blast." 1>&2
            exit 1
        fi
    else
        ARCHIVE=$FILENAME
    fi

    # With an archive, continue.

    break
done

DISTDIR=`basename "$ARCHIVE" .tar.gz`

cd "$WORKDIR"

if [ ! -e "$DISTDIR" ] ; then
    tar zxf "$ARCHIVE"
fi

# Build and install blast.

cd "$DISTDIR/c++"
./configure --prefix="$BASEDIR/lib/usr/local" --with-caution
make
make install

# vim: tabstop=4 expandtab shiftwidth=4
