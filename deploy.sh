#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
PROGNAME=`basename "$0"`
DEPLOY="$THISDIR/deploy"
LOGS="$THISDIR/deploy-logs"



# Utility functions.

initial_script()
{
    if echo "$1" | grep -q -e '^00-' ; then
        return 0
    else
        return 1
    fi
}

privileged_script()
{
    if echo "$1" | grep -q -e '^[[:digit:]]\+-root-' ; then
        return 0
    else
        return 1
    fi
}

unprivileged_script()
{
    if echo "$1" | grep -q -e '^[[:digit:]]\+-user-' ; then
        return 0
    else
        return 1
    fi
}

selected_script()
{
    SCRIPT=$1
    PRIV=$2

    if [ "$PRIV" ] ; then
        if initial_script "$SCRIPT" ; then
            if ( [ ! "$INITIAL_ONLY" ] && ( [ "$FINAL_ONLY" ] || [ "$USER_ONLY" ] ) ) ; then
                return 1
            fi
        else
            if ( [ ! "$FINAL_ONLY" ] && ( [ "$INITIAL_ONLY" ] || [ "$USER_ONLY" ] ) ) ; then
                return 1
            fi
        fi
    else
        if ( [ ! "$USER_ONLY" ] && ( [ "$FINAL_ONLY" ] || [ "$INITIAL_ONLY" ] ) ) ; then
            return 1
        fi
    fi

    return 0
}



# Main program.

if [ "$USER" = 'root' ] ; then
    cat 1>&2 <<EOF
Only some scripts run by this program need elevated privileges. Run this
command as a suitable unprivileged user and prompting for additional privileges
will occur.
EOF
    exit 1
fi

# Show any help text, if requested, then exit.

if [ "$1" = '--help' ] ; then
    cat <<EOF
Usage: $PROGNAME [ -f ] [ -i ] [ -n ] [ -u ]

Show or run the deployment scripts in order.

If -n is indicated the script invocations will be listed for inspection;
otherwise, the scripts will be run in the listed order.

Where -f is indicated, only privileged scripts at the end of the list will be
selected, these typically finalising the deployment process.

Where -i is indicated, only privileged scripts at the start of the list will be
selected, these typically initialising the system for the deployment process.

Where -u is indicated, only unprivileged scripts in the list will be selected.

Where -f, -i and -u are combined, all scripts corresponding to the indicated
options will be selected.
EOF
    exit 0
fi

ECHO=
FINAL_ONLY=
INITIAL_ONLY=
USER_ONLY=

while getopts finu NAME ; do
    case "$NAME" in
        f) FINAL_ONLY=yes ;;
        i) INITIAL_ONLY=yes ;;
        n) ECHO=echo ;;
        u) USER_ONLY=yes ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

# Write script output to a dedicated directory.

if [ ! -e "$LOGS" ] ; then
    mkdir -p "$LOGS"
fi

# Process the scripts in order.

for FILENAME in `find "$DEPLOY" -maxdepth 1 -type f | sort` ; do
    SCRIPT=`basename "$FILENAME"`

    # Run NN-root-* scripts as root.
    # Run NN-user-* scripts as the current user.

    if privileged_script "$SCRIPT" ; then
        PRIV=sudo
    elif unprivileged_script "$SCRIPT" ; then
        PRIV=
    else
        continue
    fi

    # Test the script selection parameters.

    if ! selected_script "$SCRIPT" "$PRIV" ; then
        continue
    fi

    # Define a log file and either show or run the script.

    LOGFILE="$LOGS/$SCRIPT"

    if [ "$ECHO" ] ; then
        $ECHO $PRIV "$FILENAME"
    else
        echo "Executing script: $SCRIPT" 1>&2

        if ! $PRIV "$FILENAME" $* > "$LOGFILE" 2>&1 ; then
            echo "Script failed: $SCRIPT" 1>&2
            echo "See the log file for details: $LOGFILE" 1>&2
            exit 1
        fi
    fi
done

# vim: tabstop=4 expandtab shiftwidth=4
