# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf.urls import url, include
from django.views.decorators.cache import cache_page
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

# Cache timeout in seconds

CACHE_TIMEOUT = 60 * 60 * 24 * 1

def view(obj):
    return cache_page(CACHE_TIMEOUT)(obj.as_view())

# Application details and structure.

app_name = "restapi.v1"

urlpatterns_api = [
    url(r'^v1/collections/?$', view(views.CollectionListViewSet), name='list-collections'),
    url(r'^v1/collections/(?P<collection>\w+)/$', view(views.CollectionMatrixListViewSet), name='collection-detail'),

    url(r'^v1/infer/(?P<sequence>\w+)/$', view(views.MatrixInferenceViewSet), name='matrix-infer'),

    url(r'^v1/matrix/?$', view(views.MatrixListViewSet), name='matrix-list'),
    url(r'^v1/matrix/(?P<base_id>\w+)/versions/$', view(views.MatrixVersionsViewSet), name='matrix-versions'),
    url(r'^v1/matrix/(?P<matrix_id>.+)/$', view(views.MatrixDetailsViewSet), name='matrix-detail'),

    url(r'^v1/releases/?$', view(views.ReleaseListViewSet), name='release-list'),
    url(r'^v1/releases/(?P<release_number>.+)/$', view(views.ReleaseDetailsViewSet), name='release-detail'),

    url(r'^v1/sites/(?P<matrix_id>.+)/$', view(views.MatrixSitesViewSet), name='matrix-sites'),

    url(r'^v1/species/?$', view(views.SpeciesListViewSet), name='species-list'),
    url(r'^v1/species/(?P<tax_id>\w+)/$', view(views.SpeciesMatrixListViewSet), name='specie-detail'),

    url(r'^v1/taxon/?$', view(views.TaxonListViewSet), name='list-taxon'),
    url(r'^v1/taxon/(?P<tax_group>\w+)/$', view(views.TaxonMatrixListViewSet), name='taxon-detail'),

    url(r'^v1/tffm/?$', view(views.TFFMListViewSet), name='tffm-list'),
    url(r'^v1/tffm/(?P<tffm_id>.+)/$', view(views.TFFMDetailsViewSet), name='tffm-detail'),
    ]

urlpatterns_api = format_suffix_patterns(urlpatterns_api, allowed=['json','jsonp','jaspar','meme','pfm','transfac','bed','yaml','api'])

# Prefix the API resources to configure the live API properly.

urlpatterns_api_prefixed = [url("^api/", include(urlpatterns_api))]

urlpatterns = urlpatterns_api + [
    url(r'^clients/?$', views.api_clients, name='api-clients'),
    url(r'^contact-us/?$', views.api_contactus, name='api-contactus'),
    url(r'^home/?$', views.api_homepage, name='api-homepage'),
    url(r'^overview/?$', views.api_overview, name='api-overview'),
    url(r'^v1/?$', views.api_homepage, name='api-homepage'),
    url(r'^v1/live-api/?$', views.api_live(urlpatterns_api_prefixed), name='api-live'),
    ]
