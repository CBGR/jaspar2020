#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

DB=$1
TABLE=$2
FILENAME=$3

if [ "$1" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $0 <database filename> <database table> <filename>
EOF
    exit 1
fi

if [ ! "$DB" ] || [ ! -e "$DB" ] ; then
    cat 1>&2 <<EOF
The database "$DB" was either omitted or is missing.
EOF
    exit 1
fi

if [ ! "$TABLE" ] ; then
    cat 1>&2 <<EOF
A database table must be indicated.
EOF
    exit 1
fi

if [ ! "$FILENAME" ] || [ ! -e "$FILENAME" ] ; then
    cat 1>&2 <<EOF
The filename "$FILENAME" was either omitted or is missing.
EOF
    exit 1
fi

# Remove all previous table data.

sqlite3 "$DB" "delete from $TABLE"

# Import the table data.

TAB=`printf '\t'`

sqlite3 -separator "$TAB" "$DB" ".import $FILENAME $TABLE"
