# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

"""
WSGI config for jaspar project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

from django.core.wsgi import get_wsgi_application
from os.path import abspath, dirname, join
import os, sys

# Add the local library hierarchy to the executable search path in order to be
# able to use various programs employed by the profile inference tool/library.

BASE_DIR = dirname(dirname(abspath(__file__)))
BIN_DIR = join(BASE_DIR, "lib", "usr", "local", "bin")
os.environ["PATH"] += ":%s" % BIN_DIR

RELEASE = "2022"

sys.path.append(join(BASE_DIR, "profile-inference", RELEASE))

# Set the Django configuration.

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "jaspar.settings%s" % RELEASE)

application = get_wsgi_application()
