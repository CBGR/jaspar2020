#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
TOOLS=`realpath "$BASEDIR/../jaspar_tools"`

# Repository details if the code needs to be retrieved.

TOOLS_REPOSITORY="https://bitbucket.org/CBGR/jaspar_tools.git"
CHANGESET="c91158cf759c28bf03dc8042acef099a13a8f1d6"

# Obtain tools.

if [ ! -e "$TOOLS" ] ; then
    git clone "$TOOLS_REPOSITORY" "$TOOLS"
else
    cd "$TOOLS"

    # Update the repository if it has a remote.

    if [ `git remote` ] ; then
        git fetch
    fi
fi

# Obtain the indicated version.

cd "$TOOLS"
git checkout "$CHANGESET"

# Run the deployment script to initialise required data.

"$TOOLS/deploy.sh"

# vim: tabstop=4 expandtab shiftwidth=4
