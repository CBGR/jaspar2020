#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain any site identifier and use that instead of the default.

SITE=`cat "$THISDIR/work/site.txt"`

# Determine available site versions plus the generic site.

for WSGI in "$BASEDIR/jaspar/wsgi"*".py" ; do
    BASENAME=`basename "$WSGI" .py`
    VERSION=${BASENAME#wsgi}

    "$THISDIR/tools/init_site" "$SITE" "$VERSION"
done

# vim: tabstop=4 expandtab shiftwidth=4
