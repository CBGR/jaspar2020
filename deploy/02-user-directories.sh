#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

APACHE=`apache_user_name`

# Initialise directories.

for DIRNAME in temp ; do
    DIRPATH="$BASEDIR/$DIRNAME"

    # Create the directory, set an ACL for Apache.

    mkdir "$DIRPATH"
    setfacl -m "u:$APACHE:rwx" "$DIRPATH"
done

# vim: tabstop=4 expandtab shiftwidth=4
