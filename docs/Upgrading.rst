Upgrading or Introducing Data
=============================

Currently, JASPAR data is retained by the repository, although this situation
may be changed in the future. The ``download`` directory contains several
directories, each with different types of data relevant to the matrix
profiles:

===================== =======================================================
Directory             Contents
===================== =======================================================
``all_files``         Position Weighted Matrix files
``bed_files``         Binding site details in BED format
``centrality_files``  PNG files showing ChIP-seq centrality plots
``collections``       Matrix profile details for non-CORE collections
``CORE``              Matrix profile details for the CORE collection
``database``          Database dump files containing the JASPAR metadata
``sites``             Binding site details in FASTA format
``TFFM``              Transcription Factor Flexible Model files
===================== =======================================================

Some of these directories are used to create downloadable archives. A
deployment_ script performs this archiving so that such archives need not be
retained by the repository (which would be duplicating the data already stored
there).


Changelog
---------

With new data introduced, the changelog should be updated. Unlike the blog
articles (which are held within the database), this is maintained as content
in the ``portal/templates/portal/changelog.html`` file.


.. _deployment: Deployment.rst
