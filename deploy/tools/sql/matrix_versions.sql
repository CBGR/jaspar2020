-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Populate the matrix versions table with the release years that each matrix
-- profile version is present. The 2.1 release is referenced using the special
-- designation of 2004 to fit in with the chronological labelling.

insert into matrix_version (id, release)
    select M.id,
        case release
            when '2.1' then '2004'  -- expose using the year
            else release
        end
    release
    from matrix as M
    inner join (

        -- Create version entries in subsequent releases.

        select distinct B.base_id as base_id, B.version as version, A.release as release
        from matrix_history as A
        inner join matrix_history as B
            on A.base_id = B.base_id and A.release >= B.release

        ) as H
        on M.base_id = H.base_id
        and M.version = H.version;

-- vim: tabstop=4 expandtab shiftwidth=4
