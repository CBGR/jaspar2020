{% extends 'portal/base.html' %}

{% load static %}

{% block title %}
Frequently Asked Questions
{% endblock %}

{% block content_header %}
FAQ
{% endblock %}

{% block breadcrumb %}
  <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
  <li class="active">FAQ</li>
{% endblock %}

{% block content %}

    <div class="row">

      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fas fa-question"></i> Frequently Asked Questions</h3>
          </div>

          <div class="box-body">
            <div id="how-do-i-cite-jaspar">
              <h3>How do I cite JASPAR?</h3>

              <p>If you simply want to acknowledge that you used the latest
              version of the database, use this citation:</p>

              {% with release=releases|last %}

              <p>{{ release.authors }} <b>{{ release.title }}</b>
              <i>{{ release.journal }}</i> {{ release.journal_details }}; doi:

              <a href="https://doi.org/{{ release.doi }}" target="_blank">{{ release.doi }}</a></p>

              {% endwith %}

              <p>The complete list of citations are as follows:</p>

              <dl>

                {% for release in releases %}

                <dt style="font-weight: normal">{{ release.notes }}:</dt>

                <dd style="margin: 0.5em 0 1em 1em">

                  {{ release.authors }} <b>{{ release.title }}</b>
                  <i>{{ release.journal }}</i> {{ release.journal_details }}; doi:

                  <a href="https://doi.org/{{ release.doi }}" target="_blank">{{ release.doi }}</a></dd>

                {% endfor %}

              </dl>
            </div>

            <div id="what-motif-formats-does-jaspar-support">
              <h3>What motif formats does JASPAR support?</h3>

              <p>The data from JASPAR can be downloaded in four different
              motif formats: raw, JASPAR, TRANSFAC, and MEME. Read more about
              motif formats in the JASPAR documentation

              <a href="{{ request.get_host }}/docs" target="_blank">here</a>.</p>

            </div>

            <div id="how-are-tfbss-for-a-profile-in-jaspar-computed">
              <h3>How are TFBSs for a profile in JASPAR computed?</h3>

              <p>JASPAR provides genomic TFBS predictions for 8 organisms
              (Arabidopsis thaliana, Caenorhabditis elegans, Ciona
              intestinalis, Danio rerio, Drosophila melanogaster, Homo
              sapiens, Mus musculus, and Saccharomyces cerevisiae) with the
              JASPAR CORE PFMs associated with the same taxon.</p>

              <p>DNA sequences were scanned with JASPAR CORE TF-binding
              profiles for each taxa independently using

              <a href="https://academic.oup.com/bioinformatics/article/34/14/2483/4921176" target="_blank">PWMScan</a>.

              We selected TFBS predictions with a PWM relative score ≥ 0.8 and
              a p-value &lt; 0.05.</p>

            </div>

            <div id="how-should-i-interpret-the-tfbss-score">
              <h3>How should I interpret the TFBSs score?</h3>

              <p>The score (weight) of each binding site is calculated with
              the following formula:</p>

              <p>Weight = log( P(Site|Matrix) / P(Site|Background) )</p>

              <p>Where:</p>

              <p>
                <ul>

                  <li><i>P(Site|Matrix)</i> corresponds to the probability of
                    observing a sequence given the frequencies at the
                    PFMs.</li>

                  <li><i>P(Site|Background)</i> corresponds to the probability
                    of observing a sequence given the frequencies described by
                    the background model.</li>

                </ul>
              </p>

              <p>The range of weights observed in long matrices (e.g., REST,
              with 21 nt) will be higher than those observed in shorter
              matrices (SOX2, 11 nt), for this reason a threshold based
              directly on the weight is not optimal. Since each motif has its
              own size and information content, this critically influences the
              expected distribution of weights.</p>

              <p>There are two solutions to overcome this challenge:</p>

              <ol>

                <li>Normalize the weight scores. For example, a threshold of
                  W=5 could be good for SOX2 but will be far from optimal for
                  REST, so, to avoid this problem the weights (or scores) are
                  normalized where the highest weight (score) corresponds to a
                  Relative score of 100 or 1000 (depending the granularity).
                  A Relative score of 800 will consider all the binding sites
                  above that number independently of the matrix width and the
                  weight, maybe a Score of 800 in Sox2 corresponds to a weight
                  of 5, whilst in REST corresponds to a weight of 12.</li>

                <li>Calculate a site P-value. This gives a better intuition
                  about the risk associated with each prediction. See

                  <a href="http://www.almob.org/content/2/1/15" target="_blank">10.1186/1748-7188-2-15</a>

                  and

                  <a href="https://www.nature.com/articles/nprot.2008.97" target="_blank">10.1038/nprot.2008.97</a>.</li>

              </ol>

              <p>The relative score Srel is computed as Srel = (W - min) /
              (max - min) where W is the score of the sequence given the PWM,
              min (max) is the minimal (maximal) score that can be obtained
              from the PWM.</p>

            </div>

            <div id="where-do-i-download-tfbss-for-a-profile-in-jaspar">
              <h3>Where do I download TFBSs for a profile in JASPAR?</h3>

              <p>The TFBS predictions associated with all PFMs are available <a href="http://expdata.cmmt.ubc.ca/JASPAR/downloads/UCSC_tracks/2022/" target="_blank">here</a> (note that they are not available to download through this website). We provide JASPAR TFBS predictions as genomic tracks, which can be visualized in genome browsers. Notably, the UCSC Genome Browser now presents predicted human JASPAR TFBS data as a native track for the human genome with information such as TF names, TFBS prediction scores, and PFM logo for each of the 12+ billion predictions. For details check documentation for genomic tracks.</p>

            </div>

            <div id="where-do-I-download-the-sequences-used-to-construct-the-JASPAR-PFMs">
              <h3>Where do I download the sequences used to construct the JASPAR PFMs?</h3>

              <p>If a profile was obtained using our pipeline on ChIP-seq/exo data, the corresponding sequences can be downloaded in BED and FASTA formats for individual profiles. See an example figure below. All available sequences can also be downloaded in the <a href="http://jaspar.genereg.net/downloads/" target="_blank">Download Data</a> section under “Other data”.</p>

              <img src="{% static 'img/tfbss_faq.png' %}" alt="tfbss_faq img" width="99%" />

            </div>

            <div id="why-are-certain-sequences-not-downloadable-from-jaspar-core">
              <h3>Why are certain sequences not downloadable from JASPAR CORE?</h3>

              <p>This is due to historical reasons. JASPAR CORE was originally
              built in order to create familial binding profiles for as many
              structural classes of transcription factors as possible. In some
              experimental literature, for example motifs generated from PBM
              and SELEX, only matrices and not sequences are available. For
              recent additions, for all the internally generated motifs
              (derived from ChIP-seq/-exo) it is a requirement to have the
              sequences (and the genomic coordinates) used to build the
              PFMs.</p>

            </div>

            <div id="why-is-my-matrix-study-not-included-in-jaspar-core">
              <h3>Why is my matrix study not included in JASPAR CORE?</h3>

              <p>There are two principal explanations. The most likely is that
              we were not aware of your work: please

              <a href="{{ request.get_host }}/contact-us" target="_blank">let us know!</a>

              The other possible reason is that the publication did not live
              up to the demands of the curators. As we have human curation of
              all JASPAR CORE matrices, this is to some degree an arbitrary
              call – we are happy to discuss it with you.</p>

              <p>Another possibility is that your motif is part of the

              <a href="{{ request.get_host }}/collection/unvalidated/" target="_blank">Unvalidated</a>

              collection where we store motifs without orthogonal literature
              support. In case your motifs of interest are part of the
              Unvalidated collection, please fill the fields provided at the
              ‘Community curation’ section at each motif page (e.g., a
              literature support for your motif) so our curation team can have
              a look and evaluate its further incorporation to the CORE
              collection.</p>

              <img src="{% static 'img/unvalidated_faq.png' %}" alt="unvalidated_faq img" width="99%" />

              <p>Each motif in the Unvalidated collection has a dedicated
              section where the user community may share information to add
              these motifs to the CORE collection in future JASPAR
              releases.</p>

            </div>

            <div id="linking-web-services-to-cpu-intensive-services-within-jaspar">
              <h3>How to link web services to CPU-intensive services within JASPAR?</h3>

              <p>We appreciate that other services want to link to JASPAR.
              However, if your are using the CPU-intensive services (matrix
              comparison, randomization or clustering), please ask the
              maintainers (you can find contact information

              <a href="{{ request.get_host }}/contact-us" target="_blank">here</a>)

              before you do this – otherwise your server might be rejected
              without warning. In that case, we strongly suggest setting up a
              local JASPAR database, as the database and resources are freely
              available.</p>

            </div>

            <div id="who-is-jaspar-anyhow">
              <h3>Who is JASPAR anyhow?</h3>

              <p>JASPAR was originally the name of a master student project
              algorithm for comparing matrix profiles, an obscure tribute to
              an even more obscure dialog from the Black Adder episode “The
              Black Seal” between the Seven Most Evil Men in the Kingdom:</p>

              <ul>
              <li><p>…and with all haste, we will meet at Old Jaspar’s tavern</p></li>
              <li><p>How is old Jaspar these days?</p></li>
              <li><p>Dead.</p></li>
              <li><p>How?</p></li>
              <li><p>I killed him.</p></li>
              <li><p>[Loud cheer].</p></li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div id="jaspar_forum">
        <p>Did not find what you were looking for? You can also check or ask
        your question in the
        <a href="https://groups.google.com/g/jaspar" target="_blank">JASPAR Q&A Forum.</a></p>
      </div>

      <div class="col-md-4 col-lg-4 col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fas fa-list"></i> List of questions </h3>

          </div>
          <div class="box-body">
            <div id="TOC">
              <ul>
              <li><a href="#how-do-i-cite-jaspar">How do I cite JASPAR?</a></li>
              <li><a href="#what-motif-formats-does-jaspar-support">What motif formats does JASPAR support?</a></li>
              <li><a href="#how-are-tfbss-for-a-profile-in-jaspar-computed">How are TFBSs for a profile in JASPAR computed?</a></li>
              <li><a href="#how-should-i-interpret-the-tfbss-score">How should I interpret the TFBSs score?</a></li>
              <li><a href="#where-do-i-download-tfbss-for-a-profile-in-jaspar">Where do I download TFBSs for a profile in JASPAR?</a></li>
              <li><a href="where-do-I-download-the-sequences-used-to-construct-the-JASPAR-PFMs" target="_blank">Where do I download the sequences used to construct the JASPAR PFMs?</a></li>
              <li><a href="#why-are-certain-sequences-not-downloadable-from-jaspar-core">Why are certain sequences not downloadable from JASPAR CORE?</a></li>
              <li><a href="#why-is-my-matrix-study-not-included-in-jaspar-core">Why is my matrix study not included in JASPAR CORE?</a></li>
              <li><a href="#linking-web-services-to-cpu-intensive-services-within-jaspar">How to link web services to CPU-intensive services within JASPAR?</a></li>
              <li><a href="#who-is-jaspar-anyhow">Who is JASPAR anyhow?</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

{% endblock %}
