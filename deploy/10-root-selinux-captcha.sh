#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`

. "$THISDIR/tools/system.sh"

# Allow Apache-resident application to be able to connect to the CAPTCHA server.

if have setsebool ; then
    setsebool -P httpd_can_network_connect on
fi

# vim: tabstop=4 expandtab shiftwidth=4
