# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django import forms
from .models import Matrix, MatrixAnnotation
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Invisible, ReCaptchaV2Checkbox

class CollectionSelector:
    '''
    A class providing a list of collection names.
    '''

    def choices(self):
        return Matrix.objects.values_list('collection', 'collection').distinct()

class TaxGroupSelector:
    '''
    A class providing a list of taxonomy group details.
    '''

    def choices(self):
        return MatrixAnnotation.objects.filter(tag='tax_group').values('val').distinct().order_by('val').values_list('val', 'val')

class ContactForm(forms.Form):
    '''
    Form for contact us page
    '''
    #from_name = forms.CharField(label='Your name', required=True, max_length=100)
    from_email = forms.EmailField(required=True, label='Your email')
    subject = forms.CharField(label='Subject', required=False, max_length=100)
    message = forms.CharField(label='Your message/feedback', required=True, widget=forms.Textarea)


class InferenceForm(forms.Form):
    '''
    Form for profile inference page 
    '''
    sequence = forms.CharField(label='Paste a protein sequence below', required=True, widget=forms.Textarea)

class AlignForm(forms.Form):
    '''
    Form for matrix align page
    '''
    #matrix = forms.CharField(label='Paste custom matrix or IUPAC string', required=True, widget=forms.Textarea)
    collection = forms.ChoiceField(label='Select collection',
        choices=CollectionSelector().choices,
        initial='CORE')

    tax_groups = forms.MultipleChoiceField(label='Select Tax group(s)',
        choices=TaxGroupSelector().choices,
        required=True)

    version = forms.CharField(label='Select version', widget=forms.Select(
        choices=(('latest','Latest versions (non-redundant)'),('all','All versions (redundant)'))
        ))


class SearchForm(forms.Form):
    '''
    Form for advanced search options
    '''
    #Experiment type
    tf_type = forms.ModelChoiceField(queryset=MatrixAnnotation.objects.filter(tag='type').values('val').distinct(), to_field_name='val')

    #TF Class
    tf_class = forms.ModelChoiceField(queryset=MatrixAnnotation.objects.filter(tag='class').values('val').distinct(), to_field_name='val')

    #Family
    tf_family = forms.ModelChoiceField(queryset=MatrixAnnotation.objects.filter(tag='family').values('val').distinct(), to_field_name='val')

class CurationForm(forms.Form):
    '''
    Form for profile inference page 
    '''
    message = forms.CharField(label='Please provide any information here which will help to validate this profile.', required=True, widget=forms.Textarea(attrs={'rows':4}))
    captcha = ReCaptchaField(label='', widget=ReCaptchaV2Checkbox, required=True)

# vim: tabstop=4 expandtab shiftwidth=4
