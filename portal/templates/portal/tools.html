{% extends 'portal/base.html' %}

{% load static %}

{% block title %}
Tools
{% endblock %}

{% block content_header %}
JASPAR tools
{% endblock %}

{% block breadcrumb %}
  <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
  <li class="active">Tools</li>
{% endblock %}

{% block content %}

<div class="row">

  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">JASPAR software tools</h3>
      </div>

      <div class="box-body">

        <p>JASPAR is supported by a growing number of open source
        software tools and APIs implemented in various programming
        languages including Perl, Python/Biopython, R/Bioconductor and
        Ruby. In addition, the TFBS enrichment tool was introduced in
        the 2022 release. Each of those resources are described more
        in detail below.</p>

        <div id="restful-api" class="section level1">
          <h3>RESTful API</h3>

          <p><strong>JASPAR {{ release.year }}</strong> comes with a
          Representational State Transfer (REST) application programming
          interface (API) to access the JASPAR database programmatically

          (<a href="https://doi.org/10.1093/bioinformatics/btx804" target="_blank">Khan A. et al. 2017</a>).

          The RESTful API enables programmatic access to JASPAR by most
          programming languages and returns data in seven widely used
          formats including JSON, JSONP, JASPAR, MEME, PFM, TRANSFAC, and
          YAML. Further, it provides a browsable interface for
          bioinformatics tool developers. The API is freely accessible at

          <strong><a href="https://{{ request.get_host }}/api/" target="_blank" >https://{{ request.get_host }}/api/</a></strong>. To read more about JASPAR RESTful API visit its <a href="https://{{ request.get_host }}/api/v1/docs/" target="_blank">documentation</a> page. If you wish to cite JASPAR RESTful-API, please check the <a href="https://{{ request.get_host }}/faq/" target="_blank">FAQ page</a>.</p>

        </div>

        <div id="pyjaspar" class="section level1">
          <h3>pyJASPAR</h3>

          <p><strong>pyJASPAR</strong> is a Pythonic interface to JASPAR
          transcription factor motifs. It uses Biopython and SQLite3 to
          provide a serverless interface to JASPAR database to query and
          access TF motif profiles across various releases of JASPAR. The
          releases currently available are: JASPAR2014, JASPAR2016,
          JASPAR2018, and JASPAR2022. The pyJASPAR package will be
          updated when future JASPAR releases become available.</p>

          <p>The releases currently available are:
          <strong>JASPAR2014</strong>, <strong>JASPAR2016</strong>,
          <strong>JASPAR2018</strong>, and <strong>JASPAR2022</strong>.
          The pyJASPAR package will be updated when future JASPAR releases
          become available.</p>

          <p>pyJASPAR can be easily installed using Bioconda:</p>

          <pre>conda install -c bioconda pyjaspar</pre>

          <p>Or via PyPI:</p>

          <pre>pip install pyjaspar</pre>

          <p>For more details please check the

          <a href="http://pyjaspar.rtfd.io" target="_blank">documentation</a>

          and

          <a href="http://github.com/asntech/pyjaspar" target="_blank">source code</a>.</p>

        </div>

        <div id="tfbs-enrichment" class="section level1">
          <h3>TFBS enrichment analysis</h3>

          <p>The <strong>JASPAR enrichment tool</strong> predicts which
          sets of TFBSs from the CORE collection in the JASPAR database
          are enriched in a set of given genomic regions. Enrichment
          computations are performed using the LOLA tool. The tool allows
          for two types of computations:</p>

          <ol>
            <li>Enrichment of TFBSs in a set of genomic regions compared
            to a given universe of genomic regions.</li>
            <li>Differential TFBS enrichment when comparing one set of
            genomic regions (set1) to another (set2).</li>
          </ol>

          <p>To read more about the JASPAR TFBS enrichment analysis, visit
          the enrichment analysis

          <a href="https://{{ request.get_host }}/enrichment/" target="_blank">page</a>

          and

          <a href="https://bitbucket.org/CBGR/jaspar_enrichment/src/master/" target="_blank">repository</a>.</p>

        </div>

        <div id="bioconductor-tfbstools-package" class="section level1">
          <h3>Bioconductor TFBSTools package</h3>

          <p><strong>TFBSTools</strong> is an R/Bioconductor package for
          the analysis and manipulation of transcription factor binding
          sites and their associated transcription factor profile
          matrices. <strong>TFBStools</strong> provides a toolkit for
          handling TFBS profile matrices, scanning sequences and
          alignments including whole genomes, and querying the JASPAR
          database. The functionality of the package can be easily
          extended to include advanced statistical analysis, data
          visualisation and data integration. For more information on the
          package and how to install it, please visit

          <a href="http://bioconductor.org/packages/TFBSTools/" target="_blank">http://bioconductor.org/packages/TFBSTools/</a>.</p>

          <p>To retrieve data from the JASPAR database, we also provided
          the Bioconductor data packages. Currently four JASPAR releases
          are available:</p>

          <ul>
            <li><a href="http://bioconductor.org/packages/JASPAR2020/" target="_blank">JASPAR2020</a></li>
            <li><a href="http://bioconductor.org/packages/JASPAR2018/" target="_blank">JASPAR2018</a></li>
            <li><a href="http://bioconductor.org/packages/JASPAR2016/" target="_blank">JASPAR2016</a></li>
            <li><a href="http://bioconductor.org/packages/JASPAR2014/" target="_blank">JASPAR2014</a></li>
          </ul>

        </div>

        <div id="biopython-module" class="section level1">
          <h3>Biopython module</h3>

          <p>The Biopython package <em>Bio.motifs</em> has a subpackage
          dedicated to JASPAR, which is called <em>Bio.motifs.jaspar</em>.
          It allows the retrieval of profiles from the JASPAR database as
          well as reading and writing motifs in various flat file formats.
          To read more about this module, please visit

          <a href="https://biopython.org/docs/1.75/api/Bio.motifs.jaspar.html" target="_blank">https://biopython.org/docs/1.75/api/Bio.motifs.jaspar.html</a>.</p>

        </div>

        <div id="tfbs-module" class="section level1">
          <h3>TFBS Perl/TFBSTools Bioconductor module</h3>

          <p>The <a href="http://tfbs.genereg.net/" target="_blank">Perl
            TFBS module</a> provides functionality for a large number of
          tasks including scanning sequences and alignments for putative
          TFBS. It includes a JASPAR interface module, TFBS::DB::JASPAR6
          for retrieving binding site profiles from the JASPAR database.
          Currently this Perl module is not under active development. All
          the functionality can be found in the

          <a href="https://bioconductor.org/packages/release/bioc/html/TFBSTools.html" target="_blank">TFBSTools Bioconductor</a>

          package. Users are highly encouraged to switch to TFBSTools.
          Full documentation available in their dedicated websites.</p>

        </div>

        <div id="ruby-gem" class="section level1">
          <h3>Ruby Gem</h3>

          <p>A Ruby gem providing basic functionality for parsing, searching, and comparing JASPAR motifs. To learn more about this tool please visit <a href="https://rubygems.org/gems/bio-jaspar" target="_blank">https://rubygems.org/gems/bio-jaspar</a> and the repository at <a href="https://github.com/wassermanlab/jaspar-bioruby" target="_blank">https://github.com/wassermanlab/jaspar-bioruby</a>.</p>

        </div>

        <div id="transcript-factor-flexible-models-tffm" target="_blank" class="section level1">
          <h3>Transcript Factor Flexible Models (TFFM)</h3>

          <p>TFFMs were introduced in JASPAR2016 (6th release). TFFMs are hidden Markov-based models capturing dinucleotide dependencies in TF-DNA interactions, which have been recurrently shown to occur within TFBSs and are not captured by classical PFMs. The TFFMs need to be initialized with a PFM and trained on ChIP-seq data.</p>

          <p>See the <a href="https://doi.org/10.1371/journal.pcbi.1003214" target="_blank">Mathelier and Wasserman publication</a> for a complete description of the method and its repository at <a href="https://github.com/wassermanlab/TFFM" target="_blank">https://github.com/wassermanlab/TFFM</a>.</p>
          
        </div>
      </div>
    </div>
  </div>
</div>

{% endblock %}
