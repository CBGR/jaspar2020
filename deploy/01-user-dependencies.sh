#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

# Initialise local dependencies.

PIP="pip3"
SYSTEM=`pip_system_flag`

# Conda may provide some dependencies with the rest being provided by pip as a
# conda package.

if have conda ; then
    REQUIREMENTS="$BASEDIR/`conda_requirements`"

    if [ -e "$REQUIREMENTS" ] ; then
        conda create -y -p "$BASEDIR/lib/usr/local"
        xargs conda install -y -p "$BASEDIR/lib/usr/local" < "$REQUIREMENTS"
    fi
fi

# Install Python packages using pip.

REQUIREMENTS="$BASEDIR/`python_requirements`"

if [ -e "$REQUIREMENTS" ] ; then
    "$PIP" install $SYSTEM --root "$BASEDIR/lib" -r "$REQUIREMENTS"
fi

# Fix up paths for Debian's own Python.

if system_is_debian ; then
    for DIST_PACKAGES in `find "$BASEDIR/lib" -name 'dist-packages' -type d` ; do
        LIBDIR=`dirname "$DIST_PACKAGES"`
        SITE_PACKAGES="$LIBDIR/site-packages"
        if [ ! -e "$SITE_PACKAGES" ] ; then
            ln -s 'dist-packages' "$SITE_PACKAGES"
        fi
    done
fi

# vim: tabstop=4 expandtab shiftwidth=4
