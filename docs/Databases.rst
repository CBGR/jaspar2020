Databases
=========

Information about the databases available to the JASPAR Web application can be
found in the ``jaspar/settings.py`` file, provided by the ``DATABASES``
setting.

At the very least, a ``default`` database must be available. For example:

.. code-block:: python

  DATABASES = {
      'default': {
          'ENGINE': 'django.db.backends.sqlite3',
          'NAME': os.path.join(BASE_DIR, 'JASPAR2020.sqlite'),
      },
      'JASPAR2018': {
          'ENGINE': 'django.db.backends.sqlite3',
          'NAME': os.path.join(BASE_DIR, 'JASPAR2018.sqlite'),
      }
  }

The existence of other database definitions is important to the REST API
because it is possible to indicate the release (or version) of the database in
the REST API in order to retrieve data from a particular release. In the
above, ``JASPAR2018`` is defined, permitting queries against that database in
the REST API.

The function selecting the database name can be found in the
``restapi/v1/views.py`` file and is called ``_get_jaspar_release``. This is
employed in the querying operations by the ``using`` queryset method.
