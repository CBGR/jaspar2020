# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Make sure the following:
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User



# Gene, synonym and profile information permitting synonym searches.

# Gene information used to organise synonyms.

class Gene(models.Model):
    gene_id = models.IntegerField(primary_key=True)
    tax_id = models.IntegerField()
    symbol = models.TextField()

    class Meta:
        db_table = 'genes'

# Synonyms for each gene.

class Synonym(models.Model):
    id = models.IntegerField(primary_key=True)
    gene = models.ForeignKey(Gene, db_column='gene_id', related_name='synonyms', on_delete=models.CASCADE)
    synonym = models.TextField()

    class Meta:
        db_table = 'gene_synonyms'

#Model for MATRIX table in the database
class Matrix(models.Model):
    collection_choices = (
        ('CORE', 'CORE'),
        ('CNE', 'CNE'),
        ('PHYLOFACTS', 'PHYLOFACTS'),
        ('SPLICE', 'SPILCE'),
        ('POLII', 'POLII'),
        ('FAM','FAM'),
        ('PBM','PBM'),
        ('PBM_HOMEO','PBM_HOMEO'),
        ('PBM_HLH','PBM_HLH'),
        ('UNVALIDATED', 'UNVALIDATED')
    )
    id = models.CharField(db_column='ID', primary_key=True, max_length=16)
    collection = models.CharField(db_column='COLLECTION', max_length=16, blank=True, choices=collection_choices)
    base_id = models.CharField(db_column='BASE_ID', max_length=16)
    version = models.CharField(db_column='VERSION', max_length=16)
    name = models.CharField(db_column='NAME', max_length=255)
    genes = models.ManyToManyField(Gene, related_name='profiles', through='ProfileGene')

    class Meta:
        managed = False
        db_table = 'MATRIX'
        verbose_name_plural = "matrices"

    def __str__(self):
        return self.name+'_'+self.base_id+'.'+str(self.version)

#Model for MATRIX_ANNOTATION table in the database
class MatrixAnnotation(models.Model):

    tag_choices = (
        ('class','Class'),
        ('comment','Comment'),
        ('family','Family'),
        ('gc_content','GC Content'),
        ('medline','Medline'),
        ('tax_group','TAX Group'),
        ('tfbs_shape_id','TFBS Shape ID'),
        ('type','Type'),
        ('pazar_tf_id','PAZAR TF ID'),
        ('alias','Alias'),
        ('description','Description'),
        ('symbol','Symbol'),
        ('centrality_logp','Centrality Logp'),
        ('source','Source'),
        ('consensus','Consensus'),
        ('jaspar','JASPAR'),
        ('unibind','UniBind'),
        ('mcs','MCS'),
        ('transfac','TRANSFAC'),
        ('end_relative_to_tss','End relative to TSS'),
        ('start_relative_to_tss','Start relative to TSS'),
        ('included_models','Included Models')
    )

    id = models.IntegerField(db_column='LOCAL_ID', primary_key=True)
    matrix_id = models.ForeignKey(Matrix, db_column='ID', help_text='Matrix ID: Name_Base_ID_version', on_delete=models.CASCADE)
        
    #matrix_id = models.IntegerField(db_column='ID', unique=False, help_text='Matrix ID. base_id.version')  
    tag = models.CharField(db_column='TAG', max_length=150, choices=tag_choices)  
    val = models.CharField(db_column='VAL', max_length=255, blank=True, null=True)  

    class Meta:
        #unique_together = (('matrix_id','tag','val'),)
        managed = False
        db_table = 'MATRIX_ANNOTATION'

    def __str__(self):
        #return self.model._meta.unique_together
        return "%s (%s)" % (self.matrix_id, self.tag)

#Model for MATRIX_DATA table in the database
class MatrixData(models.Model):

    matrix_id = models.ForeignKey(Matrix, db_column='ID', on_delete=models.CASCADE)
    row = models.CharField(max_length=1)
    col = models.IntegerField()  
    val = models.FloatField(blank=True, null=True) 
    
    class Meta:
        managed = False
        db_table = 'MATRIX_DATA'
        verbose_name_plural = "matrix data"
        unique_together = (('matrix_id', 'row', 'col',),)

    def __str__(self):
        return 'Matrix Data: ' + str(self.matrix_id)

# Profile history information. Note that this includes removed profiles and does
# not reference the matrix model.

class MatrixHistory(models.Model):
    base_id = models.CharField(db_column='BASE_ID', max_length=16)
    version = models.CharField(db_column='VERSION', max_length=16)
    name = models.CharField(db_column='NAME', max_length=255)
    release = models.CharField(db_column='RELEASE', max_length=16)

    class Meta:
        managed = False
        db_table = 'MATRIX_HISTORY'
        unique_together = (('base_id', 'release',),)

    def _matrix_id(self):
        return '%s.%s' % (self.base_id, self.version)

    matrix_id = property(_matrix_id)

#Model for MATRIX_PROTEIN table in the database
class MatrixProtein(models.Model):
    matrix_id = models.ForeignKey(Matrix, db_column='ID', on_delete=models.CASCADE)
    acc = models.CharField(db_column='ACC', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MATRIX_PROTEIN'

# Profile history information referencing individual matrices.

class MatrixVersion(models.Model):
    id = models.IntegerField(db_column='LOCAL_ID', primary_key=True)
    matrix_id = models.ForeignKey(Matrix, db_column='ID', on_delete=models.CASCADE)
    release = models.CharField(db_column='RELEASE', max_length=16)

    class Meta:
        managed = False
        db_table = 'MATRIX_VERSION'



#Model for TAX table in the database
class Tax(models.Model):
    tax_id = models.CharField(db_column='TAX_ID', primary_key=True, max_length=16)
    species = models.CharField(db_column='SPECIES', max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TAX'

#Model for TAX_EXT table in the database
class TaxExt(models.Model):
    tax_id = models.OneToOneField(Tax, db_column='TAX_ID', primary_key=True, max_length=16, on_delete=models.CASCADE)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TAX_EXT'

#Model for MATRIX_SPECIES table in the database
class MatrixSpecies(models.Model):
    matrix_id = models.ForeignKey(Matrix, db_column='ID', on_delete=models.CASCADE)
    tax_id = models.ForeignKey(Tax, db_column='TAX_ID', max_length=255, blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'MATRIX_SPECIES'

#Model for TFFM table in the database
class Tffm(models.Model):
    id = models.CharField(db_column='ID', primary_key=True, max_length=8)
    base_id = models.CharField(db_column='BASE_ID', max_length=16)
    version = models.CharField(db_column='VERSION', max_length=2)
    matrix_base_id = models.CharField(db_column='MATRIX_BASE_ID', max_length=16)
    matrix_version = models.CharField(db_column='MATRIX_VERSION', max_length=2)
    name = models.CharField(db_column='NAME', max_length=255)
    log_p_1st_order = models.CharField(db_column='LOG_P_1ST_ORDER', max_length=16, blank=True, null=True)
    log_p_detailed = models.CharField(db_column='LOG_P_DETAILED', max_length=16, blank=True, null=True)
    experiment_name = models.TextField(db_column='EXPERIMENT_NAME', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TFFM'

# A one-to-many mapping from JASPAR profiles to genes.

class ProfileGene(models.Model):
    profile = models.ForeignKey(Matrix, db_column='profile_id', related_name='profile_genes', on_delete=models.CASCADE)
    gene = models.ForeignKey(Gene, db_column='gene_id', related_name='gene_profiles', on_delete=models.CASCADE)

    class Meta:
        db_table = 'profile_genes'



# General taxonomy group information.

class TaxGroup(models.Model):
    tax_id = models.IntegerField(db_column='tax_id', primary_key=True)
    name = models.CharField(db_column='scientific_name', max_length=250)
    common_name = models.CharField(db_column='common_name', max_length=250)
    image_name = models.CharField(db_column='image_name', max_length=255, blank=True, null=True)
    image_url = models.CharField(db_column='image_url', max_length=255, blank=True, null=True)
    clustering = models.IntegerField(db_column='clustering',default=0)

    def __str__(self):

        'Return a string representation, used as a form label.'

        return self.name

    class Meta:
        db_table = 'tax_group'



#Model for Post table in the database
class Post(models.Model):
    category_choices = (
        ('Update', 'Update'),
        ('Bug fix', 'Bug fix'),
        ('Announcement', 'Announcement'),
        ('Other', 'Other'),
    )
    title = models.CharField(max_length=100)
    content = models.TextField(blank=True)
    slug = models.SlugField(unique=True)
    category = models.CharField(max_length=150, choices=category_choices)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

#Model for Release table in the database
class Release(models.Model):
    active_choices = (
        ('Yes', 'Yes'),
        ('No', 'No'),   
    )
    release_number = models.IntegerField(primary_key=True)
    year = models.CharField(max_length=4, unique=True)  
    release_date = models.DateField(auto_now_add=False)
    pubmed_id = models.CharField(max_length=15)
    doi = models.CharField(max_length=25)
    website = models.URLField(default='http://jaspar.genereg.net')
    active = models.CharField(max_length=3, choices=active_choices, default='Yes')
    authors = models.TextField()
    title = models.TextField()
    journal = models.TextField()
    journal_details = models.TextField()
    abstract = models.TextField()
    notes = models.TextField()

    def _citation(self):
        return "%s %s %s %s" % (self.authors, self.title, self.journal, self.journal_details)

    citation = property(_citation)

# Feature availability tables.

class ArchetypesRelease(models.Model):
    year = models.OneToOneField(Release, to_field='year', on_delete=models.CASCADE, primary_key=True)

    class Meta:
        db_table = 'archetypes_releases'

class ClusteringRelease(models.Model):
    year = models.OneToOneField(Release, to_field='year', on_delete=models.CASCADE, primary_key=True)

    class Meta:
        db_table = 'clustering_releases'



# Genome-related information.

class GenomeSpecies(models.Model):
    tax_id = models.IntegerField(db_column='tax_id', primary_key=True)
    name = models.CharField(db_column='scientific_name', max_length=250)

    class Meta:
        db_table = 'genome_species'

class Genome(models.Model):
    name = models.CharField(db_column='name', max_length=16, primary_key=True)
    data_tag = models.CharField(db_column='data_tag', max_length=8)
    species = models.ForeignKey(GenomeSpecies, db_column='tax_id', on_delete=models.CASCADE)
    year = models.ForeignKey(Release, to_field='year', on_delete=models.CASCADE)

    class Meta:
        db_table = 'genome'
