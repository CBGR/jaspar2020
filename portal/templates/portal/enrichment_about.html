{% load static %}

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fas fa-info-circle"> </i> About JASPAR TFBS sets enrichment</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">

        <p>The

        <a href="https://bitbucket.org/CBGR/jaspar_enrichment/"><em>JASPAR enrichment</em></a>

        tool predicts which sets of TFBSs from the

        <a href="https://{{ request.get_host }}">JASPAR</a>

        database are enriched in a set of given genomic regions. Enrichment
        computations are performed using the

        <a href="http://code.databio.org/LOLA/">LOLA tool</a>.

        For more information about the underlying enrichment computations, read
        the

        <a href="http://code.databio.org/LOLA/">LOLA documentation</a>.</p>

        <p>The tool allows for two types of computations:</p>

        <ol>
        <li>Enrichment of TFBSs in a set of genomic regions compared to a
            given universe of genomic regions.</li>

        <li>Differential TFBS enrichment when comparing one set of genomic
          regions (S1) to another (S2).</li>
        </ol>

      </div>
    </div>

    <div class="box box-success" id="oneSetBg">
      <div class="box-header with-border">
        <h3 class="box-title">1. Enrichment within a given universe of genomic regions</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">

       <p><img src="{% static 'img/oneSetBg.png' %}" alt="oneSetBg img" width="99%" /></p>

        <p>To compute which sets of TFBSs from JASPAR are enriched in a set
        <em>S</em> of genomic regions compared to a universe <em>U</em> of
        genomic regions, you can use the <em>oneSetBg</em> subcommand as
        follows.</p>

        <pre>bin/JASPAR_enrich.sh oneSetBg &lt;loladb_dir&gt; &lt;S bed&gt; &lt;U bed&gt; &lt;output dir&gt; &lt;API_URL&gt; &lt;n_cores&gt;</pre>

        <p>Here:</p>

        <ul>
          <li><code>loladb_dir</code> is the directory containing all the
            LOLA database batches for an organism and assembly</li>
          <li><code>API_URL</code> is the URL to the matrix API in JASPAR,
            such as <code>{{ request.get_host }}/api/v1/matrix/</code></li>
          <li><code>n_cores</code> is the number of processor cores to use
            when performing the computation</li>
        </ul>

        <p>This will compute the enrichment of TFBS sets from JASPAR in the
        genomic regions from <em>S</em> (provided as a BED file) when compared
        to the expectation from a universe <em>U</em> of genomic regions
        (provided as a BED file). All result files will be provided in the
        <code>&lt;output dir&gt;</code> directory.</p>

        <p><b>Note that every region in <em>S</em> should overlap with one
        region in <em>U</em>.</b></p>

      </div>
    </div>

    <div class="box box-success" id="twoSets">
      <div class="box-header with-border">
        <h3 class="box-title">2. Differential enrichment</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">

        <img src="{% static 'img/oneTwoSets.png' %}" alt="twoSets_img" width="100%"/>

        <p>To compute which sets of TFBSs from JASPAR are enriched in a set
        <em>S1</em> of genomic regions compared to another set <em>S2</em> of
        genomic regions, you can use the <em>twoSets</em> subcommand as
        follows.</p>

        <pre>bash bin/JASPAR_enrich.sh twoSets &lt;loladb_dir&gt; &lt;S1 bed&gt; &lt;S2 bed&gt; &lt;output dir&gt; &lt;API_URL&gt; &lt;n_cores&gt;</pre>

        <p>Here:</p>

        <ul>
          <li><code>loladb_dir</code> is the directory containing all the
            LOLA database batches for an organism and assembly</li>
          <li><code>API_URL</code> is the URL to the matrix API in JASPAR,
            such as <code>{{ request.get_host }}/api/v1/matrix/</code></li>
          <li><code>n_cores</code> is the number of processor cores to use
            when performing the computation</li>
        </ul>

        <p>This will compute the enrichment of TFBS sets from JASPAR in the
        genomic regions from <em>S1</em> (provided as a BED file) when
        compared to the genomic regions in <em>S2</em> (provided as a BED
        file). All result files will be provided in the <code>&lt;output
        dir&gt;</code> directory.</p>

      </div>
    </div>

    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fas fa-folder-open"> </i> JASPAR enrichment output and example</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">

        <h4 id="output">Output</h4>

        <p>The output directory will contain the <em>allEnrichments.tsv</em>
        file provide the enrichment score for each TFBS set from JASPAR along
        with their metadata information. Similar files (following the template
        <em>col_&lt;TF&gt;.tsv</em>) are created for each TF with all data sets
        available for that TF.</p>

        <p>A visual representation of the enrichment analysis is provided in
        the output directory with three plots:</p>

        <ol>

          <li>A

          <a href="{% static 'img/allEnrichments_swarm.pdf' %}"
            target="_blank">swarm plot</a>

          using the log10(p-value) of the enrichment for each TFBS set on the
          y-axis.</li>

          <li>An

          <a href="{% static 'html/JASPAR_enrichment_interactive_beeswarmplot.html' %}"
            target="_blank">interactive beeswarm plot</a>

          showing the 10 most enriched TFs.</li>

          <li>An

          <a href="{% static 'html/JASPAR_enrichment_interactive_ranking.html' %}"
            target="_blank">interactive ranking plot</a>

          showing the log10(p-value) for all the datasets in JASPAR.</li>

        </ol>

        <p>In the three plots, the data sets for the top 10 TFs showing
        log10(p-value) &lt; 3 are highlighted with dedicated colors (one
        colour per TF). Data sets with log10(p-value) &gt; 3 are provided with
        a colour for N.S. (non-significant).</p>

        <p>Users can explore the results with the interactive plots. These
        plots permit the hiding and showing of the top 10 enriched TF
        families, and the tooltip displays the TF name, matrix ID,
        -log10(p-value) (also known as significance), family and class
        annotations.</p>

        <h4 id="example">Example</h4>

        <p>As an example of application, we provide data derived from the
        publication

        <a href="https://www.nature.com/articles/s41467-017-00510-x">DNA
        methylation at enhancers identifies distinct breast cancer lineages,
        Fleischer, Tekpli, <em>et al</em>, <em>Nature Communications</em>,
        2017</a>.

        The genomic regions of interest correspond to 200bp-long regions around
        CpGs from cluster 2A described in the publication. These regions around
        CpGs of interest are shown to be associated with FOXA1, GATA, and ESR1
        binding. We applied the following command to compute TFBS enrichment
        using all the CpG probes from the Illumina Infinium HumanMethylation450
        microarray:</p>

        <pre>bash bin/JASPAR_enrich.sh oneSetBg data/LOLA_dbs/ data/example_Fleischer_et_al/clusterA_200bp_hg38.bed data/example_Fleischer_et_al/450k_probes_hg38_200bp.bed JASPAR_enrichment</pre>

        <p>We observe a clear enrichment for TFBSs associated with the expected
        TFs. The corresponding swarm plot is:</p>

        <img src="{% static 'img/allEnrichments_swarm.png' %}" alt="Swarm plot" />

        <p>A complementary beeswarm plot allows to clearly visualize the 10
        most enriched TFs:</p>

        <img src="{% static 'img/JASPAR_enrichment_beeswarmplot.png' %}"
             alt="Swarm plot showing the ten most enriched TFs">

        <p>The enrichment and ranking of all the datasets can be visualized in
        the ranking plot:</p>

        <img src="{% static 'img/JASPAR_enrichment_ranking.png' %}"
             alt="Enrichment and ranking of all datasets">
      </div>
    </div>
  </div>
