#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# A simplistic substitution for Python version adjustments.

make_updated()
{
    sed "s/$1/$2/g"
}

# Obtain the Python version for parameterising files.

PYTHON=`"$THISDIR/tools/python_version"`

# Obtain the appropriate library directory name.

PATCH="$THISDIR/patches/patch-django-file-move-selinux.diff"

for FILENAME in `grep '^---' "$PATCH" | sed 's/^[^ ]* //;s/python3[0-9.]*/python3*/' | cut -f 1 | xargs -I{} echo "$BASEDIR/{}"` ; do
    if [ -e "$FILENAME" ] ; then
        LIBDIR=lib64
    else
        LIBDIR=lib
    fi
    break
done

# Patch Django to work correctly with SELinux.

  cat "$PATCH" \
| make_updated "python3.7" "$PYTHON" \
| make_updated "lib64" "$LIBDIR" \
| patch -N -d "$BASEDIR" -p0

# vim: tabstop=4 expandtab shiftwidth=4
