#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

# Initialise directories for the installed libraries.

LOCALDIR="$BASEDIR/lib/usr/local"

if have semanage ; then
    semanage fcontext -a -t bin_t "$LOCALDIR/bin(/.*)?"
    semanage fcontext -a -t lib_t "$LOCALDIR/lib(/.*)?"
    semanage fcontext -a -t lib_t "$LOCALDIR/lib64(/.*)?"
    restorecon -R "$LOCALDIR"
fi

# vim: tabstop=4 expandtab shiftwidth=4
