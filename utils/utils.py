# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db.models import F, OuterRef, Max, Subquery
from jaspar.settings import BASE_DIR, TEMP_DIR, get_data_version
from os.path import basename, dirname, exists, join
from portal.models import Matrix, MatrixAnnotation, MatrixData, MatrixProtein, MatrixVersion, Release, TaxGroup
from urllib.parse import unquote
import numpy as np
import zipfile

# Profile inference imports.

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import re

# Handle unavailable profile inference tool.

try:
    import jpi
    from jpi import infer_profile
except ImportError:
    jpi = None

# Common definitions.

letters = "ACGT"

# Utility functions.

def split_id(matrix_id):
    '''
    Split JASPAR ID into based_id and version
    '''

    id_split = unquote(matrix_id).split('.')

    base_id = None
    version = None
    if len(id_split) == 2:
        base_id = id_split[0]
        version = id_split[1]
    else:
        base_id = matrix_id

    return (base_id, version)

def get_base_id(matrix_id):
    """
    Return the base_id for the given matrix id
    """

    (bases_id,_) = split_id(matrix_id)

    return bases_id

def get_internal_id(matrix_id):
    """
    Return the internal_id for the given matrix_id
    """

    (base_id, version) = split_id(matrix_id)

    queryset = Matrix.objects.get(base_id=base_id, version=version)

    return queryset.id

def get_version(matrix_id):
    """
    Return the version for the given matrix id
    """

    (_,version) = split_id(matrix_id)

    return version

def get_latest_version(self, base_id):
    """
    Return the latest version number for the given base_id
    """

    queryset = Matrix.objects.filter(base_id=base_id).order_by('version')[0:1]

    return queryset.version

def get_latest_versions(queryset):
    '''
    Obtain the latest versions of the matrices provided in the given
    queryset.

    This is so much easier in SQL using multiple column joins or
    membership tests, even joins against subqueries. Here, the
    ORM is persuaded to use a subquery as follows:

    select ..., (select max(version) as latest_version
             from matrix as X
             where X.base_id = Y.base_id
             group by X.base_id)
    from matrix as Y
    where Y.version = X.latest_version
    '''

    # Subquery...

    # Note that ordering on the query must be removed for the subquery to work.
    # Otherwise, Django decides to group using the ordering columns.

    latest_versions = queryset.order_by() \
        .values("base_id") \
        .annotate(latest_version=Max("version")) \
        .filter(base_id=OuterRef("base_id")) \
        .values("latest_version")

    # Outer query...

    return queryset.annotate(latest_version=Subquery(latest_versions)) \
        .filter(version=F("latest_version"))

def get_internal_id(matrix_id):
    '''
    Get internal identifier given a matrix identifier.
    '''

    base_id, version = matrix_id.split('.')
    matrix = Matrix.objects.get(base_id=base_id, version=version)
    return matrix.id

def get_matrix_id(id):
    '''
    Return the matrix identifier for the given internal identifier.
    '''

    queryset = Matrix.objects.get(pk=id)
    return '%s.%s' % (queryset.base_id, queryset.version)

def get_matrix_instances(imatrixids):
	'''
	Return a queryset providing instances for the given internal matrix
	identifiers.
	'''

	return Matrix.objects.filter(id__in=imatrixids)

def get_matrix_identifiers_for_release(release):
    '''
    Return internal matrix identifiers for matrices in the given release.
    This will also include identifiers for older matrix versions.
    '''

    return MatrixVersion.objects.filter(release=release).values("matrix_id")

def get_matrix_instances_for_release(release):
    '''
    Return a queryset providing instances for matrices in the given release.
    This will also include older matrix versions.
    '''

    return get_matrix_instances(get_matrix_identifiers_for_release(release))

def get_matrix_identifier(matrix):
    '''
    Return the version-qualified identifier for the given matrix.
    '''

    return '%s.%s' % (matrix.base_id, matrix.version)

def get_matrix_name(matrix):
    '''
    Return a name for the given matrix.
    '''

    return '%s.%s' % (get_matrix_identifier(matrix), matrix.name)

def get_release(year=None):
    '''
    Return the given or the current release object. Where the given release or
    no releases are defined, None is returned.
    '''

    if year:
        try:
            return Release.objects.get(year=year)
        except Release.DoesNotExist:
            return None
    else:
        return Release.objects.order_by('-release_number').first()

def get_releases():
    '''
    Return release objects in chronological order.
    '''

    return Release.objects.order_by('release_number')

def get_data_tag():
    '''
    Return the data tag: a version indicator for the data to be accessed. This
    is currently defined to be equivalent to the data version, indicating a
    general release identifier, this being the year of release.
    '''

    return get_data_version()

def get_collections(prioritised=False):
    '''
    Return all known collections. If the prioritised parameter is set to a true
    value, CORE will appear first.
    '''

    collections = []

    for name in get_collection_names(prioritised):
        collections.append({
            'name': name,
            'label': name.replace('_', ' '),
            })

    return collections

def get_collection_names(prioritised=False):
    '''
    Return all known collection names. If the prioritised parameter is set to a
    true value, CORE will appear first.
    '''

    names = list(Matrix.objects.values_list("collection", flat=True).distinct().order_by("collection"))

    if prioritised and "CORE" in names:
        names.remove("CORE")
        names.insert(0, "CORE")

    return names

def get_tax_groups(clustering=None, release=None):
    '''
    Return records for the taxonomic groups. Limit to a subset of groups if
    the clustering parameter is given as a true or false (but not None) value.

    If a release year is given, limit the groups to those present in the
    release.
    '''

    if clustering is None:
        groups = TaxGroup.objects.all()
    else:
        groups = TaxGroup.objects.filter(clustering=(clustering and 1 or 0))

    # For a given release, obtain taxonomic groups associated with matrices
    # present in the release. Then, obtain the group details.

    if release:
        matrix_ids = get_matrix_identifiers_for_release(release)
        tax_groups = MatrixAnnotation.objects.filter(matrix_id__in=matrix_ids) \
                                             .filter(tag="tax_group") \
                                             .values("val")
        groups = groups.filter(common_name__in=tax_groups)

    return groups.order_by("common_name")

def tfbs_info_exist(base_id, version):
    '''
    Check if binding sites information does exist or not.
    return: dictionary object
    '''

    data_tag = get_data_tag()

    tfbs_info = {}
    data_dir = join(BASE_DIR, 'download', 'data', data_tag)
    url_fasta = join(data_dir, 'sites', '%s.%s.sites' % (base_id, version))
    url_bed = join(data_dir, 'bed', '%s.%s.bed' % (base_id, version))
    
    tfbs_info['tfbs_info_fasta'] = exists(url_fasta)
    tfbs_info['tfbs_info_bed'] = exists(url_bed)

    return tfbs_info

def motif_infer(sequence):
    '''
    Return inference results for the given sequence.
    '''

    if not jpi:
        return None

    # Remove newlines from the submitted sequence.

    sequence = re.sub(r"\s", "", sequence)

    # Bundle the sequence in the necessary wrappings and invoke the tool.

    seq = Seq(sequence)
    rec = SeqRecord(seq)

    # Load necessary data. There is an infer_profiles function that does this,
    # but it only accepts a filename, not even a file object.

    files_dir = join(dirname(jpi.__file__), "files")

    if hasattr(infer_profile, "__load_CisBP_models"):
        cisbp = infer_profile.__load_CisBP_models(files_dir)
        jaspar = infer_profile.__load_JASPAR_files_n_models(files_dir)

        return infer_profile.infer_SeqRecord_profiles(rec, cisbp, jaspar,
                                                      latest=True,
                                                      files_dir=files_dir)
    else:
        return infer_profile.infer_SeqRecord_profiles(rec,
                                                      latest=True,
                                                      files_dir=files_dir)

def map_annotations(queryset):
    '''
    Internal method to map annotations in a structured data

    @input:
    queryset {QuerySet}

    @output:
    annotations {dictionary}
    '''

    annotations = {
        'class': [],
        'family': [],
        'tfe_id': [],
        'medline': [],
        'pazar_tf_id': [],
        }

    # Loop through annotations and get what's needed.

    for annotation in queryset:
        values = annotations.get(annotation.tag)

        if isinstance(values, list):
            values.append(annotation.val)
        elif annotation.tag == 'included_models':
            annotations[annotation.tag] = annotation.val.replace(',', ' ')
        else:
            annotations[annotation.tag] = annotation.val

    # Alias various entries.
    # NOTE: Could update any templates instead.

    annotations['tfe_ids'] = annotations['tfe_id']
    annotations['pubmed_ids'] = annotations['medline']
    annotations['pazar_tf_ids'] = annotations['pazar_tf_id']
    return annotations

def get_matrix_data(imatrixid, revcomp=False, permute=False):
    '''
    Takes internal matrix id and returns matrix data as a dictionary object.
    '''

    d = {}

    for base in letters:
        d[base] = MatrixData.objects.values_list('val', flat=True).filter(id=imatrixid, row=base).order_by('col')

    # Compute the reverse complement.

    if revcomp:
        d['A'] = d['T'][::-1]
        d['C'] = d['G'][::-1]
        d['G'] = d['C'][::-1]
        d['T'] = d['A'][::-1]

    # Permute the matrix.

    if permute:
        pfm_array = np.array([ d['A'], d['C'], d['G'], d['T'] ])
        pfm_array = pfm_array[:, np.random.permutation(pfm_array.shape[1])]

        # Update with permuted columns.

        (d['A'], d['C'], d['G'], d['T']) = (pfm_array[0], pfm_array[1], pfm_array[2], pfm_array[3])

    return d

# Export data templates.

meme_header = """\
MEME version {version}

ALPHABET= ACGT

strands: + -

Background letter frequencies
A 0.25 C 0.25 G 0.25 T 0.25

"""

meme_template = """\
MOTIF {matrix_id} {matrix_name}
letter-probability matrix: alength= 4 w= {length} nsites= {nsites} E= 0
{data}
URL http://jaspar.genereg.net/matrix/{matrix_id}

"""

transfac_template = """\
AC {matrix_id}
XX
ID {matrix_name}
XX
DE {matrix_id} {matrix_name} ; From JASPAR
PO\tA\tC\tG\tT
{data}
XX
CC tax_group:{tax_group}
CC tf_family:{tf_family}
CC tf_class:{tf_class}
CC pubmed_ids:{pubmed_ids}
CC uniprot_ids:{uniprot_ids}
CC data_type:{data_type}
XX
//
"""

def write_meme_header(version='4'):
    return meme_header.format(version=version)

def print_matrix_data(matrix_data, matrix_id=None, matrix_name=None, format='pfm'):
    """
    Return the representation of motifs in pfm, jaspar or transfac format.

    Adopted from BioPython jaspar module.
    """
    length = len(matrix_data[letters[0]])

    # Map to obtain matrix_data[letter] for all letters.
    # Zip to obtain letter data at each position.

    letter_data = zip(*map(matrix_data.get, letters))

    lines = []

    if format == 'pfm':
        if matrix_id and matrix_name:
            line = ">{0} {1}\n".format(matrix_id, matrix_name)
            lines.append(line)
        for letter in letters:
            terms = ["{0:6.2f}".format(float(value)) for value in matrix_data[letter]]
            line = "{0}\n".format(" ".join(terms))
            lines.append(line)

    elif format == 'jaspar':
        line = ">{0}\t{1}\n".format(matrix_id, matrix_name)
        lines.append(line)
        for letter in letters:
            terms = ["{0:6.0f}".format(float(value)) for value in matrix_data[letter]]
            line = "{0}  [{1} ]\n".format(letter, " ".join(terms))
            lines.append(line)

    elif format == 'transfac':
        anno_dict = map_annotations(MatrixAnnotation.objects.filter(matrix_id=get_internal_id(matrix_id)))
        proteins = MatrixProtein.objects.filter(matrix_id=get_internal_id(matrix_id)).values_list('acc', flat=True)

        l = []
        for i, (a, c, g, t) in enumerate(letter_data, 1):
            line = "{0}\t{1}\t{2}\t{3}\t{4}".format(
                str(i).zfill(2), a, c, g, t)
            l.append(line)

        get = lambda key: anno_dict.get(key, '')
        getlist = lambda key: anno_dict.get(key, [])

        lines.append(transfac_template.format(matrix_id=matrix_id,
            matrix_name=matrix_name, data="\n".join(l),
            tax_group=get('tax_group'),
            tf_family='; '.join(getlist('family')).strip(),
            tf_class='; '.join(getlist('class')).strip(),
            pubmed_ids='; '.join(getlist('pubmed_ids')),
            uniprot_ids='; '.join(proteins),
            data_type=get('type')))

    elif format == 'meme':
        nsites0 = 0
        outputs = []

        for i, column in enumerate(letter_data):
            column = list(map(float, column))

            # Sum float-coerced first element values from the matrix.

            nsites = sum(column)

            # The "nsites" value in the header is derived from the first column.

            if not i:
                nsites0 = nsites

            # Provide column values that are proportions of the sum.

            values = [("%6.6f" % (nsites and (value / nsites) or 0)) for value in column]

            # Join the values and produce the output line.

            formatted_values = "  ".join(values)
            outputs.append(" %s" % formatted_values)

        lines.append(meme_template.format(matrix_id=matrix_id,
            matrix_name=matrix_name, length=length,
            nsites=int(nsites0), data="\n".join(outputs)))
    else:
        raise ValueError("Unknown matrix format %s" % format)

    # Finished: glue the lines together.

    return "".join(lines)

def write_matrix_archive(queryset, download_format, zip_file_name, zip_dir_path=TEMP_DIR):
    '''
    Create a zipfile containing individually generated files for matrices
    provided by the given queryset.
    '''

    matrix_ids = []
    zip_file_path = join(zip_dir_path, zip_file_name)

    with zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED) as zip_file:
        for matrix in queryset:
            write_matrix_to_archive(zip_file, matrix, download_format)
            matrix_ids.append(get_matrix_identifier(matrix))

    return matrix_ids

def write_matrix_textfile(queryset, download_format, text_file_name, text_dir_path=TEMP_DIR):
    '''
    Create a text file containing matrices provided by the given queryset.
    '''

    matrix_ids = []
    text_file_path = join(text_dir_path, text_file_name)

    with open(text_file_path, 'w') as text_file:
        for matrix in queryset:
            write_matrix(text_file, matrix, download_format)
            matrix_ids.append(get_matrix_identifier(matrix))

    return matrix_ids

def write_matrix_to_archive(zip_file, matrix, download_format):
    '''
    Write individual matrix details to temporary files for
    inclusion in the zipfile.
    '''

    matrix_id = get_matrix_identifier(matrix)
    input_file_path = join(TEMP_DIR, "%s.%s" % (matrix_id, download_format))

    with open(input_file_path, 'w') as matrix_file:
        write_matrix(matrix_file, matrix, download_format)

    zip_file.write(input_file_path, basename(input_file_path))

def write_matrix(f, matrix, download_format, permute=False):
    '''
    Write matrix data to a file.
    '''

    matrix_id = get_matrix_identifier(matrix)

    # Write a header if appropriate.

    if not f.tell() and download_format == 'meme':
        f.write(write_meme_header())

    f.write(print_matrix_data(get_matrix_data(matrix.id, permute=permute),
        matrix_id, get_matrix_name(matrix), format=download_format))

# vim: tabstop=4 expandtab shiftwidth=4
