#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
ARCHETYPES="$BASEDIR/static/archetypes"
ARCHETYPES_RELEASES="$THISDIR/work/archetypes_releases.txt"
NOW=`date +%Y%m%d`

# Obtain any data location and use that instead of the default.

DATA=`cat "$THISDIR/work/data.txt"`

# Make the archetypes directory if absent.

if [ ! -e "$ARCHETYPES" ] ; then
    mkdir -p "$ARCHETYPES"
fi

# Get the current release.

for DATABASE in "$BASEDIR/JASPAR"*".sqlite3" ; do

    if [ ! -e "$DATABASE" ] ; then
        echo "No database detected." 1>&2
        exit 1
    fi

    TAG=`"$THISDIR/tools/file_tag" "$DATABASE"`

    # Unpack archetypes data.

    ARCHETYPES_DATA="$DATA/jaspar-archetypes-${TAG}.tar.gz"

    if [ ! -e "$ARCHETYPES_DATA" ] ; then
        continue
    fi

    TARGET="$ARCHETYPES/$TAG"

    if [ ! -e "$TARGET" ] || [ "$ARCHETYPES_DATA" -nt "$TARGET" ] ; then
        if [ -e "$TARGET" ] ; then
            mv -f "$TARGET" "$TARGET.backup-$NOW"
        fi
        "$THISDIR/tools/tar_extract" "$ARCHETYPES_DATA" "$TARGET"
    fi
done

# Make a record of available archetypes data for releases.

/bin/ls -1 "$ARCHETYPES" | grep -v 'backup' > "$ARCHETYPES_RELEASES"

# vim: tabstop=4 expandtab shiftwidth=4
