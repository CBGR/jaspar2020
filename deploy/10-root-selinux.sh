#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

if ! have semanage ; then
    exit 0
fi

# Initialise directories.

for DIRNAME in temp ; do
    DIRPATH="$BASEDIR/$DIRNAME"

    # Set a label for SELinux, apply the labelling for SELinux.

    semanage fcontext -a -t httpd_sys_rw_content_t "$DIRPATH(/.*)?"
    restorecon -R "$DIRPATH"
done

# Apply labelling to static resources in case it was somehow lost.

restorecon -R "$BASEDIR/static"

# vim: tabstop=4 expandtab shiftwidth=4
