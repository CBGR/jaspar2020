#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

if ! have semanage ; then
    exit 0
fi

# Set permissions on the application directory for SQLite write access.
# See: 06-user-sqlite-apache.sh

semanage fcontext -a -t httpd_sys_rw_content_t "$BASEDIR"
restorecon "$BASEDIR"

for DATABASE in "$BASEDIR/JASPAR"*".sqlite3" ; do

    # Handle the wildcard case.

    if [ ! -e "$DATABASE" ] ; then
        echo "No databases found!" 1>&2
        exit 1
    fi

    semanage fcontext -a -t httpd_sys_rw_content_t "$DATABASE"
    restorecon "$DATABASE"
done

# vim: tabstop=4 expandtab shiftwidth=4
