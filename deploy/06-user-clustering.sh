#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
CLUSTERING="$BASEDIR/static/clustering"
CLUSTERING_RELEASES="$THISDIR/work/clustering_releases.txt"

# Find collections of clustering resources to process.

for NAME in 'SUMMARY' 'radial_tree' 'tree_annotated' ; do
    for RESOURCE in `find "$CLUSTERING" -name "*_$NAME.html"` ; do
        COLLECTION=`dirname "$RESOURCE"`
        FILENAME=`basename "$RESOURCE"`

        if [ ! -e "$COLLECTION/$NAME.html" ] ; then
            ln -s "$FILENAME" "$COLLECTION/$NAME.html"
        fi
    done
done

# Handle missing tree-related files.

for RESOURCE in `find "$CLUSTERING" -name "SUMMARY.html"` ; do
    DIRNAME=`dirname "$RESOURCE"`

    if [ -e "$DIRNAME/radial_tree.html" ] && [ ! -e "$DIRNAME/tree_annotated.html" ] ; then
        ln -s "radial_tree.html" "$DIRNAME/tree_annotated.html"
    fi
done

# Handle collection naming ambiguity for directories.

for RESOURCE in `find "$CLUSTERING" -name "CORE_+_UNVALIDATED" -type d` ; do
    DIRNAME=`dirname "$RESOURCE"`

    if [ ! -e "$DIRNAME/UNVALIDATED" ] ; then
        ln -s "CORE_+_UNVALIDATED" "$DIRNAME/UNVALIDATED"
    fi
done

# Handle inconsistent naming for archives.

for RESOURCE in `find "$CLUSTERING" -name "*_CORE*.zip" -o -name "*_UNVALIDATED*.zip"` ; do
    DIRNAME=`dirname "$RESOURCE"`
    FILENAME=`basename "$RESOURCE"`

    ALIAS=`echo "$FILENAME" | sed 's/_CORE_+_UNVALIDATED//;s/_CORE//;s/_UNVALIDATED//'`

    if [ ! -e "$DIRNAME/$ALIAS" ] ; then
        ln -s "$FILENAME" "$DIRNAME/$ALIAS"
    fi
done

# Make a record of available clustering data for releases.

/bin/ls -1 "$CLUSTERING" | grep -v 'backup' > "$CLUSTERING_RELEASES"

# vim: tabstop=4 expandtab shiftwidth=4
