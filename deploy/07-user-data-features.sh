#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Run tools to update each database with feature information.

for DATABASE in "$BASEDIR/"*".sqlite3" ; do
    "$THISDIR/tools/import_features" "$DATABASE"
done

# vim: tabstop=4 expandtab shiftwidth=4
