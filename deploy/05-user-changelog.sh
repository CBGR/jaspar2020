#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain any data location and use that instead of the default.

DATA=`cat "$THISDIR/work/data.txt"`

# Find changelogs and test for their inclusion.

TEMPLATES="$BASEDIR/portal/templates/portal/changelogs"
MANIFEST="$BASEDIR/portal/templates/portal/changelogs.html"

for FILENAME in "$DATA/changelog_"*".html" ; do

    if [ ! -e "$FILENAME" ] ; then
        break
    fi

    # Identify the file tag or release identifier.

    TAG=`"$THISDIR/tools/file_tag" "$FILENAME"`

    # Test for any revised data.

    REVISED="$DATA/changelog_${TAG}_revised.html"

    if [ -e "$REVISED" ] && [ "$FILENAME" != "$REVISED" ] ; then
        echo "File $FILENAME superseded by $REVISED - not processing." 1>&2
        continue
    fi

    # Define the same target filename for both normal and revised data versions.
    # This will overwrite an existing changelog with a revised one.

    BASENAME="changelog_${TAG}.html"

    if [ "$FILENAME" -nt "$TEMPLATES/$BASENAME" ] ; then
        cp "$FILENAME" "$TEMPLATES/$BASENAME"
    fi
done

# Regenerate the changelog manifest.

  /bin/ls -1 "$TEMPLATES" \
| sort -r \
| sed 's/^/{% include "portal\/changelogs\//;s/$/" %}/' \
> "$MANIFEST"

# vim: tabstop=4 expandtab shiftwidth=4
