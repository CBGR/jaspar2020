Development
===========

The following development topics are covered by this documentation:

- `Application settings and configuration <Settings.rst>`_
- `Configuration of additional functionality <Configuration.rst>`_
- `Database management <Databases.rst>`_
- `Deployment of the software <Deployment.rst>`_
- `Database schema information <Schema.rst>`_
- `REST API architecture and considerations <REST.rst>`_
- `System testing <Testing.rst>`_
- `Upgrading and introducing data <Upgrading.rst>`_
