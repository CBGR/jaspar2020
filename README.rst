JASPAR
======

JASPAR is a database of curated, non-redundant set of profiles, derived from
published collections of experimentally defined transcription factor binding
sites for eukaryotes.

This software distribution provides the source code of the JASPAR Web
application developed in `Python`_ and `Django`_.  To run this Web application
locally, you need to have Python installed and to install Django and a number
of other Python packages. More details are provided below.

Access to several separate tools is provided via the Web application. A
separate `jaspar_tools`_ repository is provided containing the documentation
for some of these tools and details of their retrieval and deployment, with
their deployment being integrated into the deployment of this software.
Additionally, the JASPAR profile inference tool and other required programs
are obtained and configured in the deployment process referenced below.

The data to be presented by the Web application is packaged by the software
found in the `jp2021`_ repository, this operating on previously published data
and the results of a curation process.

.. _`Python`: https://www.python.org/
.. _`Django`: https://www.djangoproject.com/
.. _`jaspar_tools`: https://bitbucket.org/CBGR/jaspar_tools/
.. _`jp2021`: https://bitbucket.org/CBGR/jp2021/


Prerequisites
-------------

In order to obtain and deploy JASPAR, certain other tools and software
packages are required. Primarily, Git is used to retrieve the software, and
Python 3 is used to run the software.

Typically, Git is available through a package called ``git`` on many systems
and can be installed by the system's package manager, such as ``dnf`` on
Fedora or Red Hat systems or ``apt`` on Debian systems.

A deployment_ process that performs package installation on certain kinds of
systems is provided with this software, relying on the contents of the
following files:

- ``requirements-sys.txt`` for Fedora and Red Hat systems
- ``requirements-sys-debian.txt`` for Debian systems

The process employs the appropriate file which contains the names of packages
to be installed, this being done by invoking the system's package installation
tool with the necessary privileges.

Other packages are required to produce formatted documentation. See the
`documentation`_ instructions for more details.

.. _`deployment`: docs/Deployment.rst
.. _`documentation`: docs/Documentation.rst


Getting Started
---------------

First, get the development version of the software from Bitbucket using the
``git`` tool:

.. code:: shell

    git clone https://bitbucket.org/CBGR/jaspar2020.git

Although this software is, for the most part, a Django application that can be
run in a self-contained fashion from the distribution directory, many other
elements are required for it to perform its function in a usable fashion.
Consequently, the manual installation of software dependencies and other
resources is not really recommended.

For more sustainable and reliable deployment of this software, consult the
deployment_ documentation.

.. _`deployment`: docs/Deployment.rst


System and Development Documentation
------------------------------------

A list of documents related to the organisation and implementation of this
software can be found in the `development`_ documentation.

.. _`development`: docs/Development.rst


Copyright and Licensing Information
-----------------------------------

Source code originating in this project is licensed under the terms of the GNU
General Public License version 3 or later (GPLv3 or later). Original content
is licensed under the Creative Commons Attribution Share Alike 4.0
International (CC-BY-SA 4.0) licensing terms.

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this software's source code distribution for complete copyright and licensing
information, including the licensing details of bundled code and content.
