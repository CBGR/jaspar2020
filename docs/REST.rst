REST API Notes
==============

The REST API employs the `Django REST Framework`_ to expose database queries
as "endpoints" (or URLs) via various user interfaces. Although the entry point
on the Web site is via the ``/api`` endpoint which provides a title page for
the service, also linking to the "Live API" (whose user interface is largely
provided by other, client-side software), the REST framework automatically
generates documentation for the endpoints at ``/api/v1/docs`` with
descriptions of the different operations.

The ``/api/v1/docs/`` endpoint is also accessible using the ``coreapi`` tool,
as described on the generated documentation page. Doing so as follows will
retrieve general schema information:

.. code:: shell

   coreapi get https://jaspar.uio.no/api/v1/docs/

Such schema information is generated from the classes defined for the API in
``restapi/v1/views.py`` and exposed using ``restapi/v1/urls.py``. The REST
framework will inspect view classes and collect schema information, with this
work largely being done in the installed ``rest_framework/schemas/coreapi.py``
file.

One counterintuitive aspect of the schema generation involves list views.
Although a view class may be defined as showing lists of objects and inherit
from the ``ListAPIView`` class from the framework, and although it may employ
a pagination class to ensure that results are paginated, the framework will
suppress the pagination parameters for the class's endpoint if this endpoint
does not appear in the right place in the URL or endpoint hierarchy.

For example, consider the following URL pattern definitions (found in
``restapi/v1/urls.py``):

.. code:: python

   urlpatterns_api = [
       url(r'^v1/collections/?$', view(views.CollectionListViewSet), name='list-collections'),
       url(r'^v1/collections/(?P<collection>\w+)/$', view(views.CollectionMatrixListViewSet), name='collection-detail'),
       url(r'^v1/collections/(?P<collection>\w+)/matrix/$', view(views.CollectionMatrixListViewSet), name='list-collection-detail'),
       ...
       ]

Although the second and third URL patterns employ the same view class, only
the third pattern will expose an API endpoint that supports pagination. The
reason for this is that where a pattern has an identifier appearing at the
end, the framework will decide that such a pattern must expose a single object
and cannot therefore be exposing a list of objects. It short-circuits any
inspection of pagination parameters and returns an empty list of such
parameters instead.

The short-circuiting occurs in the ``AutoSchema.get_pagination_fields``
method, this calling the ``is_list_view`` function in the
``rest_framework/schemas/utils.py`` file. However, the configuration of the
views to observe the above, rather arbitrary limitation, is found in the
``SchemaGenerator.get_keys`` method.


.. _`Django REST Framework`: https://www.django-rest-framework.org/
