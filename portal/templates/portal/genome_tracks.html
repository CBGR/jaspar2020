{% extends 'portal/base.html' %}

{% load static %}

{% block title %}
Genome track data hub for UCSC and Ensembl
{% endblock %}

{% block content_header %}
JASPAR Genome Browser tracks
{% endblock %}

{% block breadcrumb %}
  <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
  <li class="active">Genome tracks</li>
{% endblock %}

{% block content %}

  <div class="row" id="top">

    <div class="col-md-12 col-lg-12 col-xs-12">
      <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="icon fa fa-info"></i> Genome Browser track data hub containing genome-wide binding site predictions for TF profiles in the JASPAR CORE.
      </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xs-12" id="introduction">
      <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"><i class="fa fa-map-marker"></i> Introduction</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <p>We scanned the genomes of 8 organisms (<em>A. thaliana</em>,
            <em>C. elegans</em>, <em>C. intestinalis</em>,
            <em>D. rerio</em>, <em>D. melanogaster</em>,
            <em>H. sapiens</em>, <em>M. musculus</em>, and
            <em>S. cerevisiae</em>) with the JASPAR CORE PFMs associated
            with the same taxon to predict TFBSs and create the JASPAR TFBS
            genomic tracks. Moreover, we created a collection of familial
            TFBSs by merging overlapping TFBSs that were predicted from PFMs
            associated with the same familial binding profile. The TFBS
            predictions associated with all PFMs are available
            <a href="http://expdata.cmmt.ubc.ca/JASPAR/downloads/UCSC_tracks/2022/" target="_blank">here</a>.</p>
            <p>We provide JASPAR TFBS predictions as genomic tracks, which can be
            visualized in genome browsers. Notably, the UCSC Genome Browser
            now presents predicted human JASPAR TFBS data as a native track
            for the human genome with information such as TF names, TFBS
            prediction scores, and PFM logo for each of the 12+ billion
            predictions.</p>

            <p>The code and data used to create the UCSC tracks can be found
            at
            <a href="https://github.com/wassermanlab/JASPAR-UCSC-tracks" target="_blank">https://github.com/wassermanlab/JASPAR-UCSC-tracks</a>.</p>
        </div>
      </div>
    </div>

    <div class="col-md-3 col-lg-3 col-xs-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-list"></i> Table of contents</h3>
        </div>
        <div class="box-body">
          <ul>
            <li> <a href="#introduction"> Introduction </a></li>
            <li> <a href="#ucsc_tracks"> UCSC tracks </a></li>
            <li> <a href="#ensembl_tracks">Ensembl tracks </a></li>
            <li> <a href="#track_hub">Track Hub Registry </a></li>
            <li> <a href="#further_information">Further information</a> </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xs-12" id="ucsc_tracks">
      <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"><i class="fa fa-map-marker"></i> UCSC tracks</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <p><strong>Native tracks</strong></p>

          <p>
            Since the 2022 JASPAR release, the TFBSs are accessible as
            native UCSC genome tracks.
          </p>

          <p><strong>Familial binding sites tracks</strong></p>

          <p>Genomic tracks can be viewed directly in the UCSC genome
          browser by connecting to the Public Hubs (Figure 1). The
          following genome assemblies are available and accessible
          directly by clicking on the assembly:</p>

          <ul>
            {% for genome in familial_binding_sites_genomes %}

            <li><strong>{{ genome.species_name }}:</strong>
              <a href="http://genome.ucsc.edu/cgi-bin/hgTracks?genome={{ genome.name }}&hubUrl={{ familial_binding_sites_prefix }}/{{ data_tag }}/hub.txt" target="_blank">{{ genome.name }}</a></li>

            {% endfor %}
          </ul>

          <p><strong>Download</strong></p>

          <p>Underlying TFBS prediction data is available for download and
            further analysis:
            <a href="http://expdata.cmmt.ubc.ca/JASPAR/downloads/UCSC_tracks/" target="_blank">http://expdata.cmmt.ubc.ca/JASPAR/downloads/UCSC_tracks/</a> </p>

          <p>Example of dataset (random sample of lines):</p>

          <pre><code>chr1    280     298     AGL3    821     369     -
chr1    309     327     AGL3    823     373     +
chr1    309     327     AGL3    882     488     -
chr1    1577    1595    AGL3    823     373     +
chr1    1577    1595    AGL3    883     490     -
chr1    2113    2131    AGL3    853     428     +
chr1    3186    3204    AGL3    825     376     -
chr1    3543    3561    AGL3    834     392     +
chr1    4284    4302    AGL3    824     375     +
chr1    4302    4320    AGL3    838     399     -
chr1    4318    4336    AGL3    818     365     -
chr1    4386    4404    AGL3    826     378     -
chr1    4908    4926    AGL3    838     399     -
chr1    6799    6817    AGL3    811     354     -
chr1    7097    7115    AGL3    810     352     +
chr1    7339    7357    AGL3    829     383     -
chr1    8054    8072    AGL3    802     339     +
chr1    8054    8072    AGL3    815     359     -
chr1    8097    8115    AGL3    898     525     +
chr1    8097    8115    AGL3    945     665     -
</code></pre>

          <p class="pull-right"> <a href="#top"> <i class="fa fa-arrow-circle-up"></i> Back to top </a> </p>

        </div>
      </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xs-12" id="ensembl_tracks">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-map-marker"></i> Ensembl tracks</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <p><strong>Direct link</strong></p>
          <p> View the tracks directly in the Ensembl Genome Browser using this
          <a href="http://www.ensembl.org/Trackhub?url=http://expdata.cmmt.ubc.ca/JASPAR/UCSC_tracks/hub.txt" target="_blank">link</a>.
          </p>

          <p><strong>Attach track hub</strong></p>
          <p>You can attach the JASPAR track hub to Ensembl via “Configure
          this page”. Then search the Track Hub Repository for JASPAR and
          attach the hub.</p>

          <p><img class="img img-responsive" src="{% static 'img/Ensembl_JASPAR_track_4.png' %}" width="40%"></p>
          <p><img class="img img-responsive" src="{% static 'img/Ensembl_JASPAR_track_5.png' %}" width="80%"></p>
          <p>Note that while the JASPAR UCSC tracks by default show predicted
          TF binding sites with scores of 500 or greater (P &lt; 10<sup>-5</sup>), the
          Ensembl tracks show all predicted binding sites. Ensembl colors
          predictions according to their score, from yellow (low scores) to
          green (medium) and blue (high). Please refer to the JASPAR hub
          help page (see below).</p>

          <p class="pull-right"> <a href="#top"> <i class="fa fa-arrow-circle-up"></i> Back to top </a> </p>

        </div>
      </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xs-12" id="track_hub">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-folder"></i> Track Hub Registry</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <p> JASPAR genome tracks are also available in Track Hub Registry.
          Use this
          <a href="https://www.trackhubregistry.org/search?q=jaspar" target="_blank">link</a>
          and select the track for the assembly of interest.</p>
          <p><img class="img img-responsive" src="{% static 'img/Ensembl_JASPAR_track_1.png' %}" width="100%"></p>

          <p class="pull-right"> <a href="#top"> <i class="fa fa-arrow-circle-up"></i> Back to top </a> </p>

        </div>
      </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xs-12" id="further_information">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-info"></i> Further information</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          Click on the JASPAR track details within the UCSC Genome browser to view the track help <a href="http://genome.ucsc.edu/cgi-bin/hgTrackUi?hgsid=637724977_mCbJ8XvftLprsVMF8MFL6cSXJXfz&c=chr1&g=hub_186875_JasparTFBS" target="_blank">page</a>:

          <p><img class="img img-responsive" src="{% static 'img/3.png' %}" width="100%"></p>
          <ul>
            <li>Code and data used to create the UCSC tracks are available at <a href="https://github.com/wassermanlab/JASPAR-UCSC-tracks"  target="_blank">https://github.com/wassermanlab/JASPAR-UCSC-tracks</a></li>
            <li>For more information on the use of UCSC track data hubs, see <a href='http://genome.ucsc.edu/goldenPath/help/hgTrackHubHelp.html'  target="_blank">Using UCSC Genome Browser Track Hubs </a></li>

            <li>For more information on the use of track hubs in Ensembl, see <a href="http://www.ensembl.org/info/website/adding_trackhubs.html" target="_blank">http://www.ensembl.org/info/website/adding_trackhubs.html</a> </li>

          </ul>

          <p class="pull-right"> <a href="#top"> <i class="fa fa-arrow-circle-up"></i> Back to top </a> </p>
        </div>
      </div>
    </div>

  </div>

{% endblock %}
