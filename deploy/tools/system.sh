#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

have()
{
    which "$1" > /dev/null 2>&1
}

system_is_debian()
{
    have apt
}

system_is_redhat()
{
    have dnf
}

apache_service_name()
{
    if system_is_debian ; then
        echo "apache2"
    else
        echo "httpd"
    fi
}

apache_user_name()
{
    if system_is_debian ; then
        echo "www-data"
    else
        echo "apache"
    fi
}

apache_modules_directory()
{
    if system_is_debian ; then
        echo "/etc/apache2/mods-available"
    else
        echo "/etc/httpd/conf.modules.d"
    fi
}

apache_module_loader()
{
    if system_is_debian ; then
        echo "wsgi.load"
    else
        echo "10-wsgi-python3.conf"
    fi
}

apache_site_directory()
{
    if system_is_debian ; then
        echo "/etc/apache2/sites-available"
    else
        echo "/etc/httpd/conf.d"
    fi
}

python_requirements()
{
    if have python3.5 ; then
        echo "requirements-python3.5.txt"
    else
        echo "requirements.txt"
    fi
}

conda_requirements()
{
    if system_is_debian ; then
        echo "requirements-conda-debian.txt"
    else
        echo "requirements-conda.txt"
    fi
}

# If the idiocy of Python packaging isn't irritating enough, Debian also
# apparently introduces non-standard behaviour to its pip package, effectively
# enforcing the --user option even when --root is specified (and it obstructs
# --prefix as well).
#
# See: https://github.com/pypa/pip/issues/1668

pip_system_flag()
{
    if system_is_debian ; then
        echo "--system"
    else
        echo
    fi
}

# vim: tabstop=4 expandtab shiftwidth=4
