# SPDX-FileCopyrightText: 2017-2019 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin

from .models import Matrix, MatrixAnnotation, MatrixData, MatrixProtein, MatrixSpecies, Tffm, Post, Release


class MatrixAdmin(admin.ModelAdmin):
	list_display = ('base_id', 'version', 'name','collection','id',)
	search_fields = ['collection', 'id', 'name', 'base_id']
	list_filter = ('collection',)

##enable this one if you want to manage Matrix table using admin
admin.site.register(Matrix, MatrixAdmin)


class MatrixAnnotationAdmin(admin.ModelAdmin):
	list_display = ('matrix_id', 'tag', 'val',)
	search_fields = ['tag', 'val']
	list_filter = ('tag',)

admin.site.register(MatrixAnnotation, MatrixAnnotationAdmin)


class MatrixDataAdmin(admin.ModelAdmin):
	list_display = ('matrix_id', 'row','col','val',)
	search_fields = ['row', 'col']

admin.site.register(MatrixData, MatrixDataAdmin)

class MatrixProteinAdmin(admin.ModelAdmin):
	list_display = ('id', 'acc',)
	search_fields = ['id', 'acc']

admin.site.register(MatrixProtein, MatrixProteinAdmin)

class MatrixSpeciesAdmin(admin.ModelAdmin):
	list_display = ('id', 'tax_id',)
	search_fields = ['id', 'tax_id']

admin.site.register(MatrixSpecies, MatrixSpeciesAdmin)

class TffmAdmin(admin.ModelAdmin):
	list_display = ('base_id', 'name','matrix_base_id','matrix_version',)
	search_fields = ['matrix_base_id', 'base_id','name']

##enable this one if you want to manage TFF table using admin
#admin.site.register(Tffm, TffmAdmin)


class NewsAndUpdateAdmin(admin.ModelAdmin):
	list_display = ('title', 'author','category','date')
	search_fields = ['title', 'author','category']
	list_filter = ('category','author',)

admin.site.register(Post, NewsAndUpdateAdmin)


class ReleaseAdmin(admin.ModelAdmin):
	list_display = ('year', 'release_number','pubmed_id','title','journal','journal_details')
	search_fields = ['year', 'release_number']
	list_filter = ('year', 'release_number',)

admin.site.register(Release, ReleaseAdmin)
