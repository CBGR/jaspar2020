Schema
======

This document describes the form of the database schema.

Django_ accesses databases_ via models, and such model definitions are found
in the ``portal/models.py`` file. A more conventional schema can be inspected
by entering a database environment directly using an appropriate database
file. For example:

.. code:: shell

  sqlite3 JASPAR2022.sqlite

In the SQLite_ command environment, the following command can be used to
display the entire database schema:

.. code:: sqlite3

  .schema

.. _databases: Database.rst
.. _Django: https://docs.djangoproject.com/en/2.2/
.. _SQLite: https://www.sqlite.org/


.. contents::


Overview
--------

The following diagram attempts to summarise the entities in the schema and
their relationships.

.. image:: Schema.svg

Natural and Surrogate Keys
++++++++++++++++++++++++++

Note that Django employs surrogate keys due to limitations with its
object-relational mapper. In a more sophisticated system, compound primary
keys might be employed. This surrogate key (often called ``id``) provides a
means of referencing each record and its values may be used by other tables to
reference records in the tables concerned.


Entity Descriptions
-------------------

Matrix
++++++

Table
  ``matrix``
Model class
  ``Matrix``
Natural key
  (``base_id``, ``version``)

Each record in ``matrix`` provides a distinct specific matrix identifier: that
is a distinct (``base_id``, ``version``) tuple. This record represents a
matrix profile.

Note that Django employs a surrogate key due to limitations with its
object-relational mapper. (In a more sophisticated system, a compound primary
key might be employed.) This surrogate key (``id``) provides a means of
referencing each record and is used by the other tables.

Matrix Data
+++++++++++

Table
  ``matrix_data``
Model class
  ``MatrixData``

Each record in ``matrix_data`` encodes a cell in a Position Weight Matrix
(PWM), with ``row`` indicating the row label (``A``, ``C``, ``G`` or ``T``),
with ``col`` indicating the column position (starting from 1), and with
``val`` indicating the weight.

This primary key of this table is a combination of ``id``, referencing
``matrix`` using its ``id`` column, ``row`` and ``col``.

Matrix Protein
++++++++++++++

Table
  ``matrix_protein``
Model class
  ``MatrixProtein``

Each record in ``matrix_protein`` describes a protein in the context of a
profile. Potentially many records may correspond to a single ``matrix``
record, meaning that potentially many proteins may be associated with a single
profile. However, a single protein accession may be observed in many profiles.

This table does not employ a primary key. A foreign key (``id``) references
``matrix`` using its ``id`` column.

Matrix Species
++++++++++++++

Table
  ``matrix_species``
Model class
  ``MatrixSpecies``

Each record in ``matrix_species`` describes a species in the context of a
profile. Potentially many records may correspond to a single ``matrix``
record, meaning that potentially many species may be associated with a single
profile. Meanwhile, a single species may be observed in many profiles.

This table does not employ a primary key. A foreign key (``id``) references
``matrix`` using its ``id`` column.

Matrix Annotation
+++++++++++++++++

Table
  ``matrix_annotation``
Model class
  ``Matrix Annotation``

Records in ``matrix_annotation`` provide annotations associated with each
``matrix`` record. Annotations provide details such as taxonomy groups and
foreign database identifiers.

This table employs ``local_id`` as a surrogate key. Meanwhile, a foreign key
(``id``) references ``matrix`` using its ``id`` column.

Taxonomy
++++++++

Table
  ``tax``
Model class
  ``Tax``

Each record in ``tax`` describes a single species and provides its scientific
name.

This table employs ``tax_id`` as the primary key, this being the Entrez
Taxonomy identifier for the species.

Taxonomy Extra/Extended
+++++++++++++++++++++++

Table
  ``tax_ext``
Model class
  ``TaxExt``

Each record in ``tax`` provides extra name information for a species, this
being the common name for a given species.

This table employs ``tax_id`` as the primary key, this being the Entrez
Taxonomy identifier for the species.

Gene
++++

Table
  ``genes``
Model class
  ``Gene``

Records in ``genes`` define the genes associated with the matrix proteins
present in the ``matrix_protein`` table. Along with the ``gene_id``, the
associated species is given by the ``tax_id``, with the gene ``symbol`` also
present.

This table employs ``gene_id`` as the primary key, this being the Entrez
Gene identifier for the gene.

Profile Gene
++++++++++++

Table
  ``profile_genes``
Model class
  ``ProfileGene``

This table has been introduced to map profiles to genes. Each profile may be
associated with multiple proteins, and each protein may be associated with
multiple genes. The involvement of proteins has been omitted from this schema.

Gene Synonym
++++++++++++

Table
  ``gene_synonyms``
Model class
  ``GeneSynonym``
Natural key
  (``gene_id``, ``synonym``)

Each record in the ``gene_synonyms`` table provides a synonym recorded for the
indicated gene from the Entrez Gene database.


Additional Entities
-------------------

A number of additional entities are present in the database to configure the
appearance of the Web application. The following diagram shows these entities.

.. image:: Schema-site.svg

Release
+++++++

Table
  ``portal_release``
Model class
  ``Release``

This table provides details of all JASPAR database releases. The ``year``
property (and table column) is typically used to reference a particular
release.

ArchetypesRelease
+++++++++++++++++

Table
  ``archetypes_releases``
Model class
  ``ArchetypesRelease``

This table provides details of all database releases featuring archetypes
data. The ``year`` property (and table column) references a particular release
via the ``Release`` entity's ``year`` property.

ClusteringRelease
+++++++++++++++++

Table
  ``clustering_releases``
Model class
  ``ClusteringRelease``

This table provides details of all database releases featuring matrix
clustering data. The ``year`` property (and table column) references a
particular release via the ``Release`` entity's ``year`` property.

Genome
++++++

Table
  ``genome``
Model class
  ``Genome``

This table provides details of the genomes used in familial binding sites
data. Alongside the ``name`` of each genome, the ``data_tag`` or version
identifier, ``tax_id`` (taxonomy identifier), and the database release
``year`` are indicated. These details are used to provide location information
to the Web application.

The genome ``name`` is the primary key in this table.

Genome Species
++++++++++++++

Table
  ``genome_species``
Model class
  ``GenomeSpecies``

This table provides genome species details for each of the genomes present in
the ``genome`` table. Alongside the ``tax_id``, the scientific name for the
species is given. Although this table reproduces data from the ``tax`` table,
it may also have entries that are not present in the ``tax`` table since that
table merely describes the species associated with profiles, whereas this
table catalogues species explicitly chosen for the provision of genome browser
tracks.

The species ``tax_id`` is the primary key in this table.

Taxonomy Group
++++++++++++++

Table
  ``tax_group``
Model class
  ``TaxGroup``

This table merely provides details of the taxonomy groups featured in the Web
application, providing name and illustrative image details. A ``clustering``
flag is also present, this being set to ``1`` if the clustering data is
provided for the taxonomy group. For the latest database release, typically
all groups will provide clustering data, but earlier releases may omit such
data for certain groups.

The group's ``tax_id`` is the primary key in this table.


Further Details
---------------

Some further details to help understanding of the schema, its entities, and
their relationships.

Queries
+++++++

Each matrix profile identifier plus number of referenced proteins:

.. code:: sql

  select id, count(*) as proteins from matrix_protein group by id;

This will confirm that some profiles are associated with multiple proteins.

Expanding the above, giving the distribution of referenced proteins (number of
proteins associated with a profile, number of profiles having a collection of
this size):

.. code:: sql

  select proteins, count(*) from (
    select id, count(*) as proteins from matrix_protein group by id
    ) as proteins_per_profile
  group by proteins order by proteins;

This will show that most profiles are associated with a single protein, but
there will be some profiles associated with more than one, although perhaps
not more than two.

Each protein accession plus number of involved profiles:

.. code:: sql

  select acc, count(*) as profiles from matrix_protein group by acc;

This will confirm that some proteins are associated with multiple profiles.

Expanding the above, giving the distribution of protein involvement (number of
involved profiles per protein, number of proteins being involved with this
precise number of profiles):

.. code:: sql

  select profiles, count(*) from (
    select acc, count(*) as profiles from matrix_protein group by acc
    ) as profiles_per_protein
  group by profiles order by profiles;

This will show that most proteins are associated with a single profile, but
there is a distribution of associations that extends to some proteins being
associated with tens of different profiles.

Each profile plus number of referenced species:

.. code:: sql

  select id, count(*) as species from matrix_species group by id;

This will confirm that most profiles are associated with a single species.

Expanding the above, giving the distribution of referenced species (number of
species associated with a profile, number of profiles having a collection of
this size):

.. code:: sql

  select species, count(*) from (
    select id, count(*) as species from matrix_species group by id
    ) as species_per_profile
  group by species order by species;

This will show a distribution where most profiles are associated with a single
species, but where a small number of profiles are associated with several
species.

Species plus number of involved profiles:

.. code:: sql

  select tax_id, count(*) as profiles from matrix_species group by tax_id;

This will show the number of profiles associated with each species, with many
species only being associated with a small number of profiles, but with some
species (these being the focus of the curation effort) being associated with
large numbers of profiles.

Expanding the above, giving the distribution of species involvement (number of
involved profiles per species, number of species being involved with this
precise number of profiles):

.. code:: sql

  select profiles, count(*) from (
    select tax_id, count(*) as profiles from matrix_species group by tax_id
    ) as profiles_per_species
  group by profiles order by profiles;

This will show a distribution with some species associated with only one or
two profiles, but with species generally associated with larger numbers of
profiles, and with large numbers of human-related profiles at the limit of the
distribution associated with the single ``9606`` species.
