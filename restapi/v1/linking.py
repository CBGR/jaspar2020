# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from os.path import isfile, join
from jaspar.settings import BASE_DIR

# URL generation.

def get_api_root_url(request):
    return request.build_absolute_uri(location='/api/v1/')

def get_host_name(request):
    return request.build_absolute_uri(location='/')

# Resource-specific URL generation.

def get_collection_url(request, collection):
    path = '/api/v1/collections/%s/' % collection
    return request.build_absolute_uri(location=path)

def get_matrix_url(request, base_id, version=None):
    path = '/api/v1/matrix/%s/' % (version and ('%s.%s' % (base_id, version)) or base_id)
    return request.build_absolute_uri(location=path)

def get_matrix_tffm_url(request, base_id, version):
    path = '/api/v1/tffm/%s.%s/' % (base_id, version)
    return request.build_absolute_uri(location=path)

def get_release_url(request, release_number):
    path = '/api/v1/releases/%s/' % release_number
    return request.build_absolute_uri(location=path)

def get_sequence_logo(request, base_id, version=None):
    path = '/static/logos/svg/%s.svg' % (version and ('%s.%s' % (base_id, version)) or base_id)
    return request.build_absolute_uri(location=path)

def get_sites_fasta_url(request, base_id, version):
    if isfile(join(BASE_DIR, 'download', 'sites', '%s.%s.sites' % (base_id, version))):
        path = '/download/sites/%s.%s.sites' % (base_id, version)
        return request.build_absolute_uri(location=path)
    else:
        return None

def get_sites_bed_url(request, base_id, version):
    if isfile(join(BASE_DIR, 'download', 'bed_files', '%s.%s.bed' % (base_id, version))):
        path = '/download/bed_files/%s.%s.bed' % (base_id, version)
        return request.build_absolute_uri(location=path)
    else:
        return None

def get_sites_url(request, base_id, version):
    if isfile(join(BASE_DIR, 'download', 'bed_files', '%s.%s.bed' % (base_id, version))):
        path = '/api/v1/sites/%s.%s' % (base_id, version)
        return request.build_absolute_uri(location=path)
    else:
        return None

def get_species_url(request, tax_id):
    path = '/api/v1/species/%s/' % tax_id
    return request.build_absolute_uri(location=path)

def get_taxon_url(request, tax_group):
    path = '/api/v1/taxon/%s/' % tax_group
    return request.build_absolute_uri(location=path)

def get_tffm_url(request, base_id, version, trained_order):
    if isfile(join(BASE_DIR, 'static', 'TFFM', '%s.%s' % (base_id, version),
                   'TFFM_%s_trained.xml' % trained_order)):
        path = '/static/TFFM/%s.%s/TFFM_%s_trained' % (base_id, version, trained_order)
        return request.build_absolute_uri(location=path)
    else:
        return None

def get_versions_url(request, base_id):
    path = '/api/v1/matrix/%s/versions' % base_id
    return request.build_absolute_uri(location=path)

# Filename generation.

def get_bed_filename(matrix_id):
    return join(BASE_DIR, 'download', 'bed_files', '%s.bed' % matrix_id)
