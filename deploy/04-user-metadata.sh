#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Import the metadata into the database from each versioned metadata directory.

for WORKDIR in "$THISDIR/work/metadata/"* ; do

    # Ignore any backups.

    if basename "$WORKDIR" | grep -q 'backup' ; then
        continue
    fi

    if [ -d "$WORKDIR" ] ; then

        # Employ a version-specific database.

        TAG=`basename "$WORKDIR"`
        DATABASE="$BASEDIR/JASPAR${TAG}.sqlite3"

        # Obtain metadata directories.

        METADATA=`dirname "$WORKDIR"`
        PARENT=`dirname "$METADATA"`
        DATADIR=`realpath "$WORKDIR"`
        TFFM_DATADIR=`realpath "$PARENT/metadata_tffm/$TAG"`
        SYNONYMS_DATADIR=`realpath "$PARENT/synonyms/$TAG"`

        # Initialise each database and import metadata.

        "$THISDIR/tools/init_database" "$DATABASE" "$TAG"
        "$THISDIR/tools/import_metadata" "$DATADIR" "$TFFM_DATADIR" "$DATABASE"
        "$THISDIR/tools/import_synonyms" "$SYNONYMS_DATADIR" "$DATABASE"
    fi
done

# vim: tabstop=4 expandtab shiftwidth=4
