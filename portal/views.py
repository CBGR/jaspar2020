# SPDX-FileCopyrightText: 2017-2022 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from os.path import basename, exists, join
from collections import defaultdict
import datetime, math, os, re, subprocess, time, zipfile
import numpy as np

from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMessage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.decorators.cache import cache_page

# Application settings.

from jaspar.settings import BASE_DIR, BIN_DIR, FAMILIAL_BINDING_SITES_PREFIX, \
	get_data_version, \
	MAX_PAGINATION_LIMIT, PAGINATION_DEFAULT, SEND_TO_EMAIL, STATIC_ROOT, \
	TEMP_DIR, TEMP_LIFE, TOOLS_DIR

# Biopython and utilities imports.

from Bio import motifs, SeqIO
from io import StringIO
from utils import utils

# Portal imports.

from .forms import InferenceForm, ContactForm, AlignForm, SearchForm, CurationForm
from .models import ArchetypesRelease, ClusteringRelease, Gene, Genome, GenomeSpecies, \
	Matrix, MatrixAnnotation, MatrixSpecies, MatrixData, MatrixHistory, MatrixProtein, \
	Synonym, Tax, TaxExt, Tffm, Post, Release

# Definitions.

DAY_IN_SECONDS = 60 * 60 * 24
CACHE_TIMEOUT = 1 * DAY_IN_SECONDS

# Resources.

@cache_page(60)
def index(request):
	'''
	Show the homepage.
	'''

	setattr(request, 'view', 'index')

	context = _context(request)
	search_form = SearchForm()

	tax_groups = list(utils.get_tax_groups())

	context.update({
		'search_form': search_form,
		'featured_tax_groups': tax_groups,
		'featured_tax_groups_count': len(tax_groups),
		})
	context.update(_get_advanced_search_data())

	home_version = request.GET.get('home', None)

	if home_version == 'v1':
		return render(request, 'portal/index_v1.html', context)
	else:
		return render(request, 'portal/index.html', context)

@cache_page(CACHE_TIMEOUT)
def search(request):
	'''
	Show the results based on the search query.
	'''
	setattr(request, 'view', 'search')

	context = _context(request)

	get = request.GET.get

	query_string = get('q', None)
	tax_group = get('tax_group', None)
	collection = get('collection', None)
	exp_type = get('type', None)
	tf_class = get('class', None)
	tf_family = get('family', None)
	tax_id = get('tax_id', None)
	version = get('version', None)
	has_tffm = get('has_tffm', None)

	# Determine if the advanced controls need showing.

	have_advanced = True in map(lambda s: s and s != 'all',
		[tax_group, collection, exp_type, tf_class, tf_family, tax_id, version, has_tffm])

	# Querying conveniences.

	proteins_with = MatrixProtein.objects.values_list('matrix_id').filter
	matrix_species = MatrixSpecies.objects.values_list('matrix_id')
	matrix_with_species = matrix_species.filter
	matrix_with = Matrix.objects.filter

	queryset = None

	if query_string is None:
		results = None
	else:
		# If the user is searching with matrix id, then show the detail page.

		id_query = query_string.split('.')

		if len(id_query) == 2:
			if len(matrix_with(base_id=id_query[0], version=id_query[1])) > 0:
				return redirect('/matrix/%s' % query_string)

		# If collection is not set, set it to CORE.

		if not collection:
			collection = 'CORE'

		# Get matrix ids by searching from different models.

		annotations = MatrixAnnotation.objects.values_list('matrix_id')
		queryset = Matrix.objects.all()

		# Filter based on tax group.

		if tax_group and tax_group != 'all':
			q = annotations.filter(tag='tax_group', val=tax_group.lower())
			queryset = queryset.filter(id__in=q)

		# Filter based on experiment type.

		if exp_type and exp_type != 'all':
			q = annotations.filter(tag='type', val=exp_type)
			queryset = queryset.filter(id__in=q)

		# Filter based on tf class.

		if tf_class and tf_class != 'all':
			q = annotations.filter(tag='class', val=tf_class)
			queryset = queryset.filter(id__in=q)

		# Filter based on tf family.

		if tf_family and tf_family != 'all':
			q = annotations.filter(tag='family', val=tf_family)
			queryset = queryset.filter(id__in=q)

		# Filter based on query string.

		if query_string:

			# Search matrix occurrences using the string.

			matrix_set = matrix_with(
				Q(name__icontains=query_string) |
				Q(base_id__icontains=query_string))

			# Search for matrix occurrences using gene synonym
			# information.

			matrix_set = matrix_set | matrix_with(
				genes__in=Gene.objects.filter(
					gene_id__in=Synonym.objects.filter(
						synonym__icontains=query_string).values_list('gene_id')))

			# Search annotations using the string.

			refined_annotations = MatrixAnnotation.objects.filter(
				val__icontains=query_string).values_list('matrix_id')

			# Search protein accessions using the string.

			proteins = proteins_with(acc=query_string)

			# Search species identifiers using the string.

			species = matrix_with_species(
				Q(tax_id=query_string) |
				Q(tax_id__species__icontains=query_string))

			# Obtain the union of the identifiers from the above results
			# intersected with the annotation identifiers and specific
			# organisms already defined.

			queryset = queryset & (
				matrix_set |
				matrix_with(id__in=refined_annotations) |
				matrix_with(id__in=proteins) |
				matrix_with(id__in=species)
				)

		# Apply species and collection filtering globally.

		if tax_id and tax_id != 'all':
			queryset = queryset.filter(id__in=matrix_with_species(tax_id=tax_id))

		if collection and collection != 'all':
			queryset = queryset.filter(collection=collection.upper())

		if has_tffm and has_tffm == '1':
			queryset = queryset.filter(base_id__in=Tffm.objects.values_list('matrix_base_id'))

		# If version is set to latest, then get the latest version.

		if version == 'latest':
			queryset = utils.get_latest_versions(queryset).order_by("base_id")

		# Order, paginate and aggregate matrix information from other
		# tables to produce the results.

		queryset = queryset.distinct()
		queryset = queryset.order_by("base_id")
		queryset = _paginate(request, queryset)
		results = _get_matrix_detail_info(queryset)

	data_tag = utils.get_data_tag()

	context.update({
		'data_tag': data_tag,
		'matrices': results,
		'pages': queryset,
		'have_advanced': have_advanced,
		})
	context.update(_get_advanced_search_data())
	
	return render(request, 'portal/search.html', context)

@cache_page(CACHE_TIMEOUT)
def browse_collection(request, collection):
	'''
	Browse the given collection.
	'''

	setattr(request, 'view', 'collection')
	setattr(request, 'collection', collection.upper())

	context = _context(request)

	# Order, paginate and aggregate matrix information from other
	# tables to produce the results.

	queryset = Matrix.objects.filter(collection=collection.upper()).order_by('base_id')
	queryset = _paginate(request, queryset)
	results = _get_matrix_detail_info(queryset)
	data_tag = utils.get_data_tag()

	context.update({
		'data_tag': data_tag,
		'matrices': results,
		'pages': queryset,
		'collection': collection,
		})

	return render(request, 'portal/browse_collection.html', context)

@cache_page(CACHE_TIMEOUT)
def matrix_detail(request, matrix_id):
	'''
	Show the details of a matrix indicated by the given matrix_id.
	'''

	(base_id, version) = utils.split_id(matrix_id)

	# Handle requests for unrecognised profiles.

	try:
		matrix = Matrix.objects.get(base_id=base_id, version=version)
	except Matrix.DoesNotExist:
		return matrix_removed(request, matrix_id)

	annotations = MatrixAnnotation.objects.filter(matrix_id=matrix.id)

	# Restructure annotation data.

	annotation = utils.map_annotations(annotations)
	proteins = MatrixProtein.objects.filter(matrix_id=matrix.id)
	species = MatrixSpecies.objects.filter(matrix_id=matrix.id)

	try:
		tffm = Tffm.objects.get(matrix_base_id=base_id, matrix_version=version)
	except Tffm.DoesNotExist:
		tffm = None

	revcomp = request.method == 'GET' and request.GET.get('revcomp') == '1'

	tfbs_info = utils.tfbs_info_exist(base_id=base_id, version=version)

	matrixdata = _get_matrix_data(matrix.id, revcomp=revcomp)

	if matrix.collection == 'UNVALIDATED':
		versions = _get_same_name_data(matrix.name)
	else:
		versions = _get_versions_data(base_id)

	# Obtain wordclouds, first employing the taxonomy group to select the
	# section of the generated images.

	tax_groups = MatrixAnnotation.objects.filter(matrix_id__in=[matrix.id]) \
			.filter(tag='tax_group').values_list("val", flat=True)
	tax_group = tax_groups and tax_groups[0] or None
	wordclouds = []

	# Ensure that there is a taxonomy group and that the wordcloud images
	# exist.

	if tax_group:
		wordclouds_dir = join(STATIC_ROOT, "wordclouds", tax_group, "figs")
		if exists(wordclouds_dir):

			# Obtain the genes associated with the profile, using
			# their details to identify each image.

			for gene in matrix.genes.all():
				filename = join(wordclouds_dir, "%s.png" % gene.gene_id)
				if exists(filename):

					# Generate a suitable URL for the image.

					wordcloud = "%s/figs/%s.png" % (tax_group, gene.gene_id)
					wordclouds.append(wordcloud)

	# Prepare the details for the page.

	context = _context(request)
	context.update({
		'data_tag': utils.get_data_tag(),
		'matrix': matrix,
		'proteins': proteins,
		'species': species,
		'versions': versions,
		'tffm': tffm,
		'wordclouds': wordclouds,
		})

	context.update(tfbs_info)
	context.update(matrixdata)
	context.update(annotation)

	# If requested, remove the current profile if it is in any cart.

	if request.method == 'GET':
		cart = request.session.get('imatrix_ids', None)
		if cart:
			matrix_id = request.GET.get('remove')
			if matrix_id and matrix_id in cart:
				cart.remove(matrix_id)
				request.session['imatrix_ids'] = cart

			queryset = utils.get_matrix_instances(cart)
			results = _get_matrix_detail_info(queryset)
			request.session['cart'] = results[:5]

		# Add a curation form to the page if viewing an unvalidated
		# profile.

		if matrix.collection == 'UNVALIDATED':
			form = CurationForm()
			context.update({'form': form})

	# Process any submitted curation information.

	elif request.method == 'POST':
		form = CurationForm(request.POST)

		if process_feedback(request, form):
			return HttpResponseRedirect('/matrix/%s' % matrix_id)

		# Show the form again if submission failed.

		context.update({'form': form})

	return render(request, 'portal/matrix_detail.html', context)

@cache_page(CACHE_TIMEOUT)
def matrix_removed(request, matrix_id):
	'''
	Show the details of a removed or absent matrix indicated by the given matrix_id.
	'''

	context = _context(request)

	base_id = utils.get_base_id(matrix_id)
	releases = MatrixHistory.objects.filter(base_id=base_id).order_by('release')

	if releases:
		context['first_release'] = releases.first().release
		context['last_release'] = releases.last().release

	context.update({
		'base_id': base_id,
		'matrix_id': matrix_id,
		})

	return render(request, 'portal/matrix_removed.html', context)

def profile_inference(request):
	'''
	This will inference a profile based on a protein sequence
	'''
	setattr(request, 'view', 'inference')

	context = _context(request)

	# Handle missing inference functionality.

	if not context['have_inference']:
		return render(request, 'portal/feature_absent.html', context)

	# If request is not POST, initialize an empty form.

	form = InferenceForm(request.POST or None)

	# Check whether the form is valid.

	if form.is_valid():

		# Obtain the submitted sequence.

		sequence = form.cleaned_data['sequence']

		# Obtain inferred motifs for the sequence.

		inferences = utils.motif_infer(sequence)
		matrices = []

		for values in inferences:
			(seq_record_id, name, matrix_id, evalue, query_start_end,
				target_start_end, dbd) = values[:7]

			data = {
				'matrix_id': matrix_id,
				'name': name,
				'evalue': evalue,
				'dbd': dbd,
				}

			if matrix_id:
				internal_id = utils.get_internal_id(matrix_id)
				data['id'] = internal_id

			matrices.append(data)

		data_tag = utils.get_data_tag()

		context.update({
			'matrices': matrices,
			'data_tag': data_tag,
			})

		return render(request, 'portal/profile_inference_results.html', context)

	# Without a valid form, just show a form.

	else:
		context.update({"form": form})

		return render(request, 'portal/profile_inference.html', context)

def matrix_align(request):
	'''
	This will take a custom matrix or IUPAC string and align it to CORE (default) or to a selected collection
	'''
	setattr(request, 'view', 'align')

	context = _context(request)

	# If request is not POST, initialize an empty form.

	form = AlignForm(request.POST or None)

	# Check whether the form is valid.

	if not form.is_valid():
		context.update({"form": form})
		return render(request, 'portal/align.html', context)

	pfm1_input = request.POST.get('matrix')
	matrix_type = request.POST.get('matrix_type')

	# NOTE: Could test the input against a pattern.

	#pattern = re.compile("^([\[\]][ACGT]+)+$")
	#pattern.match(pfm1_input):

	# Convert the notation according to matrix type.

	if matrix_type == 'jaspar':
		for old, new in [(' [ ', ""), ('[',""), (' ]', ""), (']',""), ('A',""), ('C',""), ('G',""), ('T',"")]:
			pfm1_input = pfm1_input.replace(old, new)

	# NOTE: matrix_type 'iupac' needs to be implemented.

	width = float(len(pfm1_input.split('\n')[0].split(' ')))

	# Write the matrix to a file for use with the matrix aligner.

	pfm1 = join(TEMP_DIR, _get_filename('matrix_align', '.pfm'))

	with open(pfm1, 'w') as pfm1_file:
		pfm1_file.write(pfm1_input)

	# Get matrix data from the indicated collection.

	collection = form.cleaned_data['collection']
	tax_groups = form.data.getlist('tax_groups')
	version = form.cleaned_data['version']

	queryset = Matrix.objects.filter(collection=collection)

	# Filter based on tax group.

	annotations = MatrixAnnotation.objects.filter(
		tag='tax_group', val__in=tax_groups).values('matrix_id')

	queryset = queryset.filter(id__in=annotations)

	# Get the latest versions if requested.

	if version == 'latest':
		queryset = utils.get_latest_versions(queryset).order_by("base_id")

	data_tag = utils.get_data_tag()
	matrices = []

	for matrix in queryset:
		matrix_id = utils.get_matrix_identifier(matrix)

		# Run the matrix_aligner tool with the input matrix and existing
		# profile matrix.

		pfm2 = join(BASE_DIR, 'download', 'data', data_tag, 'pfm', '%s.pfm' % matrix_id)
		pfm2_new = join(TEMP_DIR, _get_filename('matrix_align_%s' % matrix_id, '.pfm'))

		# Remove any header line.

		with open(pfm2) as pfm2_file, open(pfm2_new, 'w') as pfm2_new_file:
			for line in pfm2_file.readlines():
				if not line.startswith('>'):
					pfm2_new_file.write(line)

		cmd = [join(BIN_DIR, 'matrix_aligner'), pfm1, pfm2_new]

		# Obtain the last line of output and try and interpret it.

		score = 0
		try:
			try:
				lines = subprocess.Popen(cmd, stdout=subprocess.PIPE).stdout.readlines()
				last_line = lines[len(lines)-1]
				if last_line is not None:
					results = last_line.split(b'\t')
					score = float(results[3])
					#pfm1_length = int(results[4])
					#pfm2_length = int(results[5])

			# Handle missing or ill-formed output fields (or general failure).
			# NOTE: Having a proper result code would also be useful.

			except:
				return redirect('/align?error=1')

		# Always remove the temporary file.

		finally:
			os.remove(pfm2_new)

		pfm2_length = MatrixData.objects.filter(id=matrix.id, row='A').order_by('-col')[0]

		if pfm2_length.col < width:
			width = pfm2_length.col

		rel_score = 100.0*score/float(width*2)

		data = {
			'id': matrix.id,
			'matrix_id': matrix_id,
			'name': matrix.name,
			'collection': matrix.collection,
			'score': score,
			'percent_score': rel_score
			}

		matrices.append(data)

	# Remove the supplied matrix.

	os.remove(pfm1)

	context.update({
		'data_tag': data_tag,
		'matrices': matrices,
		'matrix': request.POST.get('matrix'),
		})

	return render(request, 'portal/align_results.html', context)

def analysis(request):
	'''
	Perform analysis on the selected profiles or profiles in the cart.
	'''

	context = _context(request)

	# Delete older temp files.

	_delete_temp_files(path=TEMP_DIR, days=TEMP_LIFE)

	# Show the search page given a lack of form data.

	if request.method != 'POST':
		message = "Please select models to perform analysis."
		context.update({
			"message": message,
			})

		return render(request, 'portal/search.html', context)

	# Populate data from the request.

	get = request.POST.get
	cart = get('cart_data', None)

	# Internal matrix ids in cart or in form data.

	if cart:
		imatrix_ids = request.session.get('imatrix_ids')
	else:
		imatrix_ids = request.POST.getlist('matrix_id')

	# Check the analysis type.

	if get('scan'):
		analysis_type = 'Scan'
		(results, matrix_ids) = _scan_matrix(imatrix_ids,
			get('scan_sequence'),
			get('threshold_score')
			)

	elif get('cart'):
		analysis_type = 'Add to cart'

		# Add matrices to the cart, obtaining the number added.

		profiles_added =_add_to_cart(request, imatrix_ids)
		request.session['message'] = "You have added %s profile(s) to the cart." % profiles_added

		collection_data = get('collection_data')
		page_number = get('page_number')
		page_size = get('page_size')
		inference_data = get('inference_data')
		profile_data = get('profile_data')

		# A lack of referrer should just redirect to the cart.

		referrer = request.META.get('HTTP_REFERER')

		if inference_data or not referrer:
			return redirect('view_cart')

		# Redirects to pages without parameters.

		if "?" not in referrer:
			redirect_url = '%s?cart=1' % referrer

		# Redirects to pages with parameters.

		else:
			redirect_url = '%s&cart=1' % referrer

		return HttpResponseRedirect(redirect_url)

	elif get('permute'):

		analysis_type = 'Permute'
		(results, matrix_ids) = _permute_matrix(imatrix_ids,
			get('permute_type'),
			get('permute_format')
			)

	elif get('cluster'):
		analysis_type = 'Cluster'
		(results, matrix_ids) = _cluster_matrix(imatrix_ids,
			get('tree'),
			get('align'),
			get('ma'),
			get('cc')
			)

	elif get('randomize'):
		analysis_type = 'Randomize'
		(results, matrix_ids) = _randomize_matrix(imatrix_ids,
			get('randomize_format'),
			get('randomize_count')
			)

	elif get('download'):
		analysis_type = 'Download'
		(results, matrix_ids) = _download_matrix(imatrix_ids,
			get('download_type', 'individual'),
			get('download_format', 'jaspar')
			)
	else:
		results = 'Something went wrong!'

	data_tag = utils.get_data_tag()

	context.update({
		"data_tag": data_tag,
		"results": results,
		'matrix_ids': matrix_ids,
		'analysis_type': analysis_type,
		'temp_life': TEMP_LIFE,
		})

	return render(request, 'portal/analysis_results.html', context)

@cache_page(CACHE_TIMEOUT)
def html_binding_sites(request, matrix_id):

	data_tag = utils.get_data_tag()
	sites_file = join(BASE_DIR, 'download', 'data', data_tag, 'sites', '%s.sites' % matrix_id)

	sites = _get_html_binding_sites(sites_file)

	context = _context(request)
	context.update({
		'sites': sites,
		'matrix_id': matrix_id,
		})

	return render(request, 'portal/html_binding_sites.html', context)

def view_cart(request):
	'''
	Show the cart page with list of profiles.
	'''
	setattr(request, 'view', 'cart')

	cart = request.session.get('imatrix_ids')
	removed = False

	if cart:
		matrix_id = request.GET.get('remove')
		if matrix_id and matrix_id in cart:
			cart.remove(matrix_id)
			request.session['imatrix_ids'] = cart
			removed = matrix_id
		queryset = utils.get_matrix_instances(cart)
		results = _get_matrix_detail_info(queryset)
		request.session['cart'] = results[:5]
	else:
		results = None

	context = _context(request)
	data_tag = utils.get_data_tag()

	context.update({
		'data_tag': data_tag,
		'matrices': results,
		'removed': removed,
		})

	return render(request, 'portal/cart.html', context)

def empty_cart(request):
	'''
	Empty the cart of all matrix identifiers.
	'''
	setattr(request, 'view', 'cart')

	if request.session.has_key('imatrix_ids'):
		del request.session['imatrix_ids']
	if request.session.has_key('cart'):
		del request.session['cart']

	context = _context(request)
	data_tag = utils.get_data_tag()

	context.update({
		'data_tag': data_tag,
		'matrices': None,
		})

	return render(request, 'portal/cart.html', context)

@cache_page(CACHE_TIMEOUT)
def matrix_clustering(request):
	setattr(request, 'view', 'cluster')

	context = _context(request)

	if not context['have_clustering']:
		return render(request, 'portal/feature_absent.html', context)

	tax_groups = list(utils.get_tax_groups(clustering=True))
	context.update({
		'featured_tax_groups': tax_groups,
		})
	return render(request, 'portal/clustering_home.html', context)

@cache_page(CACHE_TIMEOUT)
def radial_tree(request, tax_group):
	setattr(request, 'view', 'cluster')

	context = _context(request)
	context.update({'tax_group': tax_group})
	return render(request, 'portal/clustering_detail.html', context)

@cache_page(CACHE_TIMEOUT)
def familial_profiles(request, tax_group):
	setattr(request, 'view', 'familial_profiles')

	context = _context(request)

	if not context['have_archetypes']:
		return render(request, 'portal/feature_absent.html', context)

	context.update({'tax_group': tax_group})
	return render(request, 'portal/familial_profiles.html', context)

@cache_page(CACHE_TIMEOUT)
def genome_tracks(request):
	setattr(request, 'view', 'tracks')

	context = _context(request)
	context.update({
		'data_tag': utils.get_data_tag(),
		'familial_binding_sites_genomes': _familial_binding_sites_genomes(),
		'familial_binding_sites_prefix': FAMILIAL_BINDING_SITES_PREFIX,
		})
	return render(request, 'portal/genome_tracks.html', context)

@cache_page(CACHE_TIMEOUT)
def documentation(request):
	'''
	Show the documentation page.
	'''
	setattr(request, 'view', 'docs')

	context = _context(request)
	context.update({'collections': utils.get_collections(prioritised=True)})
	return render(request, 'portal/documentation.html', context)

@cache_page(CACHE_TIMEOUT)
def download_data(request):
	'''
	Show the downloads page.
	'''
	setattr(request, 'view', 'downloads')

	# Obtain taxonomic group metadata for presentation purposes.

	tax_groups = list(utils.get_tax_groups().values())
	tax_groups_all = [{'common_name': 'all'}] + tax_groups
	tax_groups_clustering = list(utils.get_tax_groups(clustering=True).values())

	# Make proper records for the collection details.

	all_redundancies = ['non-redundant', 'redundant']
	collections = utils.get_collections(prioritised=True)

	for collection in collections:
		collection.update({
			'subdirectory': collection['name'] == 'CORE' and 'CORE' or 'collections',
			'redundancies': all_redundancies,
			})

	# Prepare the template data.

	context = _context(request)
	context.update({
		'all_redundancies': all_redundancies,
		'clustering_collections': [
			{'name': 'CORE', 'label': 'CORE'},
			{'name': 'UNVALIDATED', 'label': 'CORE + Unvalidated'},
			],
		'clustering_formats': [
			{'label': 'Radial tree [zip]', 'folder': 'radial_trees'},
			{'label': 'Clustering summary [zip]', 'folder': 'interactive_trees'},
			],
		'collections': collections,
		'data_tag': utils.get_data_tag(),
		'data_types': [
			{'name': 'JASPAR', 'suffix': 'jaspar'},
			{'name': 'MEME', 'suffix': 'meme'},
			{'name': 'TRANSFAC', 'suffix': 'transfac'},
			],
		'familial_binding_sites_genomes': _familial_binding_sites_genomes(),
		'familial_binding_sites_prefix': FAMILIAL_BINDING_SITES_PREFIX,
		'formats': ['zip', 'txt'],
		'jaspar_release': 'JASPAR%s' % get_data_version(),
		'tax_groups_all': tax_groups_all,
		'tax_groups_clustering': tax_groups_clustering,
		'tax_groups_clustering_count': len(tax_groups_clustering),
		})

	return render(request, 'portal/downloads.html', context)

# Feedback message content.

feedback_message_text = """\
From: %s <%s>

Message: %s

Matrix ID: %s
"""

def process_feedback(request, form):
	'''
	Submit the community curation form, returning a true value if the form
	was processed, false otherwise.
	'''

	get = request.POST.get

	if form.is_valid():

		# Prepare a message from various form fields.

		# The sender address may be subject to restrictions and should
		# be updated with caution.

		from_email = 'unibindmail@uio.no'

		subject = 'JASPAR Community Curation'
		reporter_email = get('from_email', 'jaspar.team@gmail.com')
		reporter_name = get('from_name', 'NA')
		matrix_id = get('matrix_id', 'NA')
		message = form.cleaned_data['message']

		email = EmailMessage(
			subject,
			feedback_message_text % (reporter_email, reporter_name, message, matrix_id),
			from_email,
			SEND_TO_EMAIL,
			reply_to=[from_email],
			)

		# Send the feedback message.

		try:
			email.send()

		# Handle mail sending failure.

		except BadHeaderError:
			message = "Your message could not be sent! Please try again or contact the site operator for help."
			messages.add_message(request, messages.ERROR, message)
			return False

		# Handle successful submission.

		else:
			message = "Thank you for your contribution! Your contribution has been sent to our curators successfully."
			messages.add_message(request, messages.INFO, message)
			return True
	else:
		return False

@cache_page(CACHE_TIMEOUT)
def api_documentation(request):
	'''
	Show the api documentation page.
	'''
	setattr(request, 'view', 'api-home')

	context = _context(request)
	return render(request, 'restapi/api_home.html', context)

@cache_page(CACHE_TIMEOUT)
def tools(request):
	'''
	Show the tools page.
	'''
	setattr(request, 'view', 'tools')

	context = _context(request)
	return render(request, 'portal/tools.html', context)

@cache_page(CACHE_TIMEOUT)
def contact_us(request):
	'''
	Contact us and feedback page to send email.
	'''
	setattr(request, 'view', 'contact_us')

	context = _context(request)

	if request.method == 'GET':
		form = ContactForm()
	else:
		form = ContactForm(request.POST)
		if form.is_valid():
			subject = form.cleaned_data['subject']
			from_email = form.cleaned_data['from_email']
			message = form.cleaned_data['message']

			email = EmailMessage(
				subject,
				'From: %s\n\nMessage: %s' % (from_email, message),
				from_email,
				SEND_TO_EMAIL,
				reply_to=[from_email],
				)

			try:
				email.send()
			except BadHeaderError:
				context.update({
					'message': 'Invalid header found. Your message did not went through.',
					'message_type': 'error'
					})
			else:
				context.update({
					'message': 'Thank you! Your message has been sent successfully. We will get back to you shortly.',
					'message_type': 'success'
					})

			return render(request, 'portal/contact_us.html', context)

	context.update({'form': form})

	return render(request, 'portal/contact_us.html', context)

@cache_page(CACHE_TIMEOUT)
def enrichment(request):
	'''
	Show the enrichment tools page.
	'''
	setattr(request, 'view', 'enrichment')

	context = _context(request)
	return render(request, 'portal/enrichment.html', context)

@cache_page(CACHE_TIMEOUT)
def faq(request):
	'''
	Show the FAQ page.
	'''
	setattr(request, 'view', 'faq')

	context = _context(request)
	return render(request, 'portal/faq.html', context)

@cache_page(CACHE_TIMEOUT)
def changelog(request):
	'''
	Show the changelog page.
	'''
	setattr(request, 'view', 'changelog')

	context = _context(request)
	return render(request, 'portal/changelog.html', context)

@cache_page(CACHE_TIMEOUT)
def tour_video(request):
	'''
	Show the tour video page.
	'''
	setattr(request, 'view', 'tour')

	context = _context(request)
	return render(request, 'portal/tour_video.html', context)

@cache_page(CACHE_TIMEOUT)
def about(request):
	'''
	Show the about page.
	'''
	setattr(request, 'view', 'about')

	profiles = Matrix.objects.filter
	annotations = MatrixAnnotation.objects.filter

	context = _context(request)

	for tax_group in utils.get_tax_groups():
		tax_group = tax_group.name.lower()
		tax_annotations = annotations(tag='tax_group', val=tax_group)
		context[tax_group] = profiles(collection="CORE", id__in=tax_annotations).count()

	return render(request, 'portal/about.html', context)

@cache_page(CACHE_TIMEOUT)
def post_details(request, year, month, day, slug):
	'''
	Show an individual news/update post.
	'''

	post = get_object_or_404(Post, slug=slug)
	posts = Post.objects.all().order_by('-date')[:5]

	context = _context(request)
	context.update({
		'post': post,
		'posts': posts,
		})
	return render(request, 'portal/blog_single.html', context)

@cache_page(CACHE_TIMEOUT)
def post_list(request):
	'''
	List all news/updates.
	'''

	posts = Post.objects.all().order_by('-date')

	context = _context(request)
	context.update({'posts': posts})
	return render(request, 'portal/blog.html', context)

@cache_page(CACHE_TIMEOUT)
def matrix_versions(request, base_id):
	'''
	Show the details of matrix versions for base_id.
	'''
	setattr(request, 'view', 'versions')

	# Order, paginate and aggregate matrix information from other tables to
	# produce the results.

	queryset = Matrix.objects.filter(base_id=base_id).order_by('version')
	queryset = _paginate(request, queryset)
	results = _get_matrix_detail_info(queryset)
	data_tag = utils.get_data_tag()

	context = _context(request)
	context.update({
		'data_tag': data_tag,
		'matrices': results,
		'pages': queryset,	
		})
	
	return render(request, 'portal/search.html', context)

@cache_page(CACHE_TIMEOUT)
def profile_versions(request):
	'''
	Show the profile versions page.
	'''
	setattr(request, 'view', 'profile-versions')

	# Constrain versions to those included in this and earlier releases.

	data_version = get_data_version()
	historical_profiles = MatrixHistory.objects.filter(release__lte=data_version)
	historical_releases = Release.objects.filter(year__lte=data_version)

	# Obtain actual and mentioned releases in order.

	actual_releases = list(historical_releases.values_list('year', flat=True)
						.order_by('year'))

	# Releases taken from the history may not in all cases correspond to
	# actual releases (2012 is used for versioning purposes but is not an
	# actual release).

	releases = list(historical_profiles.values_list('release', flat=True)
					.distinct()
					.order_by('release'))
	final_release = releases[-1]

	# Aggregate the profile versions.

	profiles = defaultdict(dict)

	for profile in historical_profiles:
		profiles[profile.base_id][profile.release] = profile

	# Obtain a table with profiles as rows and releases as columns.

	base_ids = list(profiles.keys())
	base_ids.sort()

	table = []

	for base_id in base_ids:
		profile_versions = profiles[base_id]
		profile_releases = list(profile_versions.keys())
		profile_releases.sort()

		# Add a column for each release.

		columns = []
		active = None

		for release in releases:
			profile = profile_versions.get(release)

			# Handle a profile version for a release.

			if profile:
				# Profiles are either new, updated or unchanged.

				new_profile = not active
				updated_profile = active and active.matrix_id != profile.matrix_id

				if new_profile or updated_profile or release == final_release:
					columns.append({
						'matrix_id': profile.matrix_id,
						'release': release,
						'status': new_profile and 'new' or updated_profile and 'updated' or '',
						})
					active = profile
				else:
					columns.append({})

			# Handle the absence of a profile version.

			elif active:
				columns.append({'status': 'removed'})
				active = None
			else:
				columns.append({})

		# Make a new profile record.

		table.append({
			'base_id': base_id,
			'name': list(profile_versions.values())[0].name,
			'profile_versions': columns,
			})

	data_tag = utils.get_data_tag()

	context = _context(request)
	context.update({
		'actual_releases': actual_releases,
		'data_tag': data_tag,
		'releases': releases,
		'profiles': table,
		})

	return render(request, 'portal/profile_versions.html', context)

def url_redirection(request):
	'''
	Redirect the old URL request to the new URL structure.
	'''

	get = request.GET.get

	matrix_id = get('ID', None)
	collection = get('db', None)
	tax_group = get('tax_group', None)

	url_path = request.get_full_path()
	flat_file_dir = '/download/all_data/FlatFileDir'

	if matrix_id:
		return redirect('/matrix/%s' % matrix_id)
	elif collection and tax_group:
		return redirect('/search/?q=&collection=%s&tax_group=%s' % (collection.upper(), tax_group))
	elif url_path == '/html/DOWNLOAD':
		return redirect('/downloads')

	# Redirect PFM data accesses to the appropriate release-specific
	# location.

	elif url_path.startswith(flat_file_dir):
		remaining = url_path[len(flat_file_dir):]
		data_version = get_data_version()
		return redirect('/download/data/%s/pfm/%s' % (data_version, remaining))

	# Unrecognised locations are redirected to the front page.

	else:
		return redirect('index')

def page_not_found(request, exception):
	'''
	Return custom 404 error page.
	'''

	context = _context(request)
	return render(request, '404.html', context, status=404)

def server_error(request):
	'''
	Return custom 500 error page.
	'''

	context = _context(request)
	return render(request, '500.html', context, status=500)

############################################################################################
#----------------------------- internal functions used by view.py -------------------------#
############################################################################################

def _context(request):
	'''
	Return a context with common definitions.
	'''

	# Obtain the configured version for the site.

	data_version = get_data_version()
	releases = list(utils.get_releases())
	release = None

	if data_version:
		release = utils.get_release(data_version)

	# Attempt to get the latest release if no explicit version is
	# configured.

	latest_release = releases[-1]

	if not release:
		release = latest_release

	# Determine the availability of certain kinds of data.

	have_archetypes = ArchetypesRelease.objects.filter(year=get_data_version())
	have_clustering = ClusteringRelease.objects.filter(year=get_data_version())
	have_familial_binding_sites = have_archetypes
	have_inference = utils.jpi is not None

	# Obtain the collection names.

	collection_names = utils.get_collection_names(prioritised=True)

	return {
		'collection_names': collection_names,
		'have_archetypes': have_archetypes and True or False,
		'have_clustering': have_clustering and True or False,
		'have_familial_binding_sites': have_familial_binding_sites and True or False,
		'have_inference': have_inference,
		'latest_release': latest_release,
		'release': release,
		'release_number_ordinal': _ordinal(release.release_number),
		'releases': releases,
		}

def _familial_binding_sites_genomes():

	"Make genome entries for the familial binding sites data."

	genomes = []

	for genome in Genome.objects.all():
		genomes.append({
			'name': genome.name,
			'species_name': genome.species.name,
			'data_tag': genome.data_tag,
			})

	return genomes

def _ordinal(n):
	'''
	Return an English textual representation of an ordinal number.
	'''

	n = str(n)

	if n.endswith("1"):
		return "%sst" % n
	elif n.endswith("2"):
		return "%snd" % n
	elif n.endswith("3"):
		return "%srd" % n
	else:
		return "%sth" % n

def _paginate(request, queryset):
	'''
	Paginate the given queryset using request parameters and settings.
	'''

	get = request.GET.get

	page = get('page', 1)
	page_size = get('page_size', PAGINATION_DEFAULT)

	if not page_size or int(page_size) > MAX_PAGINATION_LIMIT:
		page_size = PAGINATION_DEFAULT

	paginator = Paginator(queryset, page_size)

	try:
		return paginator.page(page)
	except PageNotAnInteger:
		return paginator.page(1)
	except EmptyPage:
		return paginator.page(paginator.num_pages)

def _get_matrix_detail_info(queryset):
	'''
	Get detail matrix info based on queryset

	@input:
	queryset {QuerySet}
	@return:
	results {dictionary}
	'''

	results = []

	for matrix in queryset:
		matrix_id = utils.get_matrix_identifier(matrix)

		data_dict = {
			'id': matrix.id,
			'matrix_id': matrix_id,
			'base_id': matrix.base_id,
			'version': matrix.version,
			'collection': matrix.collection,
			'name': matrix.name,
			'logo': _get_sequence_logo(matrix_id),
			}

		# Get annotations for each matrix.

		annotations = MatrixAnnotation.objects.filter(matrix_id=matrix.id)

		data_dict.update(utils.map_annotations(annotations))

		# Get species details for each matrix, obtaining taxonomy
		# records referenced by the matrix species records.

		matrix_species = MatrixSpecies.objects.filter(matrix_id=matrix.id).values_list('tax_id')
		all_species = Tax.objects.filter(tax_id__in=matrix_species).values()

		data_dict.update({'species': list(all_species)})

		results.append(data_dict)

	return results

def _get_sequence_logo(matrix_id, input_type='sites', output_type='png', size='medium'):
	'''
	Return resource identifier for a sequence logo generated for the given
	matrix identifier.
	'''

	return "%s.%s" % (matrix_id, output_type)

def _add_to_cart(request, imatrix_ids):
	'''
	Add profiles to cart using ajax to download together.
	'''

	cart = request.session.get('imatrix_ids')

	profiles_added = len(imatrix_ids)

	if cart:
		imatrix_ids.extend(request.session['imatrix_ids'])

		request.session['imatrix_ids'] = list(set(imatrix_ids))

		queryset = utils.get_matrix_instances(request.session['imatrix_ids']).order_by('name')[:5]
		results = _get_matrix_detail_info(queryset)
		request.session['cart'] = results

		profiles_added = abs(len(request.session['imatrix_ids']) - len(cart))
	else:
		request.session['imatrix_ids'] = imatrix_ids
		queryset = utils.get_matrix_instances(imatrix_ids).order_by('name')[:5]
		results = _get_matrix_detail_info(queryset)
		request.session['cart'] = results

	return profiles_added

def _scan_matrix(imatrix_ids, fasta_sequence, threshold_score=80):
	'''
	Scan the matrix models for a fasta sequence

	@input:
	matrix_ids {list}, fasta_sequence {string}, threshold_score {float}
	@return:
	results {list}, matrix_ids {list}
	'''

	threshold_score = float(threshold_score)/100

	matrix_ids = []
	results = []

	# Write a temporary file containing the sequence.

	fasta_filename = _get_filename('scan_fasta_file', '.txt')
	fasta_pathname = join(TEMP_DIR, fasta_filename)

	with open(fasta_pathname, 'w+') as fasta_file:
		fasta_file.write(fasta_sequence)

	# Process each matrix.

	for matrix in utils.get_matrix_instances(imatrix_ids):
		matrix_ids.append(utils.get_matrix_identifier(matrix))

		# Read the matrix and obtain the position-specific scoring matrix.

		pssm = _get_pssm(matrix)

		max_score = pssm.max
		min_score = pssm.min
		abs_score_threshold = (max_score - min_score) * threshold_score + min_score

		for record in SeqIO.parse(fasta_pathname, "fasta"):

			# Ignore any matrix profile whose sequence is too long
			# to scan against the submitted sequence.

			if len(record.seq) < pssm.length:
				continue

			for position, score in pssm.search(record.seq, threshold=abs_score_threshold):
				if math.isnan(score):
					continue

				rel_score = (score - min_score) / (max_score - min_score)
				if not rel_score:
					continue

				position_max = position
				strand = "+"
				if position_max < 0:
					strand = "-"
					position_max = position + len(record.seq)

				start_position = position_max + 1
				end_position = position_max + pssm.length

				sequence = record.seq[start_position-1:end_position]
				if strand == "-":
					sequence = record.seq[start_position-1:end_position].reverse_complement()

				results.append({
					'matrix_id': utils.get_matrix_identifier(matrix),
					'name': utils.get_matrix_name(matrix),
					'position': position,
					'seq_id': record.id,
					'start': start_position,
					'end': end_position,
					'strand': strand,
					'score': score,
					'rel_score': rel_score,
					'sequence': sequence
					})

	return results, matrix_ids

def _permute_matrix(imatrix_ids, permute_type='intra', permute_format='pfm'):
	'''
	Permute the columns of the selected matrices. Permutation can be
	performed within a matrix (intra) or across all matrices (inter).

	@input:
	matrix_ids {list}, permute_type {string}, permute_format {string}
	@return:
	permuted_file_name {string}, matrix_ids {list}
	'''

	matrix_ids = []

	# Write matrix data to a single temporary file.

	permuted_file_name = _get_filename('permuted_matrices', '.txt')

	with open(join(TEMP_DIR, permuted_file_name), 'w') as matrix_file:

		# Intra-matrix permutation, with the permutation done in the
		# write_matrix function.

		if permute_type == "intra":
			for matrix in utils.get_matrix_instances(imatrix_ids):
				matrix_ids.append(utils.get_matrix_identifier(matrix))
				utils.write_matrix(matrix_file, matrix, permute_format, permute=True)

			return permuted_file_name, matrix_ids

		# Inter-matrix permutation, with permutation done below.

		elif permute_type == "inter":
			all_data = defaultdict(list)
			matrices = []

			for matrix in utils.get_matrix_instances(imatrix_ids):
				matrix_id = utils.get_matrix_identifier(matrix)
				matrix_name = utils.get_matrix_name(matrix)
				data = _get_matrix_data(matrix.id)

				# Aggregate base information.

				for base in utils.letters:
					all_data[base].extend(data[base])

				# Record identifier, name and size information.

				matrices.append((matrix_id, matrix_name, len(data['A'])))

			# Permute columns across all matrices.

			pfm_array = np.array([ all_data['A'], all_data['C'], all_data['G'], all_data['T'] ])
			pfm_array = pfm_array[:, np.random.permutation(pfm_array.shape[1])]

			# Update matrix data with permuted columns.

			start, end = 0, 0

			for matrix_id, matrix_name, matrix_size in matrices:
				end = end + matrix_size - 1

				data = {
					'A': pfm_array[0][start:end],
					'C': pfm_array[1][start:end],
					'G': pfm_array[2][start:end],
					'T': pfm_array[3][start:end],
					}

				matrix_file.write(utils.print_matrix_data(data, matrix_id, matrix_name, format=permute_format))
				start = end + 1

			return permuted_file_name, matrix_ids
		else:
			raise ValueError("Unknown Permute Type %s" % permute_type)

def _randomize_matrix(imatrix_ids, randomize_format='raw', randomize_count=200):
	'''
	Randomize the matrix models

	@input:
	matrix_ids {list}
	@return:
	randomized_file_name {string}, matrix_ids {list}
	'''

	matrix_ids = []

	# Write matrix data to a single temporary file.

	matrix_filename = _get_filename('randomized_matrixfile', '.txt')
	matrix_pathname = join(TEMP_DIR, matrix_filename)

	with open(matrix_pathname, 'w') as matrix_file:
		for matrix in utils.get_matrix_instances(imatrix_ids):
			matrix_ids.append(utils.get_matrix_identifier(matrix))
			utils.write_matrix(matrix_file, matrix, 'jaspar')

	# Run PWMrandom:
	# PWMrandom <inputmatrix> <outputmatrix> <nmatrix> <width>

	randomized_file_name = _get_filename('randomized_matrices', '.txt')
	pwm_path = join(TOOLS_DIR, 'CSC', 'JASPAR', 'PWMrandomization')
	pwm_program = join(BIN_DIR, 'PWMrandom')

	# The current working directory needs to be set to the program's
	# distribution directory. Otherwise, it appears to produce invalid
	# output.

	cmd = [pwm_program, matrix_filename, join(TEMP_DIR, randomized_file_name), str(randomize_count)]
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=pwm_path)

	# To avoid deadlocks and also incomplete output...

	p.communicate()
	p.wait()

	try:
		os.remove(matrix_pathname)
	except:
		pass

	return randomized_file_name, matrix_ids

def _cluster_matrix(imatrix_ids, tree="UPGMA", align="SWA", ma="PPA", cc="PCC"):
	'''
	Cluster the matrix models using STAMP

	cc: Column comparison metric
	ma: Multiple alignment method
	tree: Tree-building method
	align: Alignment method

	@input:
	matrix_ids {list}, cc
	@return:
	results {string}
	'''

	matrix_ids = []

	# Write matrix data to a single temporary file.

	matrix_filename = _get_filename('clustered_matrixfile', '.txt')
	matrix_pathname = join(TEMP_DIR, matrix_filename)

	with open(matrix_pathname, 'w') as matrix_file:
		for matrix in utils.get_matrix_instances(imatrix_ids):
			matrix_ids.append(utils.get_matrix_identifier(matrix))
			utils.write_matrix(matrix_file, matrix, 'transfac')

	score_file = join(TOOLS_DIR, 'stamp', 'ScoreDists', 'JaspRand_%s_%s.scores' % (cc, align))
	stamp_output = _get_filename('stamp_output')

	#Prepare STAMP arguments
	#-tf [input file] - Input dataset of motifs in TRANSFAC format [required!]
	#-sd [score file] - Input file with random score distributions [required!]
	#-cc: Column comparison metric
	#-ma - Multiple alignment method
	#-tree - Tree-building method
	#-align - Alignment method

	cmd = [join(BIN_DIR, 'stamp'),
		'-tf', matrix_pathname,
		'-sd', score_file,
		'-tree', tree,
		'-align', align,
		'-ma', ma,
		'-cc', cc,
		'-out', join(TEMP_DIR, stamp_output)]

	# Run STAMP, logging any messages to a separate file.

	with open(join(TEMP_DIR, '%s.txt' % stamp_output), "w") as stamp_output_file:
		p = subprocess.Popen(cmd, stdout=stamp_output_file)
		p.communicate()
		p.wait()

	# Delete the temp input file.

	try:
		os.remove(matrix_pathname)
	except:
		pass

	# Create a zipfile containing the STAMP output files.

	zip_filename = _get_filename('STAMP_output', '.zip')

	with zipfile.ZipFile(join(TEMP_DIR, zip_filename), 'w', zipfile.ZIP_DEFLATED) as stampzip:
		for filename in ['%s.txt', '%s.tree', '%sFBP.txt']: # '%s_match_pairs.txt', '%s_matched.transfac'
			result_filename = join(TEMP_DIR, filename % stamp_output)
			if exists(result_filename):
				stampzip.write(result_filename, basename(result_filename))

	return zip_filename, matrix_ids

def _download_matrix(imatrix_ids, download_type='individual', download_format='pfm'):
	'''
	Download the matrix models

	@input:
	matrix_ids {list}
	@return:
	file_name {string}, matrix_ids {list}
	'''

	jaspar_release = 'JASPAR%s' % get_data_version()

	# Create a zipfile for the different matrix files.

	if download_type == "individual":

		file_name = _get_filename('%s_individual_matrices' % jaspar_release, '_%s.zip' % download_format)

		matrix_ids = utils.write_matrix_archive(utils.get_matrix_instances(imatrix_ids),
			download_format, file_name)

	elif download_type == "combined":

		file_name = _get_filename('%s_combined_matrices' % jaspar_release, '_%s.txt' % download_format)

		matrix_ids = utils.write_matrix_textfile(utils.get_matrix_instances(imatrix_ids),
			download_format, file_name)
	else:
		raise ValueError("Unknown Download Type %s" % download_type)

	return file_name, matrix_ids

def _delete_temp_files(path=TEMP_DIR, days=TEMP_LIFE):
	'''
	Delete older temp files based on TEMP_DIR and TEMP_LIFE.
	Please change the number of days in the jaspar.settings files

	@input
	path{string}, days{integer}
	'''

	current_time = time.time()

	for f in os.listdir(path):
		f = join(path, f)
		if os.stat(f).st_mtime < current_time - days * DAY_IN_SECONDS:
			os.remove(f)

def _get_current_datetime():
	'''
	Return this instant's datetime in the form YYYYMMDDHHMMSS.
	'''

	return datetime.datetime.now().strftime("%Y%m%d%H%M%S")

def _get_versions_data(base_id):
	'''
	Return all the versions for a given base_id
	'''

	queryset = Matrix.objects.filter(base_id=base_id)
	results = _get_matrix_detail_info(queryset)
	return results

def _get_same_name_data(name):
	'''
	Return all the versions for a given base_id
	'''

	queryset = Matrix.objects.filter(name=name, collection='UNVALIDATED')
	results = _get_matrix_detail_info(queryset)
	return results

def _get_matrix_data(imatrixid, revcomp=False, permute=False):
	'''
	Takes internal matrix id and returns matrix data as a dictionary object.
	'''

	d = {}

	for base in utils.letters:
		d[base] = MatrixData.objects.values_list('val', flat=True).filter(id=imatrixid, row=base).order_by('col')

	# Compute the reverse complement.

	if revcomp:
		d_temp = {}
		d_temp['A'] = d['T'].reverse()
		d_temp['C'] = d['G'].reverse()
		d_temp['G'] = d['C'].reverse()
		d_temp['T'] = d['A'].reverse()

		d = d_temp

	# Permute the matrix.

	if permute:
		pfm_array = np.array([ d['A'], d['C'], d['G'], d['T'] ])
		pfm_array = pfm_array[:, np.random.permutation(pfm_array.shape[1])]

		# Update with permuted columns.

		(d['A'], d['C'], d['G'], d['T']) = (pfm_array[0], pfm_array[1], pfm_array[2], pfm_array[3])

	return d

def _get_advanced_search_data():
	'''
	Return a data dictionary to create advanced search options.
	'''

	exp_type = MatrixAnnotation.objects.filter(tag='type').values('val').distinct().order_by('val')
	tf_family = MatrixAnnotation.objects.filter(tag='family').values('val').distinct().order_by('val')
	tf_class = MatrixAnnotation.objects.filter(tag='class').values('val').distinct().order_by('val')
	tax_groups = MatrixAnnotation.objects.filter(tag='tax_group').values('val').distinct()#.order_by('val')
	collections = Matrix.objects.values('collection').distinct().order_by('collection')
	tax_ids = Tax.objects.filter(tax_id__in = MatrixSpecies.objects.values_list('tax_id', flat=True).distinct()).order_by('species')

	return {
		'exp_type': exp_type,
		'tf_family': tf_family,
		'tf_class': tf_class,
		'tax_ids': tax_ids,
		'tax_groups': tax_groups,
		'collections': collections,
		}

def _get_pssm(matrix):
	'''
	Read an existing profile and return the position-specific scoring
	matrix.
	'''

	matrix_id = utils.get_matrix_identifier(matrix)
	data_version = get_data_version()

	with open(join(BASE_DIR, 'download', 'data', data_version, 'pfm', '%s.pfm' % matrix_id)) as handle:
		motif = _read_pfm(handle)

	motif.pseudocounts = motifs.jaspar.calculate_pseudocounts(motif)
	return motif.pssm

def _get_html_binding_sites(sites):
	'''
	Split sequences in the sites file into three columns, these being the
	region before the site, the site itself, and the region after the site.
	'''

	split_sites = []

	# Handle missing site data.

	if not exists(sites):
		return split_sites

	with open(sites) as sitefile:
		fasta_sequences = SeqIO.parse(sitefile, 'fasta')

		for fasta in fasta_sequences:
			sequence = str(fasta.seq)

			# Isolate the binding site by splitting on capitalised
			# nucleotides.

			site = re.split('([A-Z]+)', sequence, 1)

			# Handle missing site.

			if len(site) == 1:
				site = site + ['', '']

			split_sites.append(site)

	# Handle bad site data.

	if not split_sites:
		return split_sites

	# Get the maximum column width for the sequence columns before and after
	# the binding site region.

	max_width = 0
	for i in [0, 2]:
		max_column_width = max(len(site[i]) for site in split_sites)
		max_width = max(max_width, max_column_width)

	max_site_width = max(len(site[1]) for site in split_sites)

	# Pad the columns to the maximum width.

	for site in split_sites:
		site[0] = site[0].rjust(max_width, " ")
		site[1] = site[1].ljust(max_site_width, " ")
		site[2] = site[2].ljust(max_width, " ")

	return split_sites

def _get_filename(label, suffix=""):
	'''
	Return a unique filename employing the given label and suffix.
	'''
	return '%s_%s_%s%s' % (_get_current_datetime(), label, os.getpid(), suffix)

def _read_pfm(handle):
	'''
	Read PFM data with optional header line from the file handle, returning
	the result of motifs.read on the filtered PFM data.
	'''

	f = StringIO()

	line = handle.readline()
	if not line.startswith('>'):
		f.write(line)
	for line in handle.readlines():
		f.write(line)

	f.seek(0)
	return motifs.read(f, "pfm")
