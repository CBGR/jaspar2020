Settings
========

The Web application's core settings are found in the ``jaspar/settings.py``
file. In order to support different sites providing access to different
versions of JASPAR data, all within the same repository of program code and
associated resources, specific settings files import the core settings and
indicate different database details:

=================================== ====================================
File                                Description
=================================== ====================================
``jaspar/settings2014.py``          JASPAR 2014 data settings
``jaspar/settings2016.py``          JASPAR 2016 data settings
``jaspar/settings2018.py``          JASPAR 2018 data settings
``jaspar/settings2020.py``          JASPAR 2020 data settings
``jaspar/settings2022.py``          JASPAR 2022 data settings
=================================== ====================================

The naming of these files is not significant, although it is helpful to name
them in a descriptive way, indicating which database each of them references.

These settings files are employed by the WSGI handler programs as follows:

=================================== ====================================
File                                Description
=================================== ====================================
``jaspar/wsgi.py``                  Handler for ``jaspar.uio.no``
``jaspar/wsgi2014.py``              Handler for ``jaspar2014.uio.no``
``jaspar/wsgi2016.py``              Handler for ``jaspar2016.uio.no``
``jaspar/wsgi2018.py``              Handler for ``jaspar2018.uio.no``
``jaspar/wsgi2020.py``              Handler for ``jaspar2020.uio.no``
``jaspar/wsgi2022.py``              Handler for ``jaspar2022.uio.no``
=================================== ====================================

Here, a convention is employed where the file naming reflects the site being
provided by the handler. This convention is adopted by the deployment scripts
to generate the site configuration files for the Web server.

The following diagram summarises the relationships between these files, the
sites and the databases.

.. image:: Settings.svg


Versions and Version Tags
-------------------------

Although a distinction currently exists in the code between data versions,
these corresponding to a published release of the database, and data tags,
corresponding to more precise updates of the data, JASPAR does not currently
make use of such a distinction. This was introduced from UniBind where some
use of these separate concepts was made.

Currently, all deployed database versions incorporate a complete list of all
releases in the `Release`_ model.

.. _`Release`: Schema.html#release


Versioned Data Resources
------------------------

Since different versioned collections of resources coexist within a single
directory hierarchy, a site providing data corresponding to the tag ``2022``
will need to construct paths to resources incorporating this tag.  For
example:

::

  download/data/2022/sites/MA0466.1.sites

Here, the ``download/data/2022`` directory contains resources for datasets
belonging to that particular versioned collection. In contrast, for the tag
``2020``, the following path would be generated for the corresponding dataset
from this older version of the data:

::

  download/data/2020/sites/MA0466.1.sites


Common or Unversioned Data Resources
------------------------------------

Some resources are common to all database versions. For example, the sequence
logos shown on matrix detail pages and in the profile history page are all
retrieved from the following location:

::

  static/logos/all

Such logos should not change between releases since a new version of a profile
should be the only way that a new or updated sequence is introduced.
Consequently, a common repository of logos can be employed to provide logos
for all data releases.
