#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain the Python version and amend the path configuration.

PYTHON=`"$THISDIR/tools/python_executable"`
PP=`"$THISDIR/tools/python_path"`

if [ "$PP" ] ; then
    export PYTHONPATH="$PP"
fi

# Create a secret key.

if [ ! -s "$BASEDIR/secret.txt" ] ; then
    "$PYTHON" "$THISDIR/tools/make_secret.py" > "$BASEDIR/secret.txt"
fi

# Initialise static content.

"$PYTHON" "$BASEDIR/manage.py" collectstatic --no-input

# vim: tabstop=4 expandtab shiftwidth=4
