Testing
=======

The JASPAR Web application mostly aims to present a view onto a collection of
static resources augmented by a searching mechanism. Although the structure of
the underlying data is relatively straightforward, the mechanisms for
accessing and interacting it via the Web interface can be somewhat more
complicated:

- The conventional Web interface consists of a number of different pages or
  screens offering navigation possibilities.
- A REST API offers a machine-accessible interface to Web resources providing
  access to the data.

The REST API is also accessible via a Web browser in two different ways:

- The "browsable API" employs the Django REST Framework software.
- The "live API" requires additional client-side resources served by the
  application.

Although interactive, subjective testing is useful ("Does the Web site look
right?"), automated, systematic testing is essential in discovering whether
functionality is provided correctly. To achieve this, a collection of scripts
are provided in the ``tests`` directory of the source code distribution.

General Testing Notes
---------------------

The tests described below involve unit test suites which will, when run,
produce output as described by the unittest_ testing framework's
documentation. Generally, a sequence of dots followed by an ``OK`` message
indicates success; failures or other errors are reported with more details.

Where a site has not been deployed as a secure site, problems may arise when
testing some functionality. These problems can be avoided by changing the
Django application settings_, details of which can be found by running the
appropriate test program with the ``--test-help`` option. For example:

.. code:: shell

  tests/test_api.py --test-help

.. _settings: Settings.rst
.. _unittest: https://docs.python.org/3/library/unittest.html

Web Site Tests
--------------

Web site testing can be as straightforward as being aware of published URLs
and checking whether these URLs yield valid Web pages. However, there is also
a need to interpret Web page content and to provide parameters in requests
made to the site in order to obtain specific output. It can be useful to
attempt to navigate the site programmatically to make sure that links to
resources are correct.

Testing the appearance of the site is beyond the scope of the tests provided
in this distribution. It is assumed that if Web pages provide the appropriate
elements, these elements will be visible and usable. The use of scripting can
confound such assumptions, unfortunately.

- ``test_website.py`` contains a number of tests of Web site functionality

To run the Web site tests from the top level of the software distribution,
testing a site deployed securely at ``test-jaspar.uio.no``:

.. code:: shell

  tests/test_website.py --url https://test-jaspar.uio.no

REST API Tests
--------------

REST API testing is more transactional and potentially simpler than the
testing of human-readable Web pages. Generally, testing of a known interface
structure can be performed, although there can be some merit in attempting
navigation of the structure by obtaining results and using them to
parameterise requests for more results.

- ``test_api.py`` contains a number of tests of REST API functionality

To run the API tests from the top level of the software distribution,
testing a site deployed securely at ``test-jaspar.uio.no``:

.. code:: shell

  tests/test_api.py --url https://test-jaspar.uio.no/api/v1
