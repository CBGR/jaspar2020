#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import defaultdict
from html.parser import HTMLParser
from itertools import islice
from unittest import main, TestCase
import re, requests
import sys

# Program details.

progname = sys.argv[0]

# Web site access definitions.

base_url = "http://localhost:8000"
html_format = "text/html"

# Utility classes and functions.

class Session:
    '''
    A Web site session abstraction, maintaining a referrer and offering a few
    conveniences.
    '''

    def __init__(self, site_url=None):
        self.site_url = site_url or base_url
        self.session = requests.Session()
        self.referrer = None

    def close(self):
        if self.session:
            self.session.close()
            self.session = None

    def make_url(self, path):
        return "%s%s" % (self.site_url, path)

    def set_referrer(self, headers):
        if self.referrer:
            headers["Referer"] = self.referrer

    def send(self, fn, path, params=None, data=None, format=html_format):
        url = self.make_url(path)
        headers = {"Accept" : format}
        self.set_referrer(headers)
        self.referrer = url
        return fn(url, params=params, headers=headers, data=data)

    def GET(self, path, params=None, format=html_format):
        return self.send(self.session.get, path, params=params, format=format)

    def HEAD(self, path, params=None, format=html_format):
        return self.send(self.session.head, path, params=params, format=format)

    def POST(self, path, params=None, format=html_format):
        return self.send(self.session.post, path, data=params, format=format)

    def send_form(self, path, params):
        '''
        First, get any token from the page. Then complete the form. Then post
        the form.
        '''

        response = self.GET(path)
        fields = get_fields(response)
        fields.update(params)
        return self.POST(path, fields)

def make_url(path):
    return "%s%s" % (base_url, path)

def GET(url, params=None, format=html_format):
    headers = {"Accept" : format}
    return requests.get(url, params=params, headers=headers)

def HEAD(url, params=None, format=html_format):
    headers = {"Accept" : format}
    return requests.head(url, params=params, headers=headers)

def get_matrix(matrix_id, format=html_format):
    url = make_url("/matrix/%s/" % matrix_id)
    return GET(url, format=format)

def get_fields(r):
    if r.ok:
        p = FormParser()
        p.feed(r.text)
        return p.fields
    else:
        return {}

def get_links(r):
    if r.ok:
        p = LinkParser()
        p.feed(r.text)
        return p
    else:
        return None

def get_search_results(r):
    if r.ok:
        p = SearchTableParser()
        p.feed(r.text)
        return p.results
    else:
        return {}

def get_scan_results(r):
    if r.ok:
        p = ScanResultsTableParser()
        p.feed(r.text)
        return p.results
    else:
        return {}

# Utility classes.

class CommonParser(HTMLParser):
    '''
    Common parsing routines.
    '''

    def _attrs(self, attrs):
        d = {}
        for name, value in attrs:
            d[name] = value
        return d

class FormParser(CommonParser):
    '''
    Parse HTML documents, obtaining form field values.
    '''

    def __init__(self):
        super().__init__()
        self.fields = {}
        self.name = None
        self.data = None

    def handle_starttag(self, tag, attrs):
        if tag in ("input", "textarea"):
            d = self._attrs(attrs)
            name = d.get("name")

            if tag == "textarea":
                self.name = name
                self.data = ""
                return
            elif tag == "input":
                self.fields[name] = d.get("value")

        self.name = None

    def handle_data(self, data):
        if self.name:
            self.data += data

    def handle_endtag(self, tag):
        if self.name:
            self.fields[self.name] = self.data
            self.name = None

class LinkParser(CommonParser):
    '''
    Parse HTML documents, obtaining links to other resources.
    '''

    targets = {"a" : "href", "img" : "src", "iframe" : "src"}

    def __init__(self):
        super().__init__()
        self.links = {}
        self.images = []
        self.iframes = []
        self.target = None
        self.label = None

    def handle_starttag(self, tag, attrs):
        if tag in self.targets.keys():
            d = self._attrs(attrs)
            target = d.get(self.targets[tag])
            if tag == "img":
                self.images.append(target.strip())
            elif tag == "iframe":
                self.iframes.append(target.strip())
            else:
                self.target = target
                self.label = ""

    def handle_data(self, data):
        if self.target:
            self.label += data

    def handle_endtag(self, tag):
        if tag == "a":
            self.links[self.label.strip()] = self.target.strip()
            self.target = None

class SearchTableParser(CommonParser):
    '''
    Parse HTML documents, obtaining search results from a table.
    '''

    def __init__(self):
        super().__init__()
        self.results = {}
        self.in_table = False

    def handle_starttag(self, tag, attrs):
        d = self._attrs(attrs)

        # Enter the table.

        if not self.in_table and tag == "table" and d.get("id") == "search_table":
            self.in_table = True

        # Enter a result row.

        elif tag == "tr":
            self.matrix_id = None
            self.imatrix_id = None

        # Obtain a matrix name.

        elif tag == "a" and d.get("href", "").startswith("/matrix/"):
            self.matrix_id = d.get("href")[len("/matrix/"):]

        # Obtain a numeric matrix identifier.

        elif tag == "input" and d.get("name") == "matrix_id":
            self.imatrix_id = d.get("value")

    def handle_endtag(self, tag):
        if not self.in_table:
            return

        # Leave the table.

        if tag == "table":
            self.in_table = False

        # Leave a result row.

        elif tag == "tr" and self.matrix_id:
            self.results[self.matrix_id] = self.imatrix_id

class ScanResultsTableParser(CommonParser):
    '''
    Parse HTML documents, obtaining scan results from a table.
    '''

    def __init__(self):
        super().__init__()
        self.results = []
        self.in_table = False

    def handle_starttag(self, tag, attrs):
        d = self._attrs(attrs)

        # Enter the table.

        if not self.in_table and tag == "table" and d.get("id") == "search_table":
            self.in_table = True

        # Enter a result row.

        elif tag == "tr":
            self.matrix_id = None

        # Obtain a matrix name.

        elif tag == "a" and d.get("href", "").startswith("/matrix/"):
            self.matrix_id = d.get("href")[len("/matrix/"):]

    def handle_endtag(self, tag):
        if not self.in_table:
            return

        # Leave the table.

        if tag == "table":
            self.in_table = False

        # Leave a result row.

        elif tag == "tr" and self.matrix_id:
            self.results.append(self.matrix_id)

# Test cases.

class WebTestCase(TestCase):
    '''
    Provide some common test functionality.
    '''

    def _test_page_links(self, page_path, page_title, page_links_label,
                         selected_links_prefix, selected_links_label,
                         allow_zero_links=False):
        '''
        Test links on the page with the given path, having the specified title
        and label for reporting purposes, with the given prefix used to select
        links in the page, and with the given label used to report problems in
        selecting links.
        '''

        response = self._test_page(page_path, page_title, content=True)

        # Obtain links containing collection references.

        parser = get_links(response)

        # With a valid page, the parser should also be valid.

        self.assertTrue(parser and parser.links, page_links_label)

        # Obtain all links.

        links = set()

        for label, link in parser.links.items():
            if link.startswith(selected_links_prefix):
                links.add(link)

        if not links and allow_zero_links:
            return links

        self.assertTrue(links, selected_links_label)

        # Attempt to visit download links but not obtain the full content.

        for link in links:
            self._test_page(link, link[len(selected_links_prefix):].rstrip("/"))

        # Return links in case they should be inspected further.

        return links

    def _test_page(self, path, name, content=False):
        url = make_url(path)
        method = content and GET or HEAD
        response = method(url)
        self._test_page_result(response, "%s page status at %s: %s" % (name, path, response.status_code))
        return response

    def _test_page_result(self, response, message):
        self.assertTrue(response.ok, message)

class PagesTest(WebTestCase):
    '''
    Test the different pages.
    '''

    def test_principal_pages(self):
        '''
        Test the principal pages.
        '''

        self._test_page("/", "Main")
        self._test_page("/about", "About")
        self._test_page("/align", "Align tool")
        self._test_page("/api", "API")
        self._test_page("/cart", "Cart")
        self._test_page("/cart/empty", "Cart empty")
        self._test_page("/changelog", "Changelog")
        self._test_page("/collection/core", "Core collection")
        self._test_page("/docs", "Documentation")
        self._test_page("/downloads", "Downloads")
        self._test_page("/faq", "FAQ")
        self._test_page("/genome-tracks", "Genome tracks")
        self._test_page("/inference", "Inference tool")
        self._test_page("/matrix-clusters", "Matrix clusters")
        self._test_page("/search", "Search")
        self._test_page("/tools", "External tools")

class ClusteringTest(WebTestCase):
    '''
    Test matrix clustering resource availability.
    '''

    def test_clustering_links(self):
        '''
        Test links provided on the matrix clustering page.
        '''

        links = self._test_page_links("/matrix-clusters", "Matrix clustering",
                                      "Matrix clustering page links",
                                      "/matrix-clusters/", "Matrix clustering links",
                                      allow_zero_links=True)

        # Inspect each linked document, looking for iframes.

        for link in links:
            response = self._test_page(link, "Linked content", content=True)
            parser = get_links(response)
            self.assertTrue(parser and parser.iframes, "Embedded clustering content")

            # Test each iframe for the embedded content.

            for iframe_link in parser.iframes:
                self._test_page(iframe_link, "Embedded content")

class CollectionsTest(WebTestCase):
    '''
    Test the collections.
    '''

    def test_collections(self):
        '''
        Obtain the collections and test each of their pages.
        '''

        self._test_page_links("/", "Main",
                              "Main page links",
                              "/collection/", "Main page collection links")

class DownloadsTest(WebTestCase):
    '''
    Test general download availability.
    '''

    def test_download_links(self):
        '''
        Test links provided on the downloads page.
        '''

        self._test_page_links("/downloads", "Downloads",
                              "Downloads page links",
                              "/download/", "Download links")

class ProfileDetailTest(WebTestCase):
    '''
    Test profile detail pages.
    '''

    def test_profile_detail(self):
        '''
        Test the different elements of matrix profile pages.
        '''

        matrix_id = "MA0014.2"
        response = get_matrix(matrix_id)
        self._test_page_result(response, "Detail page status for %s: %s" % (matrix_id, response.status_code))

        # Obtain image links from the page.

        parser = get_links(response)
        if parser:
            self.assertTrue(parser.images, "Detail page images for %s" % matrix_id)

            # Check each image. Some will be navigational, whereas
            # resource-specific images will contain the matrix identifier in the
            # URL.

            for image in parser.images:
                if matrix_id in image:
                    self._test_page(image, "Matrix %s image" % matrix_id)

class FormsTest(WebTestCase):
    '''
    Test forms in the application.
    '''

    # Matrix-based tests.

    example_matrix = "A [13 13 3 1 54 1 1 1 0 3 2 5]\n" \
                     "C [13 39 5 53 0 1 50 1 0 37 0 17]\n" \
                     "G [17 2 37 0 0 52 3 0 53 8 37 12]\n" \
                     "T [11 0 9 0 0 0 0 52 1 6 15 20]"

    example_matrix_results = ["MA1360.1", "MA1359.1"]

    def test_example_alignment(self):
        '''
        Test matrix alignment using the example from the page.
        '''

        session = Session()
        try:
            response = session.send_form("/align", {
                                         "matrix" : self.example_matrix,
                                         "collection" : "CORE",
                                         "tax_groups" : "plants",
                                         "version" : "all",
                                         "Align" : "Align"})

            results = get_search_results(response)

            # Look for expected profiles.

            for matrix_id in self.example_matrix_results:
                self.assertIn(matrix_id, results, "%s expected in alignment results page" % matrix_id)

            self._test_add_to_and_inspect_cart(session, response)

        finally:
            session.close()

    # Sequence-based tests.

    example_sequence = """\
MYSMLDTDMKSPVQQSNALSGGPGTPGGKGNTSTPDQDRVKRPMNAFMVWSRGQRRKMAQ
ENPKMHNSEISKRLGADWKLLSDSEKRPFIDEAKRLRAVHMKDYPDYKYRPRRKTKTLLK
KDKYSLPGNLLAPGINPVSGGVGQRIDTYPHMNGWTNGAYSLMQEQLGYGQHPAMNSSQM
QQIQHRYDMGGLQYSPMMSSAQTYMNAAASTYSMSPAYNQQSSTVMSLASMGSVVKSEPS
SPPPAITSHTQRACLGDLRDMISMYLPPGGDAGDHSSLQNSRLHSVHQHYQSAGGPGVNG
TVPLTHI"""

    example_sequence_results = ["MA0084.1", "MA0077.1"]

    def test_example_inference(self):
        '''
        Test profile inference using the example from the page.
        '''

        # Test for the feature's availability.

        response = self._test_page("/inference", "Inference", True)

        if "Feature absent" in response.text:
            return

        # Attempt to perform profile inference.

        session = Session()
        try:
            response = session.send_form("/inference", {
                                         "sequence" : self.example_sequence,
                                         "ProfileInference" : "JASPAR Profile Infererence"})

            results = get_search_results(response)

            # Look for expected profiles.

            for matrix_id in self.example_sequence_results:
                self.assertIn(matrix_id, results, "%s expected in inference results page" % matrix_id)

            self._test_add_to_and_inspect_cart(session, response)

        finally:
            session.close()

    example_scan_sequence = """\
>seq1
AGAAAAAAAGCTAGCGAGCTAGCTGATCGTAGCTAGCTGATCGATGCTAGCTATGCTCAGCGAT
"""

    example_scan_search_input = "MA0035"

    example_scan_search_result = "MA0035.1"

    example_scan_sequence_results = ["MA0035.1"]

    def test_example_scan(self):
        '''
        Test the scan operation using the example from the page.
        '''

        # Attempt to perform the scan by first searching for profiles and then
        # using them in the scan.

        session = Session()
        try:
            response = session.GET("/search", {"q" : self.example_scan_search_input})

            results = get_search_results(response)

            matrix = self.example_scan_search_result

            self.assertIn(matrix, results, "%s expected in search results page" % matrix)

            matrix_ids = list(results.values())

            params = self._get_params(response, {
                "scan_sequence" : self.example_scan_sequence,
                "threshold_score" : "80",
                "matrix_id" : matrix_ids,
                "scan" : "Scan"})

            response = session.POST("/analysis", params)
            self.assertTrue(response.ok, "analysis page status at /analysis: %s" % response.status_code)

            results = get_scan_results(response)

            # Look for expected profiles.

            for matrix_id in self.example_scan_sequence_results:
                self.assertIn(matrix_id, results, "%s expected in scan results page" % matrix_id)

        finally:
            session.close()

    def _get_params(self, response, params=None):
        '''
        Return parameters from the given response to be employed in a new request.
        '''

        if response:
            fields = get_fields(response)
            if params:
                fields.update(params)
            return fields
        else:
            return None

    def _test_add_to_and_inspect_cart(self, session, response):
        '''
        Select search results, add to cart, and inspect the result.
        '''

        # Obtain numeric matrix identifiers to select profiles.

        results = get_search_results(response)
        matrix_items = list(results.items())[:5]
        matrix_ids = set(map(lambda t: t[0], matrix_items))
        imatrix_ids = set(map(lambda t: t[1], matrix_items))

        response = self._test_add_to_cart(session, imatrix_ids, response)
        self._test_cart_items(session, matrix_ids, response)

    def _test_add_to_cart(self, session, imatrix_ids, response):
        '''
        Test the adding of profiles to the cart.
        '''

        params = self._get_params(response, {
            "matrix_id" : imatrix_ids,
            "cart" : "Add to cart"})

        response = session.POST("/analysis", params)

        self.assertTrue(response.ok, "analysis page status at /analysis: %s" % response.status_code)
        return response

    def _test_cart_items(self, session, matrix_ids, response):
        '''
        Obtain the cart and compare the items to the given matrix identifiers.
        '''

        response = session.GET("/cart", self._get_params(response))

        results = get_search_results(response)
        cart_matrix_ids = set(results.keys())

        self.assertEqual(cart_matrix_ids, matrix_ids, "%s expected in cart: %s" % (matrix_ids, cart_matrix_ids))



# Some additional help.

example_hostname = "test-jaspar.uio.no"

help_text = """\
To test a remote site, specify the --url option as follows:

%s --url http://%s

In order to function correctly, the configuration must allow cookies to be
transmitted over the protocol in use. The following settings must be verified:

 * CSRF_COOKIE_SECURE
 * SESSION_COOKIE_SECURE

It is easier to use a secure site if this has been deployed. For example:

%s --url https://%s
""" % (progname, example_hostname, progname, example_hostname)



# Main program, calling the unit test runner.

if __name__ == '__main__':
    args = sys.argv

    # Handle command arguments before the test runner sees them.

    if len(args) > 1:

        # Show some additional help.

        if args[1] == "--test-help":
            print(help_text, file=sys.stderr)
            sys.exit(1)

        # Obtain any indicated URL for the Web site.

        elif len(args) > 2 and args[1] == "--url":
            base_url = args[2].rstrip("/")
            del args[1:3]

    main()
