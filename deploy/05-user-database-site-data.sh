#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain any data location and use that instead of the default.

DATA=`cat "$THISDIR/work/data.txt"`

# Get the current release.

CURRENT_DATABASE=`for DATABASE in "$BASEDIR/JASPAR"*".sqlite3" ; do echo "$DATABASE" ; done | sort -r | head -n 1`
JASPAR_RELEASE=`"$THISDIR/tools/file_tag" "$CURRENT_DATABASE"`

if [ ! "$JASPAR_RELEASE" ] ; then
    echo "No database detected." 1>&2
    exit 1
fi

# Add existing site data to the database.

SITE_DATA="$DATA/jaspar-site-data-${JASPAR_RELEASE}.sql"

# Since the site data is not critical, its absence will not stop deployment, but
# there should be a test that reports its absence.

if [ ! -e "$SITE_DATA" ] ; then
    echo "Site data not found: $SITE_DATA" 1>&2
    exit 0
fi

for DATABASE in "$BASEDIR/JASPAR"*".sqlite3" ; do

    # Handle the wildcard case.

    if [ ! -e "$DATABASE" ] ; then
        echo "No databases found!" 1>&2
        exit 1
    fi

    sqlite3 "$DATABASE" < "$SITE_DATA"
done

# vim: tabstop=4 expandtab shiftwidth=4
