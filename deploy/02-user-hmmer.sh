#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
WORKDIR="$THISDIR/work"

. "$THISDIR/tools/system.sh"

# Test for one of the programs and exit if it is already available.

if have nhmmer ; then
    exit 0
fi

if [ -e "$BASEDIR/lib/usr/local/bin/hmmstat" ] ; then
    exit 0
fi

# Source archive details.

DISTDIR="hmmer-3.3.2"
ARCHIVE="${DISTDIR}.tar.gz"
URL="http://eddylab.org/software/hmmer/$ARCHIVE"

# Fetch the archive if necessary.

if [ ! -e "$WORKDIR/$ARCHIVE" ] ; then
    wget -P "$WORKDIR" "$URL"
fi

# Unpack the archive.

cd "$WORKDIR"

if [ ! -e "$DISTDIR" ] ; then
    tar zxf "$ARCHIVE"
fi

# Build and install HMMER.

cd "$DISTDIR"
./configure --prefix="$BASEDIR/lib/usr/local"
make
make install

# vim: tabstop=4 expandtab shiftwidth=4
