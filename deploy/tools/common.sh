#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# newer_than <directory> <file>
# Determine whether any files within directory are newer than the file.

newer_than()
{
    # Missing directory: return false.

    if [ ! -e "$1" ] ; then
        return 1
    fi

    # Missing file: return true.

    if [ ! -e "$2" ] ; then
        return 0
    fi

    LINES=`find "$1" -newer "$2" | wc -l`

    # New files: return true.

    if [ "$LINES" != '0' ] ; then
        return 0
    else
        return 1
    fi
}

# vim: tabstop=4 expandtab shiftwidth=4
