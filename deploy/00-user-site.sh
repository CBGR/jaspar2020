#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain any data location and site identifier and use the indicated values
# instead of the default.

DATA=
DATA_DEFAULT="/var/tmp/jaspar-data"
SITE=
SITE_DEFAULT="test-jaspar.uio.no"
SECURE=

while getopts d:s:S NAME ; do
    case "$NAME" in
        d) DATA=$OPTARG ;;
        s) SITE=$OPTARG ;;
        S) SECURE=1 ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

# Record the data and site details for other operations.

if [ ! -e "$THISDIR/work" ] ; then
    mkdir -p "$THISDIR/work"
fi

DATA_FILE="$THISDIR/work/data.txt"

if [ "$DATA" ] || [ ! -e "$DATA_FILE" ] ; then
    echo ${DATA:-$DATA_DEFAULT} > "$DATA_FILE"
fi

SITE_FILE="$THISDIR/work/site.txt"

if [ "$SITE" ] || [ ! -e "$SITE_FILE" ] ; then
    echo ${SITE:-$SITE_DEFAULT} > "$SITE_FILE"
fi

# Make a symbolic link to the secure site definition if the secure option was
# indicated. Any existing link is preserved.

if [ "$SECURE" ] ; then
    SITE_LINK="$THISDIR/conf/${SITE:-$SITE_DEFAULT}.conf-ssl"

    if [ ! -e "$SITE_LINK" ] ; then
        ln -s 'site.conf-ssl' "$SITE_LINK"
    fi
fi

# vim: tabstop=4 expandtab shiftwidth=4
