# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from .settings import *

# Override the default database.

DATABASES['default']['NAME'] = join(BASE_DIR, 'JASPAR2014.sqlite3')

# Override the default data version for this site.

set_data_version('2014')
