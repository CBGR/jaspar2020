#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
TOOLS="$THISDIR/tools"
DOWNLOADS="$BASEDIR/download/database"

# Generate database archives.

if [ ! -e "$DOWNLOADS" ] ; then
    mkdir -p "$DOWNLOADS"
fi

for DATABASE in "$BASEDIR/"*".sqlite3" ; do

    # Handle the wildcard case.

    if [ ! -e "$DATABASE" ] ; then
        echo "No databases found!" 1>&2
        exit 1
    fi

    FILENAME=`basename "$DATABASE"`
    DBNAME=`basename "$DATABASE" .sqlite3`

    # Generate a dump file from the database and compress it.

    DUMP=`mktemp --suffix=.sql`

    "$TOOLS/dump_database" "$DATABASE" "$DUMP"
    gzip -c "$DUMP" > "$DOWNLOADS/$DBNAME.sql.gz"
    rm "$DUMP"
done

# vim: tabstop=4 expandtab shiftwidth=4
