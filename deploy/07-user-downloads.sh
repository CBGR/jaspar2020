#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
TOOLS="$THISDIR/tools"
DOWNLOAD="$BASEDIR/download"

. "$TOOLS/common.sh"



# Generate archives of downloadable data.

for DATADIR in "$DOWNLOAD/data/"* ; do

    # Detect missing directories.

    if [ ! -e "$DATADIR" ] ; then
        break
    fi

    # Ignore any backups and stray files.

    if basename "$DATADIR" | grep -q 'backup' ; then
        continue
    elif [ ! -d "$DATADIR" ] ; then
        continue
    fi

    # Archive each of the subdirectories, putting the archives next to each of
    # the subdirectories.

    for DIRECTORY in bed centrality sites TFFM ; do
        ARCHIVE="$DATADIR/$DIRECTORY.tar.gz"

        if newer_than "$DATADIR/$DIRECTORY" "$ARCHIVE" ; then
            tar zcf "$ARCHIVE" -C "$DATADIR" "$DIRECTORY"
        fi
    done
done



# Generate SVG logos archive.

LOGOS="$BASEDIR/static/logos/all"

# Detect missing directory.

if [ ! -e "$LOGOS" ] ; then
    exit 1
fi

# Archive each subdirectory, putting each archive next to its subdirectory.

TAG=`basename "$LOGOS"`
ARCHIVE="$DOWNLOAD/data/all/sequence_logos_svg.tar.gz"

if newer_than "$LOGOS/svg" "$ARCHIVE" ; then
    if [ ! -e `dirname "$ARCHIVE"` ] ; then
        mkdir -p `dirname "$ARCHIVE"`
    fi
    tar zcf "$ARCHIVE" -C "$LOGOS" "svg"
fi

# vim: tabstop=4 expandtab shiftwidth=4
