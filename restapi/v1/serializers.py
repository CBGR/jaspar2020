# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.http import HttpRequest
from rest_framework import serializers
from portal.models import Matrix, MatrixAnnotation, Tffm, Tax, Release

# URL generation functions.

from .linking import *

class MatrixSerializer(serializers.HyperlinkedModelSerializer):
	matrix_id = serializers.SerializerMethodField()
	url = serializers.SerializerMethodField()
	sequence_logo = serializers.SerializerMethodField()

	#url = serializers.HyperlinkedIdentityField(view_name='matrix-detail', lookup_field='id')

	class Meta:
		model = Matrix
		#fields = ('__all__')
		fields = ('matrix_id', 'name','collection', 'base_id', 'version','sequence_logo','url')
		ordering = ['name']

	def get_matrix_id(self, obj):
		return '%s.%s' % (obj.base_id, obj.version)

	def get_sequence_logo(self, obj):
		return get_sequence_logo(self.context['request'], obj.base_id, obj.version)

	def get_url(self, obj):
		return get_matrix_url(self.context['request'], obj.base_id, obj.version)

class TaxSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.SerializerMethodField()
	matrix_url = serializers.SerializerMethodField()

	class Meta:
		model = Tax
		#fields = ('__all__')
		fields = ('tax_id', 'species','url','matrix_url')
		ordering = ['species']

	def get_url(self, obj):
		return get_species_url(self.context['request'], obj.tax_id)

	get_matrix_url = get_url

class TffmSerializer(serializers.HyperlinkedModelSerializer):
	tffm_id = serializers.SerializerMethodField()
	matrix_id = serializers.SerializerMethodField()
	tffm_url = serializers.SerializerMethodField()
	matrix_url = serializers.SerializerMethodField()

	class Meta:
		model = Tffm
		ordering = ['base_id']
		#fields = ('__all__')
		fields = ('tffm_id', 'name', 'base_id', 'version', 'matrix_base_id',
			'matrix_version', 'matrix_id', 'tffm_url', 'matrix_url',
			'log_p_1st_order', 'log_p_detailed', 'experiment_name')

	def get_tffm_id(self, obj):
		return '%s.%s' % (obj.base_id, obj.version)

	def get_matrix_id(self, obj):
		return '%s.%s' % (obj.matrix_base_id, obj.matrix_version)

	def get_tffm_url(self, obj):
		return get_matrix_tffm_url(self.context['request'], obj.base_id, obj.version)

	def get_matrix_url(self, obj):
		return get_matrix_url(self.context['request'], obj.base_id, obj.version)

class ReleaseSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.SerializerMethodField()

	class Meta:
		model = Release
		ordering = ['release_number']
		fields = ('year','release_number','pubmed_id','website','active','url')

	def get_url(self, obj):
		return get_release_url(self.context['request'], obj.release_number)
