#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Obtain data from the indicated database, writing data definition statements to
# an appropriate location.

PROGNAME=`basename "$0"`
THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../.."`

DEFAULT_DB="$BASEDIR/JASPAR2020.sqlite"
DEFAULT_SITE_DATA="/var/tmp/jaspar-data/jaspar-site-data.sql"

posts()
{
    cat <<EOF
.headers on
.mode insert portal_post
select id, title, content, slug, category, date, author_id from portal_post
EOF
}

releases()
{
    cat <<EOF
.headers on
.mode insert portal_release
select id, year, release_number, release_date, citation, abstract, pubmed_id, doi, website, active from portal_release
EOF
}

permissions()
{
    cat <<EOF
.headers on
.mode insert auth_permission
select id, name, content_type_id, codename from auth_permission
EOF
}

users()
{
    cat <<EOF
.headers on
.mode insert auth_user
select id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined from auth_user
EOF
}

dump()
{
    cat <<EOF
delete from portal_post;
delete from portal_release;
delete from auth_permission;
delete from auth_user;
EOF

    # Portal data.

    posts | sqlite3 "$DB"
    releases | sqlite3 "$DB"

    # Authentication data.

    permissions | sqlite3 "$DB"
    users | sqlite3 "$DB"
}



# Main program.

DB=${1:-"$DEFAULT_DB"}
SITE_DATA=${2:-"$DEFAULT_SITE_DATA"}

if [ ! -e "$DB" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ <database> [ <site data file> ] ]

Need an existing, populated database.
EOF
    exit 1
fi

dump > "$SITE_DATA"
