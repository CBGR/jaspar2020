{% extends 'portal/base.html' %}

{% load static %}

{% block title %}
Documentation
{% endblock %}

{% block content_header %}
JASPAR Documentation
{% endblock %}

{% block breadcrumb %}
  <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
  <li class="active">Docs</li>
{% endblock %}

{% block content %}

<div class="row">

  <div class="col-md-8 col-lg-8 col-xs-12">

    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fas fa-book"></i> JASPAR Documentation</h3>
        <p class="pull-right"><em>Last updated: 15 Sep. 2021</em></p>
      </div>
      <div class="box-body">

        <p>JASPAR is a regularly maintained open-access database storing
        manually curated TF binding preferences as position frequency matrices
        (PFMs). PFMs summarize occurrences of each nucleotide at each position
        in a set of observed TF-DNA interactions. PFMs can be transformed to
        probabilistic models to construct position weight matrices (PWMs) or
        position-specific scoring matrices (PSSMs), which then can be used to
        scan any DNA sequence and predict transcription factors binding sites
        (TFBSs). JASPAR database is also providing TFBSs predicted using
        profiles in the CORE collection.</p>

        <p>The motifs in JASPAR are collected in two ways (Figure 1):</p>

        <ul>

          <li><b>Internally:</b> <em>de novo</em> generated motifs, by analyzing ChIP-seq/-exo sequences using a custom motif discovery pipeline (check the code at our <a href="https://bitbucket.org/CBGR/jaspar_2022_motif_discovery_and_curation_pipeline/src/master/" target="_blank">repository</a>).</li>

          <li><b>Externally:</b> motifs taken directly from other publications
            and/or resources.</li>

        </ul>

        <p>In both cases, the selected motifs are manually curated, that is,
        our curators found an orthogonal publication giving support to the
        motif, (e.g., A motif found in ChIP-seq peaks looks similar to one
        found by SELEX-seq). The Pubmed ID associated with the orthogonal
        support is provided in the TF profile metadata (Figure 1).</p>

        <img src="{% static 'img/documentation_figure1.png' %}" alt="doc_fig1 img" width="99%" />

        <p><b>Figure 1. The workflow of data processing and motif curation for
          the JASPAR database.</b> Motifs in JASPAR database are of two types:
        externally and internally generated motifs. Both types of motifs are
        then passed to manual curation step where motifs are manually
        inspected by a team of curators and orthogonal support in the
        literature is checked.</p>

        <p>JASPAR is the only database with this scope where the data can be
        used with no restrictions (open source). For a comprehensive review of
        models and how they can be used, please see the following reviews:</p>

        <ul>
        <li><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=pubmed&amp;dopt=Abstract&amp;list_uids=25045190&amp;query_hl=1">Modeling the specificity of protein-DNA interactions Quant Biol. 2013 Jun;1(2):115-130</a></li>
        <li><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=pubmed&amp;dopt=Abstract&amp;list_uids=15131651&amp;query_hl=4">Applied bioinformatics for the identification of regulatory elements Nat Rev Genet. 2004 Apr;5(4):276-87</a></li>
        </ul>

        <div id="jaspar-collections" class="section level1">
          <h3 class="page-header">JASPAR Collections</h3>

          <p>The JASPAR database consists of smaller subsets of profiles known
          as collections. Each of these collections have different goals as
          described below. The main collection is known as JASPAR CORE and is
          the collection most scientists use.</p>

          {% for collection in collections %}

          <div id="jaspar-{{ collection.name }}-collection" class="section level2">
            <h4>JASPAR {{ collection.label }}</h4>

            {% with "portal/collections/"|add:collection.name|add:"_verbose.html" as template %}
            {% include template %}
            {% endwith %}

            <p><a href="#top">Back to top</a></p>
          </div>

          {% endfor %}

        </div>

        <div id="jaspar-metadata" class="section level1">
          <h3 class="page-header">JASPAR Metadata</h3>

          <p>Each profile when displayed in the website provides all kinds
          of information. First of all, the metadata describing the profile
          (see Table 1 and Figure 3). Then, the matrix and motif logo are
          displayed. At the bottom of the page there is also information
          available on all versions of the profiles (see Figure 3).</p>

          <table class="table table-hover"  style="width:100%;">
            <colgroup>
              <col width="5%"></col>
              <col width="10%"></col>
              <col width="85%"></col>
            </colgroup>
            <thead>
              <tr class="header">
                <th align="left">#</th>
                <th align="left">Entry</th>
                <th align="left">Note</th>
              </tr>
            </thead>
            <tbody>
              <tr class="odd">
                <td align="left">1</td>
                <td align="left">Name</td>

                <td align="left">The name of the TF. As far as possible, the
                  name is based on the standardized Entrez gene symbols. In
                  the case the model describes a TF hetero-dimer, two names
                  are concatenated, such as Pou5f1::Sox2. In a few cases,
                  one particular TF may have different splice forms
                  resulting in different binding specificity, in previous
                  releases these cases were handled as binding variants
                  (e.g., CEBPG (MA0838.1) and CEBPG(var.2) (MA1636.1)). From
                  the JASPAR 2022 release, binding variants are not anymore
                  indicated as part of the TF name: all binding variants for
                  the same TF share the TF name, but each one has a different
                  Matrix ID.</td>

              </tr>
              <tr class="even">
                <td align="left">2</td>
                <td align="left">Matrix ID</td>

                <td align="left">A unique identifier for each model. CORE
                  matrices always have identifiers starting with “MA”, while
                  profiles in the UNVALIDATED collection have identifiers
                  starting with “UN”. The number after the dot (e.g.,
                  MA1636.1) corresponds to the version.  Larger number
                  indicates that a motif has been updated.</td>

              </tr>
              <tr class="odd">
                <td align="left">3</td>
                <td align="left">Class</td>
                <td align="left">Structural class of the transcription factor,
                based on the <a href="https://doi.org/10.1093/nar/gkx987" target="_blank">TFClass system</a>.</td>
              </tr>
              <tr class="even">
                <td align="left">4</td>
                <td align="left">Family</td>
                <td align="left">Structural sub-class of the transcription factor,
                based on the <a href="https://doi.org/10.1093/nar/gkx987" target="_blank">TFClass system</a>.</td>
              </tr>
              <tr class="odd">
                <td align="left">5</td>
                <td align="left">Collection</td>
                <td align="left">Indicating which collection the profile belongs:
                CORE or UNVALIDATED.</td>
              </tr>
              <tr class="even">
                <td align="left">6</td>
                <td align="left">Taxon</td>
                <td align="left">Group of species, currently consisting of 6
                larger groups: vertebrate, plants, fungi, insects, urochordata
                and nematodes.</td>
              </tr>
              <tr class="odd">
                <td align="left">7</td>
                <td align="left">Species</td>
                <td align="left">The species source for the sequences, in Latin.
                Linked to the NCBI Taxonomic browser. The actual database entries
                are the NCBI tax IDs – the latin conversion is only in the web
                interface.</td>
              </tr>
              <tr class="even">
                <td align="left">8</td>
                <td align="left">Data type</td>
                <td align="left">Methodology used for matrix construction, e.g.,
                ChIP-seq, PBM, SELEX.</td>
              </tr>
              <tr class="odd">
                <td align="left">9</td>
                <td align="left">Validation</td>
                <td align="left">A link to PubMed indicating the orthogonal
                evidence of the Tf binding profile.</td>
              </tr>
              <tr class="even">
                <td align="left">10</td>
                <td align="left">Uniprot ID</td>
                <td align="left">A link to the corresponding UniProt record.</td>
              </tr>
              <tr class="odd">
                <td align="left">11</td>
                <td align="left">Source</td>
                <td align="left">A reference to the data, which was used to build
                a profile and where the profile was taken from.</td>
              </tr>
              <tr class="odd">
                <td align="left">12</td>
                <td align="left">Comment</td>
                <td align="left">For some matrices, a curator comment is added.</td>
              </tr>
            </tbody>
          </table>

          <p><b>Table 1. The metadata categories that are displayed in a profile
          page.</b> Each category of the metadata has a number assigned and is
          also shown in Figure 3.</p>

          <img src="{% static 'img/documentation_figure3.png' %}" alt="doc_fig3 img" width="99%" />

          <p><b>Figure 3. GATA2 profile in JASPAR

          <a href="https://{{ request.get_host }}/matrix/MA0036.3/" target="_blank">(MA0036.3)</a>,

          highlighting the metadata and other information available.</b>
          Metadata categories (green numbers) are explained in more detail
          in Table 1.</p>

          <p><a href="#top">Back to top</a></p>
        </div>

        <div id="jaspar-matrix-formats" class="section level1">
          <h3 class="page-header">JASPAR Matrix formats</h3>

          <p>JASPAR stores transcription factor binding profiles in four
          formats. Following is more information on formats and the DNA
          binding profile for GATA6 transcription factor (JASPAR ID

          <a href="https://{{ request.get_host }}/matrix/MA1104.2/" target="_blank">MA1104.2</a>)

          as an example:</p>

          <div id="raw-pfm-format" class="section level2">
            <h4>Raw PFM</h4>

            <p>Each matrix is separated by a FASTA-like header starting with
            the &gt; symbol and then a matrix ID. The count for each base
            (ACGT) is specified on its own space separated line where each
            element corresponds to one column. The order of the lines for
            the bases is A, C, G and finally T.</p>

            <pre><code>&gt;MA1104.2 GATA6
22320 20858 35360  5912 4535  2560  5044 76686  1507  1096 13149 18911 22172
16229 14161 13347 11831 62936 1439  1393   815   852 75930  3228 19054 17969
13432 11894 10394  7066 6459   580   615   819   456   712  1810 18153 11605
27463 32531 20343 54635 5514 74865 72392  1124 76629  1706 61257 23326 27698
</code></pre>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="jaspar-format" class="section level2">
            <h4>JASPAR</h4>

            <p>This is similar to the raw format, having an identical
            header. The lines for each base however start with a label for
            the nucleotide (A, C, G or T) and then the columns follow
            enclosed in brackets: [].</p>

            <pre><code>&gt;MA1104.2 GATA6
A  [ 22320  20858  35360   5912   4535   2560   5044  76686   1507   1096  13149  18911  22172 ]
C  [ 16229  14161  13347  11831  62936   1439   1393    815    852  75930   3228  19054  17969 ]
G  [ 13432  11894  10394   7066   6459    580    615    819    456    712   1810  18153  11605 ]
T  [ 27463  32531  20343  54635   5514  74865  72392   1124  76629   1706  61257  23326  27698 ]</code></pre>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="transfac-format" class="section level2">
            <h4>TRANSFAC</h4>

            <p>This is a TRANSFAC-like format having a few lines with
            information, such as “AC”, which stores JASPAR matrix unique ID,
            “ID” indicates the TF name and “DE” has both. The data itself is
            transposed as compared to the other formats, meaning that each
            line corresponds to a column in the profile. The column lines
            start with a number denoting the column index (counting from 0).
            Additional lines starting with “CC” store some additional
            metadata, such as TF family and class. A final line of the
            matrix file is indicated with “//”. Empty lines are indicated
            with “XX”.</p>

            <pre><code>AC MA1104.2
XX
ID GATA6
XX
DE MA1104.2 GATA6 ; From JASPAR
PO	A	C	G	T
01	22320.0	16229.0	13432.0	27463.0
02	20858.0	14161.0	11894.0	32531.0
03	35360.0	13347.0	10394.0	20343.0
04	 5912.0	11831.0	 7066.0	54635.0
05	 4535.0	62936.0	 6459.0	 5514.0
06	 2560.0	 1439.0	  580.0	74865.0
07	 5044.0	 1393.0	  615.0	72392.0
08	76686.0	  815.0	  819.0	 1124.0
09	 1507.0	  852.0	  456.0	76629.0
10	 1096.0	75930.0	  712.0	 1706.0
11	13149.0	 3228.0	 1810.0	61257.0
12	18911.0	19054.0	18153.0	23326.0
13	22172.0	17969.0	11605.0	27698.0
XX
CC tax_group:vertebrates
CC tf_family:GATA-type zinc fingers
CC tf_class:Other C4 zinc finger-type factors
CC pubmed_ids:9915795
CC uniprot_ids:Q92908
CC data_type:ChIP-seq
XX
//</code></pre>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="meme-format" class="section level2">
            <h4>MEME</h4>

            <p>MEME motif format is a simple text format for motifs that is
            accepted by the programs in the MEME Suite that require MEME
            Motif Format. A text file in MEME minimal motif format can
            contain more than one motif, and also (optionally) specifies the
            motif alphabet, background frequencies of the letters in the
            alphabet, and strand information (for motifs of complementable
            alphabets like DNA), as illustrated in the example below:</p>

            <pre><code>MEME version 4

ALPHABET= ACGT

strands: + -

Background letter frequencies
A 0.25 C 0.25 G 0.25 T 0.25

MOTIF MA1104.2 GATA6
letter-probability matrix: alength= 4 w= 13 nsites= 79444 E= 0
0.280953  0.204282  0.169075  0.345690
0.262550  0.178251  0.149716  0.409483
0.445093  0.168005  0.130834  0.256067
0.074417  0.148923  0.088943  0.687717
0.057084  0.792206  0.081303  0.069407
0.032224  0.018113  0.007301  0.942362
0.063491  0.017534  0.007741  0.911233
0.965284  0.010259  0.010309  0.014148
0.018969  0.010725  0.005740  0.964566
0.013796  0.955768  0.008962  0.021474
0.165513  0.040632  0.022783  0.771071
0.238042  0.239842  0.228501  0.293616
0.279090  0.226184  0.146078  0.348648
URL https://{{ request.get_host }}/matrix/MA1104.2
</code></pre>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="version-control" class="section level2">
            <h4>Version control</h4>

            <p>Since the 4th release of JASPAR, all matrix models have
            versions. This is primarily to keep track of improvements -
            which can be anything from correcting typos to actually making a
            new model based on new data. Version control works as follows:
            IDs are based on a stable ID, and a version number, so that the
            whole ID is [stable ID].[version]. The stable ID follows a
            certain TF, or other logic unit such as a dimer pair. For
            instance, the stable ID for the factor GATA2 is

            <a href="https://{{ request.get_host }}/matrix/MA0036.3/" target="_blank">MA0036.3</a>.

            However, the GATA2 matrix has been updated two times with new
            data, so there are currently four versions: MA0036.1, MA0036.2,
            and MA0036.3 (See Figure 3).</p>

            <p>The latest versions of all profiles with orthogonal support
            are referred to as JASPAR CORE non-redundant collection (latest
            versions of profiles without orthogonal support are stored in
            JASPAR UNVALIDATED non-redundant). All versions of profiles are
            also available in the database and referred to as redundant
            collection (CORE and UNVALIDATED).</p>

            <p><a href="#top">Back to top</a></p>
          </div>
        </div>

        <div id="accessing-and-downloading-jaspar-data" class="section level1">
          <h3 class="page-header">Accessing and Downloading JASPAR Data</h3>

          <p>Depending on what user wants to download there are several ways
          to access and download JASPAR data. TF binding profiles can be
          downloaded in bulk from the website (all together or per taxon),
          metadata can be retrieved using JASPAR RESTful API or accessed
          through pyJASPAR. Profiles can also be accessed through the
          R/Bioconductor package.</p>

          <div id="jaspar-downloads" class="section level2">
            <h4>JASPAR downloads</h4>

            <p>JASPAR collections in bulk are generally downloadable in
            three different forms from the JASPAR

            <a href="https://{{ request.get_host }}/downloads/" target="_blank">downloads page</a>

            (Figure 4):</p>

            <ul>

              <li>Flat files in JASPAR, MEME and TRANSFAC formats for CORE
                redundant and non-redundant collections of matrix profiles.
                CORE collection is available to download for each specific
                taxonomic group (Figure 4B,H).</li>

              <li>Archives of files (also in JASPAR, MEME and TRANSFAC
                formats for CORE and UNVALIDATED) each providing individual
                matrix profile information (Figure 4D).</li>

              <li>A database/SQL dump of the data and metadata (Figure 4F).</li>

            </ul>

            <img src="{% static 'img/documentation_figure4.png' %}" alt="doc_fig4 img" width="99%" />

            <p><b>Figure 4. Screen grab of JASPAR Download page.</b>
            Download page is accessible through the left panel (A). By
            default “CORE PFMs” download page is displayed (B), where a user
            can find all CORE PFMs or for each of the taxonomic groups for
            download as individual files or a single file (H). The
            UNVALIDATED collection can be downloaded from “JASPAR
            collections (PFMs) (D). Other data, such as TFFMs, matrix
            clustering or the entire SQL dump can also be downloaded (C, E,
            F). TFBSs and matrix clustering are available under “Other data”
            (G).</p>

            <p>Other sources and analysis results data files are also
            available:</p>

            <ul>

              <li>Transcription Factor Flexible Models (TFFMs) are available
                to download as tar files. In addition additional information
                regarding TFFMs is available as a .csv file (Figure
                4C).</li>

              <li>Matrix clustering of CORE and CORE + UNVALIDATED results
                are available to download as radial trees or as clustering
                summaries for each of the taxonomic groups (Figure 4G).</li>

              <li>Other data, such as sequences used to generate PFMs in
                FASTA format, genomic coordinates of the sequences in BED
                format, motif logos and centrality plots are available as
                bulk download under “Other data” (Figure 4G).</li>

            </ul>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="jaspar-rest-api" class="section level2">
            <h4>JASPAR RESTful API</h4>

            <p>Since 2020 JASPAR has a Representational State Transfer
            (REST) application programming interface (API) to query/retrieve
            matrix profile data from JASPAR database. It is a

            <a href="https://{{ request.get_host }}/api/" target="_blank">browsable API</a>

            and comes with a human browsable interface and also programmatic
            interface, which return the results in JSON format. For more
            details, please read the API

            <a href="https://{{ request.get_host }}/api/v1/docs/" target="_blank">documentation</a>.

            If you wish to cite the JASPAR REST API, please check the

            <a href="https://{{ request.get_host }}/faq/" target="_blank">FAQ page</a>.</p>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="pyjaspar" class="section level2">
            <h4>pyJASPAR</h4>

            <p>pyJASPAR is a Pythonic interface to access JASPAR
            transcription factor profiles. It uses Biopython and SQLite3 to
            provide a serverless interface to JASPAR database to query and
            access TF motifs across current and previous releases of the
            database. Currently these releases are available in pyJASPAR:
            JASPAR2014, JASPAR2016, JASPAR2018, JASPAR2020, and JASPAR2022.
            Information on installation of pyJASPAR can be found under

            <a href="https://{{ request.get_host }}/tools/" target="_blank">Tools</a>,

            in the pyJASPAR

            <a href="https://github.com/asntech/pyjaspar" target="_blank">repository</a>

            and

            <a href="https://pyjaspar.readthedocs.io/en/latest/" target="_blank">documentation</a>.</p>

            <p><a href="#top">Back to top</a></p>
          </div>

          <div id="r-bioconductor-package" class="section level2">
            <h4>R Bioconductor Package</h4>

            <p>Different releases of JASPAR can also be accessed through
            Bioconductor data packages. Currently four JASPAR releases are
            available:</p>

            <ul>
              <li><a href="http://bioconductor.org/packages/JASPAR2020/" target="_blank">JASPAR2020</a></li>
              <li><a href="http://bioconductor.org/packages/JASPAR2018/" target="_blank">JASPAR2018</a></li>
              <li><a href="http://bioconductor.org/packages/JASPAR2016/" target="_blank">JASPAR2016</a></li>
              <li><a href="http://bioconductor.org/packages/JASPAR2014/" target="_blank">JASPAR2014</a></li>
            </ul>

            <p>To browse the database TFBSTools package is required.  You
            can find more information about this package and installation
            under Tools and

            <a href="http://bioconductor.org/packages/TFBSTools/" target="_blank">here</a>.</p>

            <p><a href="#top">Back to top</a></p>
          </div>
        </div>

        <div id="browsing-jaspar-motif-clusters" class="section level1">
          <h3 class="page-header">Browsing JASPAR motif clusters</h3>

          <p>Since JASPAR 2018 (7th release) we have introduced a visual
          representation of all motifs (CORE collection and CORE +
          UNVALIDATED collections) as a hierarchical tree displaying a
          global motif alignment (Figure 5). Clusters are accessible by
          clicking on the “Matrix Clusters” button at the left menu on the
          JASPAR website (Figure 5A). For each taxon, separately, all motifs
          are clustered and aligned using the RSAT

          <a href="https://doi.org/10.1093/nar/gkx314" target="_blank">matrix-clustering</a>

          tool, these radial trees can be zoomed and explored more
          closely.</p>

          <img src="{% static 'img/documentation_figure5.png' %}" alt="doc_fig5 img" width="99%" />

          <p><b>Figure 5. Browsing matrix clustering in JASPAR.</b> All
          motifs corresponding to a taxonomic group are displayed as a
          hierarchical tree with a global alignment. A) The TF class
          information is displayed as an outer ring. B) Users can change
          motif logo orientation and trees can be zoomed in to ease the
          motif exploration. C) Each TF name in the tree has a link pointing
          to its corresponding profile page on the JASPAR website.</p>

          <p>The TF classes obtained from

          <a href="https://doi.org/10.1093/nar/gks1123" target="_blank">TFclass</a>

          are displayed in the annotation table, each one with an associated
          number and a color (Figure 5A). The tree is surrounded by an outer
          ring containing the TF classes colors and numbers (Figure 5B).
          The UNVALIDATED motifs are in addition depicted with a star (*)
          next to their TF class (Figure 5B). Users can change TF logo
          alignment orientation by clicking on the Direct/Reverse red
          buttons on top (Figure 5B).</p>

          <p>The branch colors correspond to the clusters found by RSAT

          <a href="https://doi.org/10.1093/nar/gkx314" target="_blank">matrix-clustering</a>,

          note that although TFs from the same family tend to be grouped in
          the same cluster, sometimes members of other TF families are
          grouped together (Figure 5B, see cluster containing TF from
          classes 9 and 10). The tree leaves correspond to TF names, by
          clicking on one of them, it will open a tab in your browser with
          the dedicated profile page (Figure 5D).</p>

          <p><a href="#top">Back to top</a></p>
        </div>

        <div id="jaspar-software-tools" class="section level1">
          <h3 class="page-header">JASPAR Software Tools</h3>

          <p>JASPAR contains not only collections of TF binding profiles,
          but also comes with a suite of tools, such as TFBS enrichment
          tool, For further details please check the <a
          href="https://{{ request.get_host }}/tools/" target="_blank">Tools</a>
          page.</p>

          <p><a href="#top">Back to top</a></p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-lg-4 col-xs-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fas fa-list"></i> Table of contents</h3>
      </div>
      <div class="box-body">
        <div id="TOC">
          <ul>
            <li><a href="#jaspar-collections">JASPAR Collections</a>
              <ul>
                {% for collection in collections %}

                <li><a href="#jaspar-{{ collection.name }}-collection">JASPAR
                    {{ collection.label }}</a></li>

                {% endfor %}
              </ul>
            </li>
            <li><a href="#jaspar-metadata">JASPAR metadata</a></li>
            <li><a href="#jaspar-matrix-formats">JASPAR matrix formats</a>
              <ul>
                <li><a href="#raw-pfm-format">Raw PFM</a></li>
                <li><a href="#jaspar-format">JASPAR</a></li>
                <li><a href="#transfac-format">TRANSFAC</a></li>
                <li><a href="#meme-format">MEME</a></li>
              </ul>
            </li>
            <li><a href="#version-control">Version control</a></li>
            <li><a href="#accessing-and-downloading-jaspar-data">Accessing and downloading JASPAR data</a>
              <ul>
                <li><a href="#jaspar-downloads">JASPAR downloads</a></li>
                <li><a href="#jaspar-rest-api">JASPAR RESTful API</a></li>
                <li><a href="#pyjaspar>py">pyJASPAR</a></li>
                <li><a href="#r-bioconductor-package">R Bioconductor package</a></li>
              </ul>
            </li>
            <li><a href="#browsing-jaspar-motif-clusters">Browsing JASPAR motif clusters</a></li>
            <li><a href="#jaspar-software-tools">JASPAR software tools</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

{% endblock %}

{% block javascript %}

<script>
  // add bootstrap table styles to pandoc tables
  $(document).ready(function () {
    $('tr.header').parent('thead').parent('table').addClass('table table-condensed');
  });
</script>

{% endblock %}
