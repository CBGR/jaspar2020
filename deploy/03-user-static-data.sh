#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
NOW=`date +%Y%m%d`

# Define common storage locations.

DOWNLOAD="$BASEDIR/download"
STATIC="$BASEDIR/static"

# Obtain any data location and use that instead of the default.

DATA=`cat "$THISDIR/work/data.txt"`

if [ ! -e "$DATA" ] ; then
    echo "Source data directory missing: $DATA" 1>&2
    exit 1
fi

# Unpack the data into the output directory.

for FILENAME in `find "$DATA" -maxdepth 1 -name '*.tar*'` ; do

    # Identify the file type.

    FILETYPE=`"$THISDIR/tools/file_type" "$FILENAME"`
    TAG=`"$THISDIR/tools/file_tag" "$FILENAME"`

    # Test for any revised data at different levels.

    HAVE_REVISED=
    IS_REVISED=

    for REVISION in "revised_fixed" "revised" ; do

        # Look for each revision level for the given data.

        for REVISED in "$DATA/${FILETYPE}_${TAG}_${REVISION}.tar"* ; do
            if [ -e "$REVISED" ] ; then

                # Determine whether the current archive corresponds to this
                # revision level or whether an archive at this level obsoletes
                # the current archive.

                if [ "$FILENAME" = "$REVISED" ] ; then
                    IS_REVISED="$REVISED"
                else
                    HAVE_REVISED="$REVISED"
                fi
                break
            fi
        done

        # Any archive at this revision level either corresponds to the current
        # archive or revises it.

        if [ "$HAVE_REVISED" ] || [ "$IS_REVISED" ] ; then
            break
        fi
    done

    if [ "$HAVE_REVISED" ] ; then
        echo "File $FILENAME superseded by $HAVE_REVISED - not processing." 1>&2
        continue
    fi

    # Set the versioned data root.

    DATADIR="$DOWNLOAD/data/$TAG"

    # Indicate paths for existing data that may be replaced if the archives are
    # newer.

    UNPACKTESTS=

    # Choose a suitable directory for unpacking.

    # Archived profiles are unpacked to a common location.

    if [ "$FILETYPE" = 'matrix_archived' ] ; then
        UNPACKDIR="$DOWNLOAD/data/archived/pfm"

    # Clustering data is unpacked to a specific location.

    elif [ "$FILETYPE" = 'matrix_clustering' ] ; then
        UNPACKDIR="$STATIC/clustering/$TAG"

    # Matrix data files are unpacked directly.

    elif [ "$FILETYPE" = 'matrix_data' ] ; then
        UNPACKDIR="$DATADIR"
        UNPACKTESTS="$DATADIR/bed $DATADIR/centrality $DATADIR/pfm $DATADIR/sites"

    # TFFM download data files are unpacked directly.

    elif [ "$FILETYPE" = 'matrix_data_tffm' ] ; then
        UNPACKDIR="$DATADIR"
        UNPACKTESTS="$DATADIR/TFFM"

    # Download data is unpacked directly.

    elif [ "$FILETYPE" = 'matrix_download' ] ; then
        UNPACKDIR="$DATADIR"
        UNPACKTESTS="$DATADIR/collections $DATADIR/CORE"

    # Logos are unpacked to a common location.

    elif [ "$FILETYPE" = 'matrix_logos' ] ; then
        UNPACKDIR="$STATIC/logos/all"

    # Metadata is unpacked to a specific location.

    elif [ "$FILETYPE" = 'matrix_metadata' ] ; then
        UNPACKDIR="$THISDIR/work/metadata/$TAG"

    elif [ "$FILETYPE" = 'matrix_metadata_tffm' ] ; then
        UNPACKDIR="$THISDIR/work/metadata_tffm/$TAG"

    elif [ "$FILETYPE" = 'matrix_synonyms' ] ; then
        UNPACKDIR="$THISDIR/work/synonyms/$TAG"

    # TFFM data is unpacked to a common location.

    elif [ "$FILETYPE" = 'matrix_tffm' ] ; then
        UNPACKDIR="$STATIC/TFFM"

    # Wordclouds are unpacked to a common location.

    elif [ "$FILETYPE" = 'matrix_wordclouds' ] ; then
        UNPACKDIR="$STATIC/wordclouds"

    else
        echo "Ignoring unrecognised file: $FILENAME" 1>&2
        continue
    fi

    UNPACKTESTS=${UNPACKTESTS:-$UNPACKDIR}

    # Unpack the archive if apparently necessary.
    # NOTE: This relies on filenames being conservative, having no spaces, and
    # NOTE: so on.

    for UNPACKTEST in $UNPACKTESTS ; do
        if [ ! -e "$UNPACKTEST" ] || [ "$FILENAME" -nt "$UNPACKTEST" ] ; then

            echo "Processing file: $FILENAME" 1>&2

            # Back up any existing data.

            for UNPACKTEST in $UNPACKTESTS ; do
                if [ -e "$UNPACKTEST" ] ; then
                    mv -f "$UNPACKTEST" "$UNPACKTEST.backup-$NOW"
                fi
            done

            # Unpack the archive, hopefully producing the same paths.

            "$THISDIR/tools/tar_extract" "$FILENAME" "$UNPACKDIR"

            # Make sure that each tested path is now newer than the archive.

            for UNPACKTEST in $UNPACKTESTS ; do
                touch "$UNPACKTEST"
            done

            # Only unpack the archive one, breaking out of the loop after the
            # first older path was detected.

            break
        fi
    done
done

# vim: tabstop=4 expandtab shiftwidth=4
